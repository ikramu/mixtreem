## Building MixTreEM
Given you have all the dependencies installed, here are the steps.

1. Create and go to build directory.
2. Type "cmake ../". This will generate the Makefile script in the build directory.
3. Type "make". This will compile and build the project.
4. You can find the serial version of MixTreEM in ./applications/mixtreem/ and the parallel version in ./applications/mixtreem_mpi/



## Installing Boost with MPI support
1. You should have openmpi installed in the system.
2. Go to main boost directory and use the following command

```
./bootstrap.sh --with-libraries=serialization,mpi,graph,regex --prefix=$INSTALL_DIRECTORY
```
where INSTALL_DIRECTORY is the directory where you want to install the boost libraries. This is only necessary if you want to install
boost in a non-standard directory. For MixTreEM, we only need the above four packages (also given in CMakeLists.txt).

3. In order to install boost with MPI support
    * Copy the user-config.jam file from tools/build/v2/user-config.jam (or tools/build/user-config.jam)
```
cp tools/build/v2/user-config.jam .
```

	* Use the following command
```
./bjam --user-config=./user-config.jam install
```

## Linking cmake to boost installation in user-defined directory
In case you have Boost library installed at custom location (for instance if you are running MixTreEM on cluster where you have 
source-compiled the Boost in your own user directory), you may use the following command from within build/ directory
```
cmake -DCMAKE_INSTALL_PREFIX=$TARGET -DBoost_NO_BOOST_CMAKE=TRUE \
      -DBoost_NO_SYSTEM_PATHS=TRUE -DBOOST_ROOT=$TARGET \
      -DBoost_LIBRARY_DIRS=${TARGET}/lib ../
```
where ```TARGET``` is the main Boost directory (containing Boost ```include/``` and ```lib/``` directories)

Finally 
```
make -j n
make install
```
where ```n```  is the number of parallel jobs (default is 1).



Enjoy the ride ;-)

Ikram Ullah
KTH
