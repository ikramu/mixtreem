# The name of our project is "STISEM". CMakeLists files in this project can 
# refer to the root source directory of the project as ${STISEM_SOURCE_DIR} and 
# to the root binary directory of the project as ${STISEM_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.6) 
project (STISEM) 

SET(CMAKE_BUILD_TYPE Debug)

find_package(Threads REQUIRED)
find_package(MPI REQUIRED)
find_package(Boost COMPONENTS serialization graph mpi regex REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})
include_directories(${MPI_INCLUDE_PATH})

set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})

# Recurse into the "libraries" and "applications" subdirectories. 
# This does not actually cause another cmake executable to run.  
# The same process will walk through the project's entire directory structure. 
add_subdirectory (libraries/core) 
add_subdirectory (libraries/semphy) 
add_subdirectory (applications/mixtreem)
add_subdirectory (applications/outgroup_rooting)
add_subdirectory (applications/extract_seq_for_stisem_strees)
add_subdirectory (applications/sp_tree_from_stisem_log)
add_subdirectory (applications/mixtreem_mpi)
