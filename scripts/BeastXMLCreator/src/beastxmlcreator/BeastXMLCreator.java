/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beastxmlcreator;

/**
 *
 * @author ubuntu
 */
public class BeastXMLCreator {

    /**
     * @param args the command line arguments
     */     
    public static void main(String[] args) {
        //BXC bxc = new BXC();
        final int numArgs = 5;
        if(args.length != numArgs) {
            showArgsInfoAndExit();
        }
        
        int numIterations = 0;
        int thinning = 0;
        String seqDir = args[0];
        //String g2sMapping = args[1];
        String fileNamePrefix = args[1];
        String outDir = args[4];
        try {
            numIterations = Integer.parseInt(args[2]);
            thinning = Integer.parseInt(args[3]);
            
        } catch (Exception e) {
            System.err.println("Error: Either the numIterations or the thinning value is not integer");
            System.err.println(e.getMessage());
            System.err.println("Exiting now...");
            System.exit(1);
        }
        
        System.out.println("Creating BEAST XML file...");
        BXC bxc = new BXC(seqDir, fileNamePrefix, numIterations, thinning, outDir);
        System.out.println("BEAST XML file \"" + fileNamePrefix + ".xml\" created successfully");
        //bxc.readG2S("/home/ikramu/Documents/speciesTreeProject/stisem/data/synt_data/9_leaves/1/species.tree_gene1.tree.gs");
    }

    private static void showArgsInfoAndExit() {
        System.err.println("Error: Wrong number of arguments");
        System.err.println("Usage: ");
        System.err.println("java -jar BeastXMLCreator.jar seqDir g2sMapping fileNamePrefix numIterations thinning outDir");
        System.err.println("seqDir          directory where gene sequences (having .seq extension) are stored");
        //System.err.println("g2sMapping      path to gene-to-species mapping file in tab delimited format i.e. gene_name tab species name");
        System.err.println("fileNamePrefix  prefix to be used with all generated (log and tree) files");
        System.err.println("numIterations   number of iterations in the MCMC");
        System.err.println("thinning        Thinning i.e. how often values are saved in MCMC");
        System.err.println("outDir          Directory for saving the XML file");
        System.err.println();
        System.exit(1);
    }
}
