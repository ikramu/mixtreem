/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beastxmlcreator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author ikramu
 */
public class FastaReader {
    /* variables */

    HashMap<String, StringBuilder> all_seqs_as_string;
    HashMap<String, FastaFormat> all_seqs_as_fasta;
    FastaFormat super_seq;
    ArrayList<String> gene_names;
    StringBuilder seq_part;    
    StringBuilder mrbayes_info;    
    String sequence_directory;
    int num_iter;
    String best_out_file;
    
    StringBuilder nexus_file;
    StringBuilder mrbayes_file;

    public FastaReader() {
    }

    public FastaReader(String seqDir, String seqExt, int iter, String of) {
        num_iter = iter;
        sequence_directory = seqDir;
        try {
            best_out_file = new File(of).getName();
        } catch (Exception e) {
            System.err.println("Exception at FastaReader constructor in reading file name");
            System.err.println("Reason: " + e.getMessage());
            System.err.println("Exiting now...");
            System.exit(1);
        }
        //readGeneSpeciesMappingFile(mappingFile);
        readAllSequences(seqDir, seqExt);
    }

    public void readAllSequences(String seqDir, String seqExt) {
        try {
            all_seqs_as_string = new HashMap<>();
            all_seqs_as_fasta = new HashMap<>();

            /* read all sequence files */
            File[] all_files = readFiles(seqDir, seqExt);
            ArrayList<File> afiles = new ArrayList();
            for(int i = 0; i < all_files.length; ++i)
                afiles.add(all_files[i]);
            Collections.sort(afiles);

            for (int i = 0; i < afiles.size(); ++i) {
                //System.out.println("Reading " + afiles.get(i).getCanonicalPath());
                readSequenceFile(afiles.get(i));
            }

            //readAllGeneNames();
            //createNexusAndMrBayesFiles();

            //createNexusFileForBEST();
        } catch (Exception ex) {
            System.err.println("Exception @ readAllSequences(String seq_dir, String seq_ext)");
            System.err.println("Reason: " + ex.getMessage());
            System.err.println("Exiting now...");
            System.exit(1);
        }
    }

    public File[] readFiles(String dirName, final String ext) throws Exception {
        File dir = new File(dirName);
        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(ext);
            }
        });

    }
    
    public HashMap<String, StringBuilder> getAllSeqAsString(){
        return all_seqs_as_string;
    }
    
    public HashMap<String, FastaFormat> getAllSeqAsFasta(){
        return all_seqs_as_fasta;
    }

    private void readSequenceFile(File file) throws Exception {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        while ((line = br.readLine()) != null) {
            sb.append(line.trim());
            sb.append("\n");
        }

        /* add sb to all_seq hashmap */
        all_seqs_as_string.put(file.getName(), sb);
        all_seqs_as_fasta.put(file.getName(), constructFastaFromString(sb.toString()));
    }

    private void createNexusAndMrBayesFiles() {
        mrbayes_info = new StringBuilder();
        super_seq = new FastaFormat();
        seq_part = new StringBuilder();
        Iterator<String> seq_it = all_seqs_as_fasta.keySet().iterator();

        for (int i = 0; i < gene_names.size(); ++i) {
            super_seq.put(gene_names.get(i), "");
        }

        while (seq_it.hasNext()) {
            String file_name = seq_it.next();
            String rand_seq = super_seq.get(gene_names.get(0));
            int st_index = rand_seq.equals("") ? 1 : rand_seq.length() + 1;
            FastaFormat sequence = all_seqs_as_fasta.get(file_name);
            int end_index = st_index + sequence.get(gene_names.get(0)).length() - 1;
            file_name = file_name.substring(0, file_name.length() - 4);
            mrbayes_info.append("CHARSET " + file_name);
            mrbayes_info.append(" = " + st_index + "-" + end_index + ";\n");

            for (int j = 0; j < gene_names.size(); ++j) {
                String gene_id = gene_names.get(j);
                String gene_content = sequence.get(gene_id);
                String temp = super_seq.get(gene_id) + gene_content;
                super_seq.put(gene_id, temp);

                // add to mr_bayes_part as well
                seq_part.append(gene_id).append("   ");
                seq_part.append(gene_content).append("\n");
            }
            seq_part.append("\n");
        }

        //System.out.println(mrbayes_info.toString());

    }

    private FastaFormat constructFastaFromString(String seqString) {
        FastaFormat ff = new FastaFormat();
        StringBuilder sbuilder = new StringBuilder("");
        String geneName = "";
        String[] lines = seqString.split("\n");

        for (int i = 0; i < lines.length; ++i) {
            /* we exploit the fact that seq names comes before its contents */
            if (lines[i].charAt(0) == '>') {
                if (!geneName.equals("")) {
                    ff.put(geneName, sbuilder.toString());
                    sbuilder = new StringBuilder("");
                }

                String[] gn = lines[i].split(" ");
                geneName = gn[0].substring(1);
                //ff.put(geneName, "");
            } else {
                /* case for seq content */
                if (!lines[i].equals("")) {
                    sbuilder.append(lines[i].toUpperCase());
                }
            }
        }
        /* put the last gene contents into hashmap */
        ff.put(geneName, sbuilder.toString());

        return ff;
    }

    /**
     *
     * @param mappingFile gene to species mapping the format is gene_1 species_1
     * (first gene, then species)
     */
    private void readGeneSpeciesMappingFile(String mappingFile) {
        try {
            gene_names = new ArrayList();
            BufferedReader br = new BufferedReader(new FileReader(mappingFile));
            String line = "";

            while ((line = br.readLine()) != null) {
                if (line.trim().equals("")) {
                    continue;
                }

                gene_names.add(line.split(" ")[0]);
            }
        } catch (Exception e) {
            System.err.println("Error in readGeneSpeciesMappingFile(String mappingFile)");
            System.err.println("Reason: " + e.getMessage());
            System.err.println("Exiting now...");
            System.exit(1);
        }
    }

    private void createNexusFileForBEST() {
        nexus_file = new StringBuilder();
        mrbayes_file = new StringBuilder();
        int thinning = 100;
        int nchar = super_seq.get(gene_names.get(0)).length();
        Iterator<String> gf_iter = all_seqs_as_fasta.keySet().iterator();
        int total_genes = all_seqs_as_fasta.size();

        // start writing the file
        nexus_file.append("#NEXUS\n");
        nexus_file.append("[This dataset is constructed by a java program (CreateBESTMrBayesFileFromFastaSeq) using gene family data ");
        nexus_file.append(" in ").append(sequence_directory).append(" directory]\n");
        nexus_file.append("Begin data; \n");
        nexus_file.append("\tDimensions ntax=").append(gene_names.size()).append(" nchar=").append(nchar).append(";\n");
        nexus_file.append("\tFormat datatype=Protein gap=- missing=? matchchar=. interleave;\n");
        nexus_file.append("\tMatrix\n\n");

        // now add the sequences from nexus_part
        nexus_file.append(seq_part.toString());
        nexus_file.append("\t;\nEnd;\n");

        // start MrBayes file now
        mrbayes_file.append("begin mrbayes;\n");
        mrbayes_file.append("execute data.nex;\n");
        mrbayes_file.append("set autoclose=yes nowarn=yes;\n");

        // skipping the outgroup for now
        //the_file.append("outgroup 4;\n");

        // now add the mrbayes_info 
        mrbayes_file.append(mrbayes_info.toString());

        mrbayes_file.append("partition Genes = ").append(total_genes).append(": ");
        //for(int i = 0; i < gene_names.size()-1; ++i) {
        String next = gf_iter.next();
        mrbayes_file.append(next.substring(0, next.length() - 4));
        while (gf_iter.hasNext()) {
            //the_file.append(gene_names.get(i)).append(",");
            next = gf_iter.next();
            mrbayes_file.append(",").append(next.substring(0, next.length() - 4));
        }
        mrbayes_file.append(";\n");
        mrbayes_file.append("set partition=Genes;\n");

        // now add the taxa sets (or gene_ids)
        for (int i = 0; i < gene_names.size(); ++i) {
            String gf = gene_names.get(i);
            mrbayes_file.append("taxset ").append(gf).append("=").append(gf).append(";\n");
        }

        mrbayes_file.append("prset Aamodelpr=fixed(jones);\n");
        mrbayes_file.append("prset thetapr=invgamma(3,0.003) GeneMuPr=uniform(0.5,1.5) best=1;\n");
        mrbayes_file.append("unlink topology=(all) brlens=(all) statefreq=(all) genemu=(all);\n");
        mrbayes_file.append("mcmc ngen=").append(num_iter).append(" samplefreq=").append(thinning).append(" nrun=1 nchain=1;\n");
        mrbayes_file.append("sumt Burnin=").append(Math.round(num_iter / (thinning * 4)));
        //mrbayes_part.append(" Filename=").append(best_out_file).append(".sptree;\n");
        mrbayes_file.append(" Filename=best.nex.sptree;\n");
        mrbayes_file.append("sump Burnin=").append(Math.round(num_iter / (thinning * 4)));
        //mrbayes_part.append(" Filename=").append(best_out_file).append(".sptree;\n");
        mrbayes_file.append(" Filename=data.nex.sptree;\n");
        mrbayes_file.append("quit;\nend;\n");

        //System.out.println("*******************************************");
        //System.out.println("*******************************************");
        //System.out.println(the_file.toString());

        // write to output file (/tmp/BEST.output)
        //writeOutputFile(the_file.toString());        
    }

    public void writeOutputFile(String outdir) {
        try {
            String data_file = outdir + "/" + "data.nex";
            String mb_file = outdir + "/" + "BEST.nex";
            BufferedWriter bw = new BufferedWriter(new FileWriter(data_file));
            bw.write(nexus_file.toString());
            bw.flush();
            bw.close();
            
            bw = new BufferedWriter(new FileWriter(mb_file));
            bw.write(mrbayes_file.toString());
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("Couldn't write to " + outdir + " in writeOutputFile()");
            System.err.println("Reason: " + e.getMessage());
            System.err.println("Exiting now...");
            System.exit(1);
        }
    }

    private void readAllGeneNames() {
        Iterator<String> gf_iter = all_seqs_as_fasta.keySet().iterator();

        if (!gf_iter.hasNext()) {
            System.err.println("Error in getGeneNames()");
            System.err.println("There isn't any fasta sequences in the hashmap. "
                    + "Perhaps there was some error in reading them from the file.");
            System.err.println("Exiting now...");
            System.exit(1);
        }

        FastaFormat msa = all_seqs_as_fasta.get(gf_iter.next());
        //Iterator<String> gnames = msa.keySet().iterator();
        gene_names = new ArrayList(msa.keySet());
        Collections.sort(gene_names);
        //while(gnames.hasNext()){
        //    gene_names.add(gnames.next());
        //}
        //System.out.println("");
    }
}
