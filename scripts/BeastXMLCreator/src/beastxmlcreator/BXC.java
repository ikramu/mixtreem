/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beastxmlcreator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author ubuntu
 */
public class BXC {
    //String g2sFile = "/home/ikramu/Documents/speciesTreeProject/stisem/data/synt_data/9_leaves/1/species.tree_gene1.tree.gs";    

    String gfDirectory = "/tmp/gene_families/";
    String fileNamePrefix;
    String xmlOutDirectory;
    int numIterations;
    int thinning;
    //String gfDirectory = "/home/ikramu/Documents/speciesTreeProject/stisem/data/synt_data/9_leaves/set1/gene_families/";
    HashMap<String, String> gsMapping;
    StringBuilder xmlFile;

    public BXC() {
        xmlFile = new StringBuilder();
        initialize();
    }

    /**
     * Constructor with arguments
     *
     * @param gfd gene file directory
     * @param g2s gene to species mapping file
     * @param fns file prefix to use with log and tree files
     * @param numIt number of iterations (in MCMC)
     * @param th thinning i.e. how often values are saved (in MCMC)
     * @param xod output directory for generated XML file
     */
    public BXC(String gfd, String fns, int numIt, int th, String xod) {
        // copy arguments
        gfDirectory = gfd;
        fileNamePrefix = fns;
        numIterations = numIt;
        thinning = th;
        xmlOutDirectory = xod;

        // initialize stringbuilder object
        xmlFile = new StringBuilder();

        // construct the BEAST xml file
        initialize();
    }

    public void extractG2S(HashMap<String, FastaFormat> seqAsFasta) {
        try {
            Iterator<String> files = seqAsFasta.keySet().iterator();

            gsMapping = new HashMap<>();
            while (files.hasNext()) {
                FastaFormat ff = seqAsFasta.get(files.next());
                Iterator<String> genes = ff.keySet().iterator();                
                while (genes.hasNext()) {
                    String g = genes.next();
                    String s = g.substring(0, g.length() - 2);
                    if (!gsMapping.containsKey(g)) {
                        gsMapping.put(g, s);
                    }
                }
            }
            /*
             BufferedReader br = new BufferedReader(new FileReader(g2s));
             String line = "";
             while((line = br.readLine()) != null) {
             if(!line.trim().equals("")){
             String[] tok = line.split("\t");
             //System.out.println(tok[0] + "," + tok[1]);
             gsMapping.put(tok[0], tok[1]);
             }
             }
             */
        } catch (Exception e) {
            System.err.println("Some error in readG2S. Exiting...");
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    private void initialize() {
        //readG2S(g2sFile);
        FastaReader freader = new FastaReader();
        freader.readAllSequences(gfDirectory, "seq");
        HashMap<String, FastaFormat> seqAsFasta = freader.getAllSeqAsFasta();
        extractG2S(seqAsFasta);

        /* contents of Beast XML file */
        xmlFile.append("<beast>\n");
        xmlFile.append(writeListOfTaxa());
        xmlFile.append(writeAllSequences(seqAsFasta));
        xmlFile.append(writePatterns(seqAsFasta));
        xmlFile.append(writeStartingTree(seqAsFasta));
        xmlFile.append(writeGenerateTreeModel(seqAsFasta));
        xmlFile.append(writeUniformRatesAcrossBranches(seqAsFasta));
        xmlFile.append(writeModelForGeneFamiles(seqAsFasta));
        xmlFile.append(writeLikelihoodForGeneFamiles(seqAsFasta));
        xmlFile.append(writeSpeciesDefinitions(seqAsFasta));
        xmlFile.append(writeDefineOperators(seqAsFasta));
        xmlFile.append(writeMCMCInfo(seqAsFasta));
        xmlFile.append("</beast>\n");

        writeToFile(xmlFile.toString());
    }

    private String writeListOfTaxa() {
        StringBuilder sb = new StringBuilder();
        sb.append("<taxa id=\"taxa\">\n");
        Iterator<String> miter = gsMapping.keySet().iterator();
        while (miter.hasNext()) {
            String gName = miter.next();
            String sName = gsMapping.get(gName);
            sb.append("\t<taxon id=\"" + gName + "\">\n");
            sb.append("\t<attr name=\"species\">" + sName + "</attr>\n");
            sb.append("\t</taxon>\n");
        }
        sb.append("</taxa>\n");

        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeAllSequences(HashMap<String, FastaFormat> seqAsFasta) {
        Iterator<String> fNames = seqAsFasta.keySet().iterator();
        StringBuilder sb = new StringBuilder();

        while (fNames.hasNext()) {
            String file = fNames.next();
            FastaFormat ff = seqAsFasta.get(file);
            sb.append("<alignment id=\"" + file + ".id\" dataType=\"amino acid\">\n");
            Iterator<String> genes = ff.keySet().iterator();
            while (genes.hasNext()) {
                String gene = genes.next();
                sb.append("\t<sequence>\n");
                sb.append("\t<taxon idref=\"" + gene + "\"/>\n");
                sb.append("\t" + ff.get(gene) + "\n");
                sb.append("\t</sequence>\n");
            }
            sb.append("\t</alignment>\n");
        }
        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writePatterns(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();
        //Iterator<String> files  = gsMapping.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<patterns id=\"" + file + ".patterns\" from=\"1\" strip=\"false\">\n");
            sb.append("\t<alignment idref=\"" + file + ".id\"/>\n");
            sb.append("\t</patterns>\n");
        }
        return sb.toString();
    }

    private Object writeStartingTree(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        /* first write the constant part */
        sb.append("<constantSize id=\"constant\" units=\"substitutions\">\n");
        sb.append("<populationSize>\n");
        sb.append("<parameter id=\"constant.popSize\" value=\"1.0\" lower=\"0.0\"/>\n");
        sb.append("</populationSize>\n");
        sb.append("</constantSize>\n");

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<coalescentSimulator id=\"" + file + ".startingTree\">\n");
            sb.append("\t<taxa idref=\"taxa\"/>\n");
            sb.append("\t<constantSize idref=\"constant\"/>\n");
            sb.append("\t</coalescentSimulator>\n");
        }
        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeGenerateTreeModel(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<treeModel id=\"" + file + ".treeModel\">\n");
            sb.append("\t\t<coalescentTree idref=\"" + file + ".startingTree\"/>\n");
            sb.append("\t<rootHeight>\n");
            sb.append("\t\t<parameter id=\"" + file + ".treeModel.rootHeight\"/>\n");
            sb.append("\t</rootHeight>\n");
            sb.append("\t<nodeHeights internalNodes=\"true\">\n");
            sb.append("\t\t<parameter id=\"" + file + ".treeModel.internalNodeHeights\"/>\n");
            sb.append("\t</nodeHeights>\n");
            sb.append("\t<nodeHeights internalNodes=\"true\" rootNode=\"true\">\n");
            sb.append("\t\t<parameter id=\"" + file + ".treeModel.allInternalNodeHeights\"/>\n");
            sb.append("\t</nodeHeights>\n");
            sb.append("\t</treeModel>\n");
        }
        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeUniformRatesAcrossBranches(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<strictClockBranchRates id=\"" + file + ".branchRates\">\n");
            sb.append("\t<rate>\n");
            sb.append("\t\t<parameter id=\"" + file + ".clock.rate\" value=\"1.0\" lower=\"0.0\"/>\n");
            sb.append("\t</rate>\n");
            sb.append("\t</strictClockBranchRates>\n");
        }
        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeModelForGeneFamiles(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<aminoAcidModel id=\"" + file + ".aa\" type=\"JTT\"/>\n");
            sb.append("\t<siteModel id=\"" + file + ".siteModel\">\n");
            sb.append("\t\t<substitutionModel>\n");
            sb.append("\t\t\t<aminoAcidModel idref=\"" + file + ".aa\"/>\n");
            sb.append("\t\t</substitutionModel>\n");
            sb.append("\t</siteModel>\n");
        }
        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeLikelihoodForGeneFamiles(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<treeLikelihood id=\"" + file + ".treeLikelihood\" useAmbiguities=\"false\">\n");
            sb.append("\t\t<patterns idref=\"" + file + ".patterns\"/>\n");
            sb.append("\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
            sb.append("\t\t<siteModel idref=\"" + file + ".siteModel\"/>\n");
            sb.append("\t\t<strictClockBranchRates idref=\"" + file + ".branchRates\"/>\n");
            sb.append("\t</treeLikelihood>\n");
        }
        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeSpeciesDefinitions(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        sb.append("\t<species id=\"species\">\n");

        // first bind species to the their genes
        Iterator<String> theList = gsMapping.keySet().iterator();
        while (theList.hasNext()) {
            String g = theList.next();
            String s = gsMapping.get(g);
            sb.append("\t<sp id=\"" + s + "\">\n");
            sb.append("\t\t<taxon idref=\"" + g + "\"/>\n");
            sb.append("\t</sp>\n");
        }

        sb.append("\t<geneTrees id=\"geneTrees\">\n");
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
        }

        sb.append("\t\t</geneTrees>\n");
        sb.append("\t</species>\n");

        // full species set for species tree root height
        theList = gsMapping.keySet().iterator();
        sb.append("\t<taxa id=\"allSpecies\">\n");
        while (theList.hasNext()) {
            String g = theList.next();
            String s = gsMapping.get(g);
            sb.append("\t\t<sp idref=\"" + s + "\"/>\n");
        }
        sb.append("\t</taxa>\n");

        // Species Tree: Provides Per branch demographic function
        sb.append("\t<speciesTree id=\"sptree\" constantRoot=\"true\">\n");
        sb.append("\t\t<species idref=\"species\"/>\n");
        sb.append("\t\t<sppSplitPopulations value=\"1.0\">\n");
        sb.append("\t\t\t<parameter id=\"speciesTree.splitPopSize\"/>\n");
        sb.append("\t\t</sppSplitPopulations>\n");
        sb.append("\t</speciesTree>\n");

        // Species tree prior: Birth Death Model
        sb.append("\t<birthDeathModel id=\"birthDeath\" units=\"substitutions\">\n");
        sb.append("\t\t<birthMinusDeathRate>\n");
        sb.append("\t\t\t<parameter id=\"species.birthDeath.meanGrowthRate\" value=\"1.0\" lower=\"0.0\"/>\n");
        sb.append("\t\t</birthMinusDeathRate>\n");
        sb.append("\t\t<relativeDeathRate>\n");
        sb.append("\t\t\t<parameter id=\"species.birthDeath.relativeDeathRate\" value=\"0.5\" lower=\"0.0\" upper=\"1.0\"/>\n");
        sb.append("\t\t</relativeDeathRate>\n");
        sb.append("\t</birthDeathModel>\n");

        // Species Tree Likelihood: Birth Death Model
        sb.append("\t<speciationLikelihood id=\"speciation.likelihood\">\n");
        sb.append("\t\t<model>\n");
        sb.append("\t\t\t<birthDeathModel idref=\"birthDeath\"/>\n");
        sb.append("\t\t</model>\n");
        sb.append("\t\t<speciesTree>\n");
        sb.append("\t\t\t<speciesTree idref=\"sptree\"/>\n");
        sb.append("\t\t</speciesTree>\n");
        sb.append("\t</speciationLikelihood>\n");
        sb.append("\t\t\n");

        //Species Tree: tmrcaStatistic 
        sb.append("\t<tmrcaStatistic id=\"speciesTree.rootHeight\" name=\"speciesTree.rootHeight\">\n");
        sb.append("\t\t<speciesTree idref=\"sptree\"/>\n");
        sb.append("\t\t<mrca>\n");
        sb.append("\t\t\t<taxa idref=\"allSpecies\"/>\n");
        sb.append("\t\t</mrca>\n");
        sb.append("\t</tmrcaStatistic>\n");
        sb.append("\t\t\n");

        //Species Tree: Coalescent likelihood for gene trees under species tree
        sb.append("\t<speciesCoalescent id=\"species.coalescent\">\n");
        sb.append("\t\t<species idref=\"species\"/>\n");
        sb.append("\t\t<speciesTree idref=\"sptree\"/>\n");
        sb.append("\t</speciesCoalescent>\n");
        sb.append("\t\t\n");

        //Species tree prior: gama2 + gamma4 
        sb.append("\t<mixedDistributionLikelihood id=\"species.popSizesLikelihood\">\n");
        sb.append("\t\t<distribution0>\n");
        sb.append("\t\t\t<gammaDistributionModel>\n");
        sb.append("\t\t\t\t<shape>\n");
        sb.append("\t\t2\n");
        sb.append("\t\t\t\t</shape>\n");
        sb.append("\t\t\t\t<scale>\n");
        sb.append("\t\t\t\t\t<parameter id=\"species.popMean\" value=\"1.0\" lower=\"0.0\"/>\n");
        sb.append("\t\t\t\t</scale>\n");
        sb.append("\t\t\t</gammaDistributionModel>\n");
        sb.append("\t\t</distribution0>\n");
        sb.append("\t\t<distribution1>\n");
        sb.append("\t\t\t<gammaDistributionModel>\n");
        sb.append("\t\t\t\t<shape>\n");
        sb.append("\t\t\t\t4\n");
        sb.append("\t\t\t\t</shape>\n");
        sb.append("\t\t\t\t<scale>\n");
        sb.append("\t\t\t\t\t<parameter idref=\"species.popMean\"/>\n");
        sb.append("\t\t\t\t</scale>\n");
        sb.append("\t\t\t</gammaDistributionModel>\n");
        sb.append("\t\t</distribution1>\n");

        sb.append("\t\t<data>\n");
        sb.append("\t\t\t<parameter idref=\"speciesTree.splitPopSize\"/>\n");
        sb.append("\t\t</data>\n");
        sb.append("\t\t<indicators>\n");
        sb.append("\t\t\t<parameter value=\"1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\"/>\n");
        sb.append("\t\t</indicators>\n");
        sb.append("\t</mixedDistributionLikelihood>\n");

        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeDefineOperators(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        sb.append("<operators id=\"operators\" optimizationSchedule=\"default\">");
        /*        
         while(files.hasNext()){
         String file = files.next();
         sb.append("\t<scaleOperator scaleFactor=\"0.75\" weight=\"0.1\">\n");
         sb.append("\t\t<parameter idref=\"" + file + ".alpha\"/>\n");
         sb.append("\t</scaleOperator>\n");                        
         } 
        
         files = seqAsFasta.keySet().iterator();
         */

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t<scaleOperator scaleFactor=\"0.75\" weight=\"3\">\n");
            sb.append("\t\t<parameter idref=\"" + file + ".clock.rate\"/>\n");
            sb.append("\t</scaleOperator>\n");
        }

        // scale factor UP
        files = seqAsFasta.keySet().iterator();
        sb.append("\t<upDownOperator scaleFactor=\"0.75\" weight=\"30\">\n");
        sb.append("\t\t<up>\n");
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t\t<parameter idref=\"" + file + ".clock.rate\"/>\n");
        }
        sb.append("\t\t</up>\n");

        // scale factor DOWN
        files = seqAsFasta.keySet().iterator();
        sb.append("\t\t<down>\n");
        sb.append("\t\t<parameter idref=\"sptree\"/>\n");
        sb.append("\t\t<parameter idref=\"species.popMean\"/>\n");
        sb.append("\t\t<parameter idref=\"speciesTree.splitPopSize\"/>\n");

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t\t<parameter idref=\"" + file + ".treeModel.allInternalNodeHeights\"/>\n");
        }
        sb.append("\t\t</down>\n");
        sb.append("\t</upDownOperator>\n");

        // subtreeSlide
        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<subtreeSlide size=\"0.09\" gaussian=\"true\" weight=\"15\">\n");
            sb.append("\t\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
            sb.append("\t\t</subtreeSlide>\n");
            sb.append("\t\t<narrowExchange weight=\"15\">\n");
            sb.append("\t\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
            sb.append("\t\t</narrowExchange>\n");
            sb.append("\t\t<wideExchange weight=\"3\">\n");
            sb.append("\t\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
            sb.append("\t\t</wideExchange>\n");
            sb.append("\t\t<wilsonBalding weight=\"3\">\n");
            sb.append("\t\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
            sb.append("\t\t</wilsonBalding>\n");
            sb.append("\t\t<scaleOperator scaleFactor=\"0.75\" weight=\"3\">\n");
            sb.append("\t\t\t<parameter idref=\"" + file + ".treeModel.rootHeight\"/>\n");
            sb.append("\t\t</scaleOperator>\n");
            sb.append("\t\t<uniformOperator weight=\"30\">\n");
            sb.append("\t\t\t<parameter idref=\"" + file + ".treeModel.internalNodeHeights\"/>\n");
            sb.append("\t\t</uniformOperator>\n");
        }

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<upDownOperator scaleFactor=\"0.75\" weight=\"3\">\n");
            sb.append("\t\t<up>\n");
            sb.append("\t\t\t<parameter idref=\"" + file + ".clock.rate\"/>\n");
            sb.append("\t\t</up>\n");
            sb.append("\t\t<down>\n");
            sb.append("\t\t\t<parameter idref=\"" + file + ".treeModel.allInternalNodeHeights\"/>\n");
            sb.append("\t\t</down>\n");
            sb.append("\t\t</upDownOperator>\n");
        }

        // some lines about species tree 
        sb.append("\t\t<scaleOperator scaleFactor=\"0.9\" weight=\"5\">\n");
        sb.append("\t\t\t<parameter idref=\"species.popMean\"/>\n");
        sb.append("\t\t</scaleOperator>\n");
        sb.append("\t\t<scaleOperator scaleFactor=\"0.75\" weight=\"3\">\n");
        sb.append("\t\t\t<parameter idref=\"species.birthDeath.meanGrowthRate\"/>\n");
        sb.append("\t\t</scaleOperator>\n");
        sb.append("\t\t<scaleOperator scaleFactor=\"0.75\" weight=\"3\">\n");
        sb.append("\t\t\t<parameter idref=\"species.birthDeath.relativeDeathRate\"/>\n");
        sb.append("\t\t</scaleOperator>\n");
        sb.append("\t\t<scaleOperator scaleFactor=\"0.5\" weight=\"94\">\n");
        sb.append("\t\t\t<parameter idref=\"speciesTree.splitPopSize\"/>\n");
        sb.append("\t\t</scaleOperator>\n");
        sb.append("\t\t<nodeReHeight weight=\"94\">\n");
        sb.append("\t\t<species idref=\"species\"/>\n");
        sb.append("\t\t<speciesTree idref=\"sptree\"/>\n");
        sb.append("\t\t</nodeReHeight>\n");
        sb.append("\t\t</operators>\n");

        //System.out.println(sb.toString());
        return sb.toString();
    }

    private String writeMCMCInfo(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        sb.append("\t<mcmc id=\"mcmc\" chainLength=\"" + numIterations
                + "\" autoOptimize=\"true\" operatorAnalysis=\"" + fileNamePrefix + ".ops\">\n");
        sb.append("\t\t<posterior id=\"posterior\">\n");
        sb.append("\t\t<prior id=\"prior\">\n");
        sb.append("\t\t<speciesCoalescent idref=\"species.coalescent\"/>\n");
        sb.append("\t\t<mixedDistributionLikelihood idref=\"species.popSizesLikelihood\"/>\n");
        sb.append("\t\t<speciationLikelihood idref=\"speciation.likelihood\"/>\n");
        /*
         while(files.hasNext()){
         String file = files.next();
         sb.append("\t\t<exponentialPrior mean=\"0.5\" offset=\"0.0\">\n");
         sb.append("\t\t\t<parameter idref=\"" + file + ".alpha\"/>\n");
         sb.append("\t\t</exponentialPrior>\n");
         sb.append("\t\t\n");
         }      
         */

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<uniformPrior lower=\"0.0\" upper=\"1.0E10\">\n");
            sb.append("\t\t\t<parameter idref=\"" + file + ".clock.rate\"/>\n");
            sb.append("\t\t</uniformPrior>\n");
            sb.append("\t\t\n");
        }

        sb.append("\t\t<oneOnXPrior>\n");
        sb.append("\t\t<parameter idref=\"species.popMean\"/>\n");
        sb.append("\t\t</oneOnXPrior>\n");
        sb.append("\t\t<oneOnXPrior>\n");
        sb.append("\t\t\t<parameter idref=\"species.birthDeath.meanGrowthRate\"/>\n");
        sb.append("\t\t</oneOnXPrior>\n");
        sb.append("\t\t<uniformPrior lower=\"0.0\" upper=\"1.0\">\n");
        sb.append("\t\t\t<parameter idref=\"species.birthDeath.relativeDeathRate\"/>\n");
        sb.append("\t\t</uniformPrior>\n");
        sb.append("\t\t</prior>\n");

        // likelihoods
        sb.append("\t\t<likelihood id=\"likelihood\">\n");
        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<treeLikelihood idref=\"" + file + ".treeLikelihood\"/>\n");
        }
        sb.append("\t\t</likelihood>\n");
        sb.append("\t</posterior>\n");

        // operators
        sb.append("\t\t<operators idref=\"operators\"/>\n");
        sb.append("\t\t<log id=\"screenLog\" logEvery=\"" + thinning + "\">\n");
        sb.append("\t\t<column label=\"Posterior\" dp=\"4\" width=\"12\">\n");
        sb.append("\t\t<posterior idref=\"posterior\"/>\n");
        sb.append("\t\t</column>\n");
        sb.append("\t\t<column label=\"Prior\" dp=\"4\" width=\"12\">\n");
        sb.append("\t\t<prior idref=\"prior\"/>\n");
        sb.append("\t\t</column>\n");
        sb.append("\t\t<column label=\"Likelihood\" dp=\"4\" width=\"12\">\n");
        sb.append("\t\t<likelihood idref=\"likelihood\"/>\n");
        sb.append("\t\t</column>\n");
        sb.append("\t\t<column label=\"PopMean\" dp=\"4\" width=\"12\">\n");
        sb.append("\t\t<parameter idref=\"species.popMean\"/>\n");
        sb.append("\t\t</column>\n");

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<column label=\"" + file + ".rootHeight\" sf=\"6\" width=\"12\">\n");
            sb.append("\t\t<parameter idref=\"" + file + ".treeModel.rootHeight\"/>\n");
            sb.append("\t\t</column>\n");
        }

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<column label=\"" + file + ".clock.rate\" sf=\"6\" width=\"12\">\n");
            sb.append("\t\t<parameter idref=\"" + file + ".clock.rate\"/>\n");
            sb.append("\t\t</column>\n");
        }
        sb.append("\t\t</log>\n");

        sb.append(writeWriteLogToFile(seqAsFasta));
        sb.append("\t\t</mcmc>\n");

        sb.append("\t\t<report>\n");
        sb.append("\t\t<property name=\"timer\">\n");
        sb.append("\t\t<mcmc idref=\"mcmc\"/>\n");
        sb.append("\t\t</property>\n");
        sb.append("\t\t</report>\n");

        return sb.toString();
    }

    private void writeToFile(String xmlFile) {
        try {
            String outfile = xmlOutDirectory + "/" + fileNamePrefix + ".xml";
            BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
            bw.write(xmlFile);
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("Error at writeToFile()");
            System.err.println("Reason: " + e.getMessage());
            System.err.println("Exiting now...");
            System.exit(1);
        }
    }

    private String writeWriteLogToFile(HashMap<String, FastaFormat> seqAsFasta) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> files = seqAsFasta.keySet().iterator();

        sb.append("\t\t<log id=\"fileLog\" logEvery=\"" + thinning + "\" fileName=\""
                + fileNamePrefix + ".log\" overwrite=\"false\">\n");
        sb.append("\t\t<posterior idref=\"posterior\"/>\n");
        sb.append("\t\t<prior idref=\"prior\"/>\n");
        sb.append("\t\t<likelihood idref=\"likelihood\"/>\n");
        sb.append("\t\t<speciesCoalescent idref=\"species.coalescent\"/>\n");
        sb.append("\t\t<mixedDistributionLikelihood idref=\"species.popSizesLikelihood\"/>\n");
        sb.append("\t\t<speciationLikelihood idref=\"speciation.likelihood\"/>\n");
        sb.append("\t\t<parameter idref=\"species.popMean\"/>\n");
        sb.append("\t\t<parameter idref=\"speciesTree.splitPopSize\"/>\n");
        sb.append("\t\t<parameter idref=\"species.birthDeath.meanGrowthRate\"/>\n");
        sb.append("\t\t<parameter idref=\"species.birthDeath.relativeDeathRate\"/>\n");
        sb.append("\t\t<tmrcaStatistic idref=\"speciesTree.rootHeight\"/>\n");

        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<parameter idref=\"" + file + ".treeModel.rootHeight\"/>\n");
        }

        /*
         files = seqAsFasta.keySet().iterator();
         while(files.hasNext()){
         String file = files.next();
         sb.append("\t\t<parameter idref=\"" + file + ".alpha\"/>\n");           
         }
         */

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<parameter idref=\"" + file + ".clock.rate\"/>\n");
        }

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<parameter idref=\"" + file + ".treeLikelihood\"/>\n");
        }
        sb.append("\t\t</log>\n");

        sb.append("\t\t<logTree id=\"species.treeFileLog\" logEvery=\"" + thinning + "\" nexusFormat=\"true\" fileName=\""
                + fileNamePrefix + ".species.trees\" sortTranslationTable=\"true\">\n");
        sb.append("\t\t<speciesTree idref=\"sptree\"/>\n");
        sb.append("\t\t<posterior idref=\"posterior\"/>\n");
        sb.append("\t\t</logTree>\n");

        files = seqAsFasta.keySet().iterator();
        while (files.hasNext()) {
            String file = files.next();
            sb.append("\t\t<logTree id=\"" + file + ".treeFileLog\" logEvery=\"" + thinning
                    + "\" nexusFormat=\"true\" fileName=\"" + fileNamePrefix + "." + file
                    + ".trees\" sortTranslationTable=\"true\">\n");
            sb.append("\t\t<treeModel idref=\"" + file + ".treeModel\"/>\n");
            sb.append("\t\t<trait name=\"rate\" tag=\"" + file + ".rate\">\n");
            sb.append("\t\t<strictClockBranchRates idref=\"" + file + ".branchRates\"/>\n");
            sb.append("\t\t</trait>\n");
            sb.append("\t\t<posterior idref=\"posterior\"/>\n");
            sb.append("\t\t</logTree>\n");

        }

        return sb.toString();
    }
}
