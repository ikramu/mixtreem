/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package replaceseqnames;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


/**
 *
 * @author ikramu
 */
public class ReplaceSeqNames {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        final String directory = "/tmp/jj";
        keepOnlySpeciesNamesInAllFiles(directory);
        //StisemUtilities su = new StisemUtilities();
        //su.getUniqueSpeciesGeneFamilies("/tmp/qqq", 5, "/tmp/list_qqq.txt");
    }

    private static void keepOnlySpeciesNamesInAllFiles(String directory) {
        String mod_files = directory + "_mod";
        try {
            File dir = new File(directory);
            File mod_dir = new File(mod_files);
            if(!mod_dir.exists()){
                mod_dir.mkdir();
            }
            
            File[] list = dir.listFiles();
            
            for(int i = 0; i < list.length; ++i){
                if(list[i].getName().trim().contains("svn"))
                    continue;
                BufferedReader br = new BufferedReader(new FileReader(list[i]));
                BufferedWriter bw = new BufferedWriter(new FileWriter(mod_dir.getAbsoluteFile() + "/" + list[i].getName()));
                String line = "";
                while((line = br.readLine()) != null){
                    if(line.trim().charAt(0) == '>'){
                        String tok[] = line.split("\\|");
                        bw.write(tok[0]);                                              
                    } else {
                        bw.write(line);
                    }
                    bw.write("\n");
                    System.out.println(list[i].getName() + " done...");
                }
                bw.flush();
                bw.close();
                br.close();
            }
        } catch (Exception e) {
            System.err.println("Some exception : " + e.getMessage());
        }
    }    
}
