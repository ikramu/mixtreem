/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package replaceseqnames;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author ikramu
 */
public class StisemUtilities {
    
    /**
     * Get unique sets where each set is a hashmap of (i) the specific species
     * set (1,2,3 or 1,2,4 or 1,3,4 etc) AND (ii) the names of 
     * corresponding files. The format of input file "infile" must be 
     * infile_name1
     * gene_1
     * gene_2
     * ... gene_n
     * infile_name2
     * gene_1 and so on
     * 
     * @param infile The text file containing above info
     * @param numSpecies number of species. Though it may be parsed from infile
     * but still to make the life easy we provide it.
     * @param outfile The output file where we write the summary of the result
     */
    public void getUniqueSpeciesGeneFamilies(String infile, int numSpecies, 
            String outfile){
        /*
        ArrayList<String> l1 = new ArrayList<>();
        l1.add("a"); l1.add("b"); l1.add("c");
        ArrayList<String> l2 = new ArrayList<>();
        l2.add("c"); l2.add("d"); l2.add("a");
        if(l1.containsAll(l2)) 
            System.out.println("YES");
        else
            System.out.println("NO");
        */
        
        HashMap<ArrayList<String>, String> hMap = new HashMap<>();
        try {
            BufferedReader buf = new BufferedReader(new FileReader(infile));
            ArrayList<String> list = new ArrayList<>();
            
            String line = "";
            String fileName = "";
            
            while((line = buf.readLine()) != null){
                if(line.trim().charAt(0) == '>'){
                    list.add(line);
                }else {
                    String[] tok = line.split("/");
                    fileName = tok[tok.length - 1];
                    
                    /* now check if the list already exists */
                    Iterator<ArrayList<String>> it = hMap.keySet().iterator();
                    boolean found = false;
                    
                    while(it.hasNext()) { 
                        ArrayList<String> curList = it.next();
                        if(curList.containsAll(list)){
                            //curList
                            String val = hMap.get(curList);
                            val += " " + fileName;
                            hMap.put(curList, val);
                            list.clear();
                            found = true;
                            break;
                        }
                    }
                    
                    /* if not found, then add the list to hMap */
                    if(!found){
                        ArrayList<String> temp = new ArrayList<>(list);
                        Collections.sort(temp);
                        hMap.put(temp, fileName);
                        list.clear();
                    }                    
                }
            }
            
            /* now write the results to outfile */
            BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
            
            
            Iterator<ArrayList<String>> it = hMap.keySet().iterator();
            while(it.hasNext()){
                ArrayList<String> as = it.next();
                
                bw.write(as.toString());
                bw.write("\t");
                bw.write(hMap.get(as));
                bw.write("\n");
            }
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("Some error in getUniqueSpeciesGeneFamilies(): ");
            System.err.println(e.getMessage());
        }
    }
    
}
