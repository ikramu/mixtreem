/* 
 * File:   boost_version.cpp
 * Author: ikramu
 *
 * Created on January 14, 2014, 3:02 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include "BipartiteMatching.h"

#define VERBOSE false

using namespace std;
using namespace boost;

typedef boost::adjacency_list<boost::vecS, boost::vecS,
boost::bidirectionalS,
boost::property<boost::vertex_name_t, std::string>,
boost::property<boost::edge_weight_t,
double> > Graph;
typedef boost::property<boost::edge_weight_t, double> EdgeWeightProperty;

void read_rf_values_from_file(string, vector< vector<int> > &, int &, int &);
void show_graph_edges(Graph g);
void show_usage(char *prog_name);

/*
 * 
 */
int main(int argc, char** argv) {
    if (argc != 2) {
        show_usage(argv[0]);
    }
    string rf_file(argv[1]); // = "/tmp/rf_data.txt";
    vector< vector<int> > rf_distances;
    int num_true, num_computed;

    read_rf_values_from_file(rf_file, rf_distances, num_true, num_computed);

    // first show the contents of the matrix
    cout << "\nThe RF-distance graph (Real-vs-Computed) is " << endl;
    for (int i = 0; i < rf_distances.size(); ++i) {
        for (int j = 0; j < rf_distances[i].size(); ++j) {
            cout << -rf_distances[i][j] << ",\t";
        }
        cout << endl;
    }
    cout << endl;

    if ((rf_distances.size() == 0) || (rf_distances[0].size() == 0)) {
        cerr << "some problem in reading RF-distance file \"" << rf_file << "\"." << endl;
        cerr << "Exiting now..." << endl;
        exit(EXIT_FAILURE);
    }

    // first rf_distances[0].size() constitute first partite while 
    // rf_distances.size() constitute the second partite.
    // we will keep this convension while assigning weights to links
    Graph pg(rf_distances[0].size() + rf_distances.size());
    boost::property_map<Graph,
            boost::vertex_name_t>::type
            vm = boost::get(boost::vertex_name, pg);
    boost::property_map<Graph,
            boost::edge_weight_t>::type
            ew = boost::get(boost::edge_weight, pg);

    unsigned first_partite_size = rf_distances[0].size();
    // start assigning the weights
    // i and j refers to true and computed partites respectively    
    for (unsigned i = 0; i < rf_distances.size(); ++i) {
        for (unsigned j = 0; j < first_partite_size; ++j) {
            EdgeWeightProperty ewp = rf_distances[i][j];
            add_edge(j, i + first_partite_size, ewp, pg);
        }
    }

    if (VERBOSE)
        show_graph_edges(pg);

    /* by reversing the signs of edge weights, and computing the maximum matching, 
     * we compute the minimum matching.
     */
    int total_rf_distance = 0;
    std::vector<std::pair<int, int> > out
            = get_maximum_weight_bipartite_matching(pg, first_partite_size,
            boost::get(boost::vertex_index, pg),
            boost::get(boost::edge_weight, pg));

    // now the results
    cout << "*******************************************************************" << endl;
    cout << "Minimum matching results for RF-distances of computed vs real trees" << endl;
    cout << "*******************************************************************" << endl;
    cout << "Edge\tCom-Tree-No\tReal-Tree-No\t=\tRF-value" << endl;
    for (unsigned int i = 0; i < out.size(); ++i) {
        int original_value = -rf_distances[out[i].second - first_partite_size][out[i].first];
        std::cout << "Edge\t" << out[i].first << "\t\t" << out[i].second - first_partite_size
                << "\t\t=\t" << original_value << std::endl;
        // reverse the sign and add to total 
        total_rf_distance += original_value;
    }

    cout << "\nTotal RF-distance is " << total_rf_distance << endl;
    return 0;
}

void show_usage(char *prog_name) {
    cerr << "Error: wrong number of arguments" << endl;
    cerr << "Usage: " << prog_name << " rf_distance_file" << endl;
    cerr << "\nwhere" << endl;
    cerr << "rf_distance_file: Real-vs-Computed RF-distance file generated by create_rf_matrix.r script" << endl;
    cerr << "Exiting now..." << endl;
    exit(EXIT_FAILURE);
}

void show_graph_edges(Graph g) {
    Graph::vertex_iterator vertexIt, vertexEnd;
    typedef Graph::edge_descriptor Edge;
    Graph::in_edge_iterator inedgeIt, inedgeEnd;
    Graph::out_edge_iterator outedgeIt, outedgeEnd;
    boost::property_map<Graph, boost::edge_weight_t>::type weights =
            get(boost::edge_weight_t(), g);

    BOOST_FOREACH(Edge e, edges(g)) {
        std::cout << "(" << boost::source(e, g) << ", "
                << boost::target(e, g) << ")\t"
                << get(weights, e) << "\n";
    }

    /*
    tie(vertexIt, vertexEnd) = vertices(g);
    for (; vertexIt != vertexEnd; ++vertexIt) {
        cout << "incoming edges for " << *vertexIt << ": ";
        tie(inedgeIt, inedgeEnd) = in_edges(*vertexIt, g);
        for (; inedgeIt != inedgeEnd; ++inedgeIt) {
            cout << *inedgeIt << " ";
            //cout << inedgeIt->first << ", " << inedgeIt->second << endl;
        }
        cout << "\n";
        std::cout << "out-edges for " << *vertexIt << ":  " << endl;
        tie(outedgeIt, outedgeEnd) = out_edges(*vertexIt, g);
        for (; outedgeIt != outedgeEnd; ++outedgeIt) {
            cout << *outedgeIt << " ";
        }
    }
     */
    //cout << "weight (" << boost::source(me.first, G)
    //       << ", " << boost::target(me.first, G) <<
    //       ") = " << get(myweights, me.first) << endl;
}

void read_rf_values_from_file(string filename, vector< vector<int> > &rf_distances, int &num_true, int &num_computed) {
    ifstream ifile(filename.c_str());
    string line;
    bool started = false;
    vector<int> vec;

    if (ifile.is_open()) {
        while (getline(ifile, line)) {
            if (line.find("Real") == 0) {
                started = true;
                continue;
            }

            if (started && (line.find("***************") == 0)) {
                break;
            }
            if (started) {
                char_separator<char> sep(",\t");
                tokenizer< char_separator<char> > tokens(line, sep);

                BOOST_FOREACH(const string& t, tokens) {
                    vec.push_back(-atoi(t.c_str()));
                }
                rf_distances.push_back(vec);
                vec.clear();
            }
        }
        ifile.close();
    }
}

