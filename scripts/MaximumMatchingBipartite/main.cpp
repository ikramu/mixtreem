/* 
 * File:   main.cpp
 * Author: ikramu
 *
 * Created on January 13, 2014, 10:52 PM
 */

#include <cstdlib>
#include <iostream>
#include <string.h>

using namespace std;

/*
 * 
 */

// define M and N to be the maximum sizes of the left and right set respectively
const int M = 4, N = 5;
bool graph[M][N];
bool seen[N];
int matchL[M], matchR[N];
int m, n;

bool bpm(int u) {
    for (int v = 0; v < n; v++) if (graph[u][v]) {
            if (seen[v]) {
                continue;
            }
            seen[v] = true;
            if (matchR[v] < 0 || bpm(matchR[v])) {
                matchL[u] = v;
                matchR[v] = u;
                return true;
            }
        }
    return false;
}

int not_main(int argc, char** argv) {

    cout << "Hello everyone!" << endl;

    // Read input and populate graph[][]
    int graph[M][N] ={
        {12, 4, 6, 6, 10},
        {8, 12, 10, 10, 0},
        {6, 2, 0, 0, 10},
        {0, 8, 6, 6, 8}
    };

    // Set m to be the size of L, n to be the size of R
    memset(matchL, -1, sizeof (matchL));
    memset(matchR, -1, sizeof (matchR));
    int cnt = 0;
    for (int i = 0; i < m; i++) {
        memset(seen, 0, sizeof ( seen));
        if (bpm(i)) cnt++;
    }
    // cnt contains the size of the matching
    // matchL[i] is what left vertex i is matched to (or ­1 if unmatched)
    // matchR[j] is what right vertex j is matched to (or ­1 if unmatched)
    return 0;

}

