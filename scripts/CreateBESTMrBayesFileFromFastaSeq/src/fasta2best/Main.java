/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fasta2best;

/**
 *
 * @author ikramu
 */
public class Main {

    /**
     * This script expects following CLA 1. directory with gene family sequences
     * (in fasta format) 2. file extension for above sequences (may be "fasta"
     * or "seq") 3. Gene to species mapping file
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here        

        if (args.length != 5) {
            showCommandLineInfo();
            System.exit(1);
        }
        
        createBESTFileFromData(args);

    }

    private static void showCommandLineInfo() {
        System.err.println("\nError: Incorrect number of arguments");
        System.err.println("");
        System.err.println("Usage: ");
        System.err.println("java -jar CreateBESTMrBayesFileFromFastaSeq seq_dir seq_ext num_iter out_file");       
        System.err.println("seq_dir:\tdirectory having gene family sequences (in fasta format)");
        System.err.println("seq_ext:\tfile extension for above sequences (for instance \"fasta\" for \"foo.fasta\")");
        System.err.println("num_iter: number of iteration for MrBayes iteration");
        System.err.println("thinning: how often MrBayes MCMC is sampled");
        System.err.println("out_dir: output directory for nexus and MrBayes files");
    }

    private static void createBESTFileFromData(String[] args) {
        String seq_directory = args[0];
        String seq_ext = args[1];        
        int num_iter = Integer.parseInt(args[2]);
        int thinning = Integer.parseInt(args[3]);
        String outdir = args[4];
        
        FastaReader freader = new FastaReader(seq_directory, seq_ext, num_iter, thinning, outdir);        
        freader.writeOutputFile(outdir);
        
    }
}
