/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package loosc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author ikramu
 */
public class SequenceCopier {

    String[] progArgs;
    private static final String FILE_TEXT_EXT = ".seq";
    HashMap<String, String> trimmedFile;

    public SequenceCopier(String[] args) {
        progArgs = args;
        trimmedFile = new HashMap<>();
    }

    public void processSequences() {
        try {
            File dirFile = getFileFromString(progArgs[0]);
            if (!dirFile.isDirectory()) {
                System.err.println("Program expects directory path, not the file path");
                System.err.println("Exiting now...");
                System.exit(1);
            }

            File[] files = dirFile.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(FILE_TEXT_EXT);
                }
            });
            Arrays.sort(files);

            int numFiles = Integer.parseInt(progArgs[1]);
            int curFile = 0, availFile = 0;            

            // Note that we are only using the file count
            for (int i = 0; i < files.length; ++i) {
                String nextFile = progArgs[0] + "/gene" + availFile + ".seq";
                availFile++;
                File nFile = new File(nextFile);
                String contents = "";
                if (nFile.exists()) {
                    contents = getSequenceFileContents(nFile);
                    if (!contents.equals("")) {
                        String appFile = progArgs[3] + "/" + progArgs[5] + nFile.getName();
                        trimmedFile.put(appFile, contents);
                        curFile++;
                        System.out.println(nextFile + " has been copied to " + appFile + " excluding sequence " + progArgs[2]);
                        if (curFile == numFiles) {
                            break;
                        }
                    }
                } else {
                    // we haven't found the file, so don't need to increment the "files" increment
                    --i;
                }
            }

            // write to output directory
            writeSequencesToOutDirectory();

        } catch (Exception e) {
            System.err.println("Some error in processSequences()");
            System.err.println("Reason: " + e.getMessage());
            System.exit(1);
        }
    }

    private File getFileFromString(String path) throws IOException {
        File f = new File(path);
        return f;
    }

    private String getSequenceFileContents(File sFile) throws Exception {
        BufferedReader buf = new BufferedReader(new FileReader(sFile));
        StringBuilder sb = new StringBuilder();
        String line = "";
        int numSeq = 0;
        boolean all_fam = false;
        if(progArgs[6].equalsIgnoreCase("Y")) {
            all_fam = true;
        }

        boolean isRemoved = false;

        while ((line = buf.readLine()) != null) {
            line = line.trim();
            if (line.equals("")) {
                continue;
            }

            if (line.charAt(0) == '>') {
                // we exploit the fact that multigene family will have at least one 
                // gene with more than one family, so in gene_j_i, i > 0
                int counter = Integer.parseInt(line.charAt(line.length() - 1) + "");
                if (!all_fam && counter > 0) {
                    // put something to negate the if condition after the loop
                    numSeq = 1110000;
                    break;
                }

                if (line.substring(1).contains(progArgs[2])) {
                    isRemoved = true;
                } else {
                    if (isRemoved) {
                        isRemoved = false;
                    }

                    sb.append(line);
                    sb.append("\n");
                }
                numSeq++;
            } else {
                if (!isRemoved) {
                    sb.append(line);
                    sb.append("\n");
                }
            }
        }

        if(all_fam) {
            return sb.toString();
        } else  if ((Integer.parseInt(progArgs[4]) == numSeq) && (!all_fam)) {
            return sb.toString();
        } else {
            return "";
        }
    }

    private void writeSequencesToOutDirectory() throws IOException {
        Iterator<String> allFiles = trimmedFile.keySet().iterator();

        if (trimmedFile.size() > 0) {
            File fdir = new File(progArgs[3]);
            if (!fdir.exists()) {
                fdir.mkdir();
            }
        }

        while (allFiles.hasNext()) {
            String curFile = allFiles.next();

            writeOneSequence(curFile, trimmedFile.get(curFile));
        }
    }

    private void writeOneSequence(String fileName, String msa) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        bw.write(msa);
        bw.flush();
        bw.close();
    }
}
