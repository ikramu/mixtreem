/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package loosc;

/**
 *
 * @author ikramu
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final int REQ_ARGS = 7;
        if(args.length != REQ_ARGS){
            printUsage(args, REQ_ARGS);
        }
        
        SequenceCopier sc = new SequenceCopier(args);
        sc.processSequences();
    }

    private static void printUsage(String[] args, final int reqArgs) {
        System.err.println("This program copies an MSA file with all the sequences except the seq_to_leave sequence,");
        System.err.println("from src_directory to dest_directory. In total, there are no_of_files files copied");
        System.err.println("");
        System.err.println("Error: " + args.length + " arguments are provided while the program expects " + reqArgs + " arguments.");      
        System.err.println("\nUsage:");
        System.err.println("java -jar LeaveOneOutSequenceCopier src_directory no_of_files seq_to_leave dest_directory num_seq file_prefix_to_add all_families");
        System.err.println("src_directory = Source directory");
        System.err.println("no_of_files = No of files to copy (5, 10 etc)");       
        System.err.println("seq_to_leave = Sequence which need NOT to be copied. The output MSA will consist all sequences except this one");
        System.err.println("dest_directory = Destination directory");
        System.err.println("num_seq = Number of sequences in the source mono-copy gene families. This may be taken as the number of species in species tree.");
        System.err.println("file_prefix_to_add = File prefix to be added to the destination file (to differentiate across different species trees)");
        System.err.println("all_families = Boolean showing if all families (mono-copy and multi-gene) should be copied (Y) or just mono-copy (N)");
        System.err.println("");
        System.exit(1);
    }
}
