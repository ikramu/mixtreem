/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author ikramu
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            printUsage();
            System.exit(1);
        }
        String path = args[0];
        String outfile = args[1];

        ArrayList<String> files = readFile(path);
        HashMap<String, ArrayList<String>> sp_specific_files = decomposeIntoSpSpecificFiles(files);

        Iterator<String> all_sp = sp_specific_files.keySet().iterator();
        ArrayList<Double> sp_likelihoods = new ArrayList<Double>();

        while (all_sp.hasNext()) {
            ArrayList<String> sp_list = sp_specific_files.get(all_sp.next());
            DLRSPosteriorAnalyzer dpa = new DLRSPosteriorAnalyzer(sp_list);
            System.out.println("Overall likelihood for species tree is " + dpa.getOverallSpeciesTreeLikelihood());
            sp_likelihoods.add(dpa.getOverallSpeciesTreeLikelihood());
        }
        
        System.out.println("Writing likelihood to " + outfile);
        writeLikelihoodsToFile(sp_likelihoods, outfile);
    }

    private static void printUsage() {
        System.err.println("Wrong number of arguments.\n");
        System.err.println("Usage: java -jar AnalyzeDLRSPosterior dlrs_dir/dlrs_file_list likelihood_outfile");
        System.err.println("dlrs_dir/dlrs_file_list = either the directory having DLRS posterior files, or a file consisting of such a list");
        System.err.println("likelihood_outfile = File where overall summary, and likelihoods for individual species tree will be written");        
    }

    /*
     * If dfile is a directory, then it would contain all the files
     * If dfile is a file, then it would contain the list of all files
     */
    private static ArrayList<String> readFile(String dfile) {
        ArrayList<String> files = new ArrayList<>();
        try {
            File file = new File(dfile);
            if(!file.exists()) {
                System.err.println("ERROR: There dlrs_dir/dlrs_file_list doesn't exists");
                System.err.println("Please check the file/directory and try again");
                System.exit(1);
            } else if (file.isFile()) {
                BufferedReader buf = new BufferedReader(new FileReader(dfile));
                String line = "";

                while ((line = buf.readLine()) != null) {
                    files.add(line);
                }
            } else if (file.isDirectory()) {
                File[] all_files = file.listFiles();
                for (int i = 0; i < all_files.length; ++i) {
                    files.add(all_files[i].getCanonicalPath());
                }
            } else { 
                System.err.println("ERROR: There is some problem with dlrs_dir file, it neither a file nor a directory");
                System.err.println("Please check the file/directory and try again");
                System.exit(1);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return files;
    }

    private static HashMap<String, ArrayList<String>> decomposeIntoSpSpecificFiles(ArrayList<String> files) {
        HashMap<String, ArrayList<String>> decomposedMap = new HashMap<>();        
        // start with <sep>1_*.mcmc
        int id = 1;

        try {
            for (int i = 0; i < files.size(); ++i) {
                String[] fn = files.get(i).split("/");
                String[] fnp = fn[fn.length - 1].split("\\.");

                if (decomposedMap.containsKey(fnp[0])) {
                    //ArrayList<String> as = decomposedMap.get(fnp[0]);
                    //as.add(files.get(i));
                    decomposedMap.get(fnp[0]).add(files.get(i));
                    //System.out.println(fnp[0] + ", " + files.get(i));
                    //decomposedMap.put(fnp[0], as);
                } else {
                    ArrayList<String> as = new ArrayList<>();
                    as.add(files.get(i));
                    decomposedMap.put(fnp[0], as);
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return decomposedMap;
    }

    private static void writeLikelihoodsToFile(ArrayList<Double> sp_likelihoods, String outfile) {
        Double maximum = Double.NEGATIVE_INFINITY;
        int maximum_stree = -1;
        
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
            
            for(int i = 0; i < sp_likelihoods.size(); ++i) {
                bw.write("Species tree " + (i+1) + " likelihood = " + sp_likelihoods.get(i) + ".\n");
                if(maximum < sp_likelihoods.get(i)) {
                    maximum = sp_likelihoods.get(i);
                    maximum_stree = (i+1);
                }
            }
            bw.write("*************************************************************\n");
            bw.write("Species tree with best likelihood is \n");
            bw.write("Species tree " + maximum_stree + " likelihood = " + maximum + ".\n");
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
