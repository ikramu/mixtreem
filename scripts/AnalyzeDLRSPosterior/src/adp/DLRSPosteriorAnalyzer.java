/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ikramu
 */
public class DLRSPosteriorAnalyzer {

    private String dlrsFile;
    private double burnin;
    private ArrayList<String> files;
    private double overall_likelihood;

    public DLRSPosteriorAnalyzer() {
    }

    public DLRSPosteriorAnalyzer(ArrayList<String> fileList) {
        files = new ArrayList<>(fileList);
        overall_likelihood = 0.0;
        burnin = 0.4;

        //findUnconvergedFiles();
        for (int i = 0; i < files.size(); ++i) {
            System.out.println("Parsing " + files.get(i));
            overall_likelihood += getMaxLikelihoodOfTree(files.get(i));
        }
    }
    
    public double getOverallSpeciesTreeLikelihood() {
        return overall_likelihood;
    }

    public String getDLRSSummary(int fileNo) {

        StringBuilder contents = new StringBuilder();
        StringBuilder command = new StringBuilder();
        command.append(System.getProperty("user.home") + "/scripts/mcmc_analysis -b ");
        command.append(burnin);
        command.append(" -i SubstModel5.substLike,Tree_weights3.EdgeWeightLike,Tree_weights3.Tree_Lengths,Density2.mean,Density2.variance,DupLoss0.birthRate,DupLoss0.deathRate ");
        command.append(files.get(fileNo));
        try {
            String line;
            Process p = Runtime.getRuntime().exec(command.toString());
            int counter = 0;
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                //System.out.println(line);

                if (line.contains("Tree_Model1.Tree")) {
                    counter = 1;
                }
                if (counter == 1) {
                    contents.append(line);
                    contents.append("\n");
                }
            }
            input.close();
        } catch (Exception err) {
            System.err.println(err.getMessage());
        }
        return contents.toString();
    }

    private void readDLRSFiles() {
        try {
            File dfiles = new File(dlrsFile);

            if (dfiles.isDirectory()) {
                File[] all_files = dfiles.listFiles();
                for (int i = 0; i < all_files.length; ++i) {
                    files.add(all_files[i].getPath());
                }
            } else {
                files.add(dlrsFile);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void readFile(String dfile) {
        try {
            BufferedReader buf = new BufferedReader(new FileReader(dfile));
            String line = "";

            while ((line = buf.readLine()) != null) {
                files.add(line);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void findUnconvergedFiles() {
        ArrayList<Integer> uncFiles = new ArrayList<>();

        for (int i = 0; i < files.size(); ++i) {
            String contents = getDLRSSummary(i);
            String[] lines = contents.split("\n");

            // the first two lines are headers, so we get the second line
            String[] tokens = lines[2].split("\t");
            double support = Double.parseDouble(tokens[2]);
            System.out.println("In File " + (i + 1) + ", the MAP gene tree has support of " + (support * 100));
            //if (support < 0.5) {
            //    System.out.println("File " + (i + 1) + " didn't converge");
            //    uncFiles.add(i + 1);
            //}
        }
    }

    private double getMaxLikelihoodOfTree(String file) {
        HashMap<Integer, Integer> treeCounterMap = new HashMap<>();

        StringBuilder command = new StringBuilder();
        ArrayList<Double> likelihoods = new ArrayList<>();
        ArrayList<Integer> treeIndex = new ArrayList<>();        
        command.append(System.getProperty("user.home") + "/scripts/mcmc_analysis -b ");
        command.append(burnin);
        command.append(" -codatrees -i SubstModel5.substLike,Tree_weights3.EdgeWeightLike,Tree_weights3.Tree_Lengths,Density2.mean,Density2.variance,DupLoss0.birthRate,DupLoss0.deathRate ");
        command.append(file);
        try {
            String line;
            Process p = Runtime.getRuntime().exec(command.toString());
            int numSamples = 0;
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            line = input.readLine();
            if (line == null) {
                return 0.0;
            }

            while ((line = input.readLine()) != null) {
                numSamples++;
                //System.out.println(line);
                String[] mainTokens = line.split("\t");
                String[] llTokens = mainTokens[0].split(" ");
                int treeId = Integer.parseInt(mainTokens[2]);
                likelihoods.add(Double.parseDouble(llTokens[2]));
                treeIndex.add(treeId);

                if (treeCounterMap.containsKey(treeId)) {
                    int cc = treeCounterMap.get(treeId) + 1;
                    treeCounterMap.put(treeId, cc);
                } else {
                    treeCounterMap.put(treeId, 1);
                }
            }
            input.close();

            Map.Entry<Integer, Integer> maxEntry = getMaxHashEntry(treeCounterMap);
            double treeSupport = (maxEntry.getValue() + 0.0) / numSamples;
            //System.out.println("The MAP tree support is " + treeSupport);
            
            double maxll = getMaxLikeForTree(likelihoods, treeIndex, maxEntry.getKey());
            //System.out.println("The maximum likelihood for tree " + maxEntry.getKey() + " is " + maxll);
            return maxll;
        } catch (Exception err) {
            System.err.println(err.getMessage());
        }
        return 0.0;
    }

    private Map.Entry<Integer, Integer> getMaxHashEntry(HashMap<Integer, Integer> treeCounterMap) {
        Map.Entry<Integer, Integer> maxEntry = null;

        for (Map.Entry<Integer, Integer> entry : treeCounterMap.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }

        return maxEntry;
    }

    private double getMaxLikeForTree(ArrayList<Double> likelihoods, ArrayList<Integer> treeIndex, Integer treeId) {
        ArrayList<Double> maxTreeLike = new ArrayList<>();
        
        double max = Double.NEGATIVE_INFINITY;
        
        for(int i = 0; i < likelihoods.size(); ++i){
            if( (treeIndex.get(i) == treeId) && ( max < likelihoods.get(i)) ){
                max = likelihoods.get(i);
            }
        }
        
        return max;
    }
}
