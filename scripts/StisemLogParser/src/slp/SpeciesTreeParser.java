/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package slp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import org.apache.commons.lang3.mutable.MutableInt;
//import org.apache.commons.lang3.mutable.mu


/**
 *
 * @author ikramu
 */
public class SpeciesTreeParser {
    
    /*
     * The format for file is 
     * ##############################
     * ===Species-Trees===
     * tree1
     * likelihood1
     * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     * tree2
     * likelihood2
     * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     * More of them...
     * ##############################
     */
    void parseSpeciesTrees(String[] args){
        
        /* we expect two arguments */ 
        if(args.length != 2){
            System.err.println("Incorrect number of arguments");
            System.err.println("\nUsage: java -jar StisemLogParser.jar logFile outFile");
            System.err.println("Where");
            System.err.println("logFile = Stisem log file");
            System.err.println("outFile = Output file to write trees\n");
            System.exit(1);
        }
        
        String logFile = args[0];
        String outFile = args[1];
        
        ArrayList<String> contents = getArrayListFromTextFile(logFile);
        if(contents == null){
            System.err.println("Couldn't read the logfile. Exiting now...");
            System.exit(1);
        }
                
        MutableInt lastHash, secondLastHash;
        lastHash = new MutableInt(-1);
        secondLastHash = new MutableInt(-1);
        getLastTwoHashLines(contents, secondLastHash, lastHash);
        
        int lh = lastHash.getValue();
        int slh = secondLastHash.getValue();
        
        if((lh == -1) && (slh == -1)){
            System.err.println("Couldn't find the start/stop markers for likelihoods. Exiting now...");
            System.exit(1);
        }
        
        StringBuilder trees = new StringBuilder();       
        for(int i = slh+2; i < lh; i=i+3){
            trees.append(contents.get(i));
            trees.append("\n");
            System.out.println(contents.get(i));
        }
        
        /* write the trees to output file */
        InputOutput io = new InputOutput();
        io.writeToTextFile(trees.toString(), outFile);
    }
    
    private ArrayList<String> getArrayListFromTextFile(String file){
        ArrayList<String> array = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            
            while( line != null){
                array.add(line);
                line = br.readLine();
            }
        } catch (Exception e) {
            System.err.println("Exception raised in getArrayListFromTextFile()");
            System.err.println("Reason: " + e.getMessage());
            return null;
        }
        
        return array; 
    }
    
    private void getLastTwoHashLines(ArrayList<String> contents, MutableInt hash1, MutableInt hash2){
        int h1, h2;
        h1 = h2 = -1;
        
        for(int i = contents.size()-1; i >= 0; --i ){
            if(contents.get(i).contains("#############")){
                if(h2 == -1) {
                    h2 = i;
                } else if(h1 == -1) {
                    h1 = i;
                } 
                
                /* if both h1 and h2 are set, then exit out of loop */
                if((h1 != -1) && (h2 != -1)){
                    break;
                }
            }
        }
        
        hash1.setValue(h1);
        hash2.setValue(h2);
    }
}
