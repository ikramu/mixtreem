/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package slp;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 *
 * @author ikramu
 */
public class InputOutput {
    void writeToTextFile(String contents, String outfile){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
            bw.write(contents);
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("Some error in writing in writeToTextFile(). Exiting now...");
            System.exit(1);
        }
    }
}
