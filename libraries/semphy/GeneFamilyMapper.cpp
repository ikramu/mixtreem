
#include "GeneFamilyMapper.h"

GeneFamilyMapper::GeneFamilyMapper() {
    
}

GeneFamilyMapper::GeneFamilyMapper(const std::string sn, int sid, int sl, int sp) {
    seqName = sn;
    seqId = sid;
    seqLength = sl;
    startPosition = sp;
}

GeneFamilyMapper::GeneFamilyMapper(const GeneFamilyMapper& orig) {
    this->seqName = orig.seqName;
    this->seqLength = orig.seqLength;
    this->startPosition = orig.startPosition;
}

GeneFamilyMapper::~GeneFamilyMapper() {
    
}