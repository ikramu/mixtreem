/* 
 * File:   GeneFamilyMapper.h
 * Author: ikramu
 *
 * Created on November 22, 2013, 8:07 PM
 */

#ifndef SEQUENCEMAPPER_H
#define	SEQUENCEMAPPER_H

#include <string>

class GeneFamilyMapper {
public:
    GeneFamilyMapper();
    GeneFamilyMapper(const std::string sn, int sid, int sl, int sp);
    GeneFamilyMapper(const GeneFamilyMapper& orig);
    virtual ~GeneFamilyMapper();

    void SetStartPosition(int startPosition) {
        this->startPosition = startPosition;
    }

    int GetStartPosition() const {
        return startPosition;
    }

    void SetSeqLength(int seqLength) {
        this->seqLength = seqLength;
    }

    int GetSeqLength() const {
        return seqLength;
    }

    void SetSeqName(std::string seqName) {
        this->seqName = seqName;
    }

    std::string GetSeqName() const {
        return seqName;
    }

    void SetSeqId(int seqId) {
        this->seqId = seqId;
    }

    int GetSeqId() const {
        return seqId;
    }
    
private:
    std::string seqName;
    int seqId;
    int seqLength;
    int startPosition;    
};

#endif	/* SEQUENCEMAPPER_H */

