/* 
 * File:   EdmondsAlgorithm.h
 * Author: ikramu
 *
 * Created on June 19, 2013, 10:12 PM
 */

#ifndef EDMONDSALGORITHM_H
#define EDMONDSALGORITHM_H
//#ifndef MST2EDMOND_H
//#define	MST2EDMOND_H
#include "definitions.h"
#include "tree.h"
//#include "Tre"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "/usr/include/edmonds_optimum_branching.hpp"
#include "rearangeTree.h"

// Define a directed graph type that associates a weight with each
// edge. We store the weights using internal properties as described
// in BGL.
typedef boost::property<boost::edge_weight_t, double>       EdgeProperty;
typedef boost::adjacency_list<boost::listS,
                              boost::vecS,
                              boost::directedS,
                              boost::no_property,
                              EdgeProperty>                 Graph;
typedef boost::graph_traits<Graph>::vertex_descriptor       Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor         Edge;

typedef std::pair<Edge, bool> myEdge;

using namespace boost::foreach;

class EdmondsAlgorithm {
public:
    EdmondsAlgorithm(VVdouble pt, tree t); 
    EdmondsAlgorithm(const EdmondsAlgorithm& orig);
    virtual ~EdmondsAlgorithm();
    
    rearrangeTree::pairSet runEdmondsAlgorithm();
private:
    void convertNegativeWeightsToPositiveInPenaltyTable();
    tree constructTreeFromBranching(std::vector<Edge> branching, Graph G);
    
    VVdouble penaltyTable;
    int minValue;
    tree geneTree;    
    tree edmondsTree;
};

#endif	/* MST2EDMOND_H */

