/* 
 * File:   EdmondsAlgorithm.cpp
 * Author: ikramu
 * 
 * Created on June 19, 2013, 10:12 PM
 */

#include <iomanip>

#include "EdmondsAlgorithm.h"
#include "rearangeTree.h"

EdmondsAlgorithm::EdmondsAlgorithm(VVdouble pt, const tree t) {
    //this->penaltyTable(pt);

    // assign memory
    this->penaltyTable.resize(pt.size());
    for (int i = 0; i < pt[0].size(); ++i)
        this->penaltyTable[i].resize(pt[0].size());

    for (int i = 0; i < pt.size(); i++) {
        //this->penaltyTable[i].resize(pt[0].size());
        for (int j = (i + 1); j < pt[0].size(); j++) {
            this->penaltyTable[i][j] = pt[i][j];
            this->penaltyTable[j][i] = pt[i][j];
        }
    }
    /*
    cout << "dimensions of penaltyTable are (" << penaltyTable.size() << "," << penaltyTable[0].size() << ")" << endl;
    for(int i = 0; i < penaltyTable.size(); ++i){        
        for(int j = (i+1); j < penaltyTable.size(); ++j){
            this->penaltyTable[j][i] = this->penaltyTable[i][j];
        }        
    }
     */

    convertNegativeWeightsToPositiveInPenaltyTable();
    // assign t to geneTree
    geneTree = t;
}

EdmondsAlgorithm::EdmondsAlgorithm(const EdmondsAlgorithm& orig) {
}

EdmondsAlgorithm::~EdmondsAlgorithm() {
}

rearrangeTree::pairSet EdmondsAlgorithm::runEdmondsAlgorithm() {
    rearrangeTree::pairSet pairs;
    int numVertices = geneTree.getNodesNum();
    Graph G(numVertices);

    /* Don't know the utility but seems important to do (copied from Ali) */
    std::vector<Vertex> the_vertices;

    BOOST_FOREACH(Vertex v, vertices(G)) {
        the_vertices.push_back(v);
    }

    /* 
     * From undirected to directed 
     * Use same values for both direction for each 
     * vertex pair except the one involving a leaf
     * where direct it towards the leaf
     */
    vector<tree::nodeP> allNodes;
    geneTree.getAllNodes(allNodes, geneTree.getRoot());

    /*
    for (unsigned i = 0; i < allNodes.size(); ++i) {
        if (allNodes[i]->isLeaf())
            cout << "leaf " << i << " has id " << allNodes[i]->id() << endl;
        else
            cout << "internal vertex " << i << " has id " << allNodes[i]->id() << endl;
    }
     */

    for (int i = 0; i < allNodes.size(); i++) {
        for (int j = i + 1; j < allNodes.size(); j++) {

            int iNodeID = allNodes[i]->id();
            int jNodeID = allNodes[j]->id();

            /** IMPORTANT 20131228 20:58
             * previously, I was using penaltyTable[i][j] which was wrong, I should
             * be using penaltyTable[iNodeID][jNodeID] as I am using now :)
             */

            /* if both nodes is leaves, then leave */
            if (allNodes[i]->isLeaf() && allNodes[j]->isLeaf()) {
                myEdge me = add_edge(the_vertices[iNodeID], the_vertices[jNodeID], 0.0, G);
                //cout << boost::source(me, G) << endl;
                myEdge me2 = add_edge(the_vertices[jNodeID], the_vertices[iNodeID], 0.0, G);
                /*
                boost::property_map<Graph, boost::edge_weight_t>::type myweights =
                        get(boost::edge_weight_t(), G);
                cout << "weight (" << boost::source(me.first, G)
                        << ", " << boost::target(me.first, G) <<
                        ") = " << get(myweights, me.first) << endl;
                cout << "weight (" << boost::source(me2.first, G)
                        << ", " << boost::target(me2.first, G) <<
                        ") = " << get(myweights, me2.first) << endl;
                 */
                //continue;
            }
                /* if ith node is a leaf */
            else if (allNodes[i]->isLeaf() && !(allNodes[j]->isLeaf())) {
                //add_edge(the_vertices[jNodeID], the_vertices[iNodeID], penaltyTable[i][j], G);
                myEdge me = add_edge(the_vertices[jNodeID], the_vertices[iNodeID], penaltyTable[iNodeID][jNodeID], G);
                myEdge me2 = add_edge(the_vertices[iNodeID], the_vertices[jNodeID], 0.0, G);
                //cout << "(" << jNodeID << "->" << iNodeID << ") = " << penaltyTable[iNodeID][jNodeID] << endl;
                /*
                boost::property_map<Graph, boost::edge_weight_t>::type myweights =
                        get(boost::edge_weight_t(), G);
                cout << "weight (" << boost::source(me.first, G)
                        << ", " << boost::target(me.first, G) <<
                        ") = " << get(myweights, me.first) << endl;
                cout << "weight (" << boost::source(me2.first, G)
                        << ", " << boost::target(me2.first, G) <<
                        ") = " << get(myweights, me2.first) << endl;
                 */
            }                /* if jth node is a leaf */
            else if (allNodes[j]->isLeaf() && !(allNodes[i]->isLeaf())) {
                //add_edge(the_vertices[iNodeID], the_vertices[jNodeID], penaltyTable[i][j], G);
                myEdge me = add_edge(the_vertices[iNodeID], the_vertices[jNodeID], penaltyTable[iNodeID][jNodeID], G);
                myEdge me2 = add_edge(the_vertices[jNodeID], the_vertices[iNodeID], 0.0, G);
                /*
                boost::property_map<Graph, boost::edge_weight_t>::type myweights =
                        get(boost::edge_weight_t(), G);
                cout << "weight (" << boost::source(me.first, G)
                        << ", " << boost::target(me.first, G) <<
                        ") = " << get(myweights, me.first) << endl;
                cout << "weight (" << boost::source(me2.first, G)
                        << ", " << boost::target(me2.first, G) <<
                        ") = " << get(myweights, me2.first) << endl;
                 */
            }                /* if none is a leaf */
            else {
                //add_edge(the_vertices[iNodeID], the_vertices[jNodeID], penaltyTable[i][j], G);
                //add_edge(the_vertices[jNodeID], the_vertices[iNodeID], penaltyTable[i][j], G);
                myEdge me = add_edge(the_vertices[iNodeID], the_vertices[jNodeID], penaltyTable[iNodeID][jNodeID], G);
                myEdge me2 = add_edge(the_vertices[jNodeID], the_vertices[iNodeID], penaltyTable[iNodeID][jNodeID], G);
                //cout << "Node " << i << " = " << allNodes[i]->id() << " and Node " << j << "= " << allNodes[j]->id() << endl;
                //cout << "(" << iNodeID << "->" << jNodeID << ") = " << penaltyTable[i][j] << endl;
                /*
                boost::property_map<Graph, boost::edge_weight_t>::type myweights =
                        get(boost::edge_weight_t(), G);
                cout << "weight (" << boost::source(me.first, G)
                        << ", " << boost::target(me.first, G) <<
                        ") = " << get(myweights, me.first) << endl;
                //cout << "(" << jNodeID << "->" << iNodeID << ") = " << penaltyTable[i][j] << endl;
                cout << "weight (" << boost::source(me2.first, G)
                        << ", " << boost::target(me2.first, G) <<
                        ") = " << get(myweights, me2.first) << endl;
                 */
                //cout << "Node " << j << " = " << allNodes[j]->id() << " and Node " << i << "= " << allNodes[i]->id() << endl;
            }
        }
    }

    // This is how we can get a property map that gives the weights of
    // the edges.
    boost::property_map<Graph, boost::edge_weight_t>::type weights =
            get(boost::edge_weight_t(), G);

    // This is how we can get a property map mapping the vertices to
    // integer indices.
    boost::property_map<Graph, boost::vertex_index_t>::type vertex_indices =
            get(boost::vertex_index_t(), G);

    /*
    // Print the graph (or rather the edges of the graph).
    std::cout << "This is the graph:\n";
    std::cout.unsetf(std::ios::floatfield);
    std::cout << std::setprecision(5);

    cout << "number of edges are " << num_edges(G) << endl;
    BOOST_FOREACH(Edge e, edges(G)) {
        std::cout << "(" << boost::source(e, G) << ", "
                << boost::target(e, G) << ")\t"
                << get(weights, e) << "\n";
    }
     */
    
    // Find the maximum branching.
    std::vector<Edge> branching;
    edmonds_optimum_branching < true, false, false > (G,
            vertex_indices,
            weights,
            static_cast<Vertex *> (0),
            static_cast<Vertex *> (0),
            std::back_inserter(branching));

    // Print the edges of the maximum branching
    //std::cout << "This is the maximum branching\n";
    pairs.clear(); //branching.size()
    int score = 0.0;

    BOOST_FOREACH(Edge e, branching) {
        std::pair<int, int> p(boost::source(e, G), boost::target(e, G));
        pairs.insert(p);
        //std::cout << "(" << boost::source(e, G) << ", "
        //        << boost::target(e, G) << ")\t"
        //        << get(weights, e) << "\n";
        score += get(weights, e);
    }

    cout << "score from Edmonds' algorithm is " << score << endl;
    return pairs;
}

tree EdmondsAlgorithm::constructTreeFromBranching(std::vector<Edge> branching, Graph G) {
    // first put all edges into multimap 
    multimap<Vertex, std::vector<Edge> > mmap;

    BOOST_FOREACH(Edge e, branching) {
        Vertex src = boost::source(e, G);
        Vertex sink = boost::target(e, G);
        cout << "source is " << src << " and sink is " << sink << endl;
        //mmap.insert(pair<Vertex, std::vector<Edge> >(boost::source(e, G),e));
    }
    tree t;
    return t;
}

void EdmondsAlgorithm::convertNegativeWeightsToPositiveInPenaltyTable() {
    // initialize minimum to big value
    int vHighFloat = 100000000.00;
    int min = vHighFloat;

    for (int i = 0; i < penaltyTable.size(); i++) {
        for (int j = (i + 1); j < penaltyTable[0].size(); j++) {
            if (min > penaltyTable[i][j])
                min = penaltyTable[i][j];
        }
    }
    //cout << "minimum value in the penalty table is " << min << endl;
    if (min == vHighFloat)
        this->minValue = 0;
    else
        this->minValue = min;

    /*
    // add the offset to penaltyTable to make all values positive
    for (int i = 0; i < penaltyTable.size(); i++) {
        for (int j = 0; j < penaltyTable[0].size(); j++) {
            penaltyTable[i][j] -= this->minValue;
        }
    }
     */
}

