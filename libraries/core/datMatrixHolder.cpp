// $Id: datMatrixHolder.cpp,v 2.6 2005-06-12 12:44:18 ninio Exp $

#include "datMatrixHolder.h"

const datMatrixString datMatrixHolder::cpREV45(
#include "cpREV45.dat.q"
); 
const datMatrixString datMatrixHolder::dayhoff(
#include "dayhoff.dat.q"
);
const datMatrixString datMatrixHolder::jones(
#include "jones.dat.q"
);
const datMatrixString datMatrixHolder::mtREV24(
#include "mtREV24.dat.q"
);
const datMatrixString datMatrixHolder::wag(
#include "wag.dat.q"
);

