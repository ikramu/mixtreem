/* 
 * File:   MixTreEMLogParser.cpp
 * Author: ikramu
 * 
 * Created on February 26, 2014, 7:31 PM
 */

#include <stdlib.h>
#include <string.h>

#include "treeUtil.h"

#include "MixTreEMLogParser.h"
#include "Reader.h"

MixTreEMLogParser::MixTreEMLogParser(string lf, int soa) {
    log_file = lf;
    sizeOfAlp = soa;
}

MixTreEMLogParser::MixTreEMLogParser(const MixTreEMLogParser& orig) {
}

MixTreEMLogParser::~MixTreEMLogParser() {
}

gene_families MixTreEMLogParser::getAllGeneFamilies() {
    gene_families all_families;
    s_vector fnames;

    std::ifstream infile(log_file.c_str());
    if (!infile.good()) {
        cerr << "can't open " << log_file << endl;
        cerr << "exiting now..." << endl;
        exit(EXIT_FAILURE);
    }

    std::string line;
    int counter = 0;
    while (std::getline(infile, line)) {
        if (line == "Gene Family Files") {
            cout << line << endl;
            counter++;
            continue;
        }
        if (counter == 1) {
            counter++;
            continue;
        }

        if ((counter == 2) && (line == "##############################")) {
            break;
        }
        if (counter == 2) {
            cout << line << endl;
            if (line != "")
                fnames.push_back(line);
        }
    }

    for (int i = 0; i < fnames.size(); ++i) {
        if (fnames[i] != "") {
            sequenceContainer mysc = reader.readSingleSequenceFile(sizeOfAlp, fnames[i]);
            mysc.addGeneralRemark(fnames[i]);
            all_families.push_back(mysc);
        }
    }

    return all_families;
}

bool MixTreEMLogParser::sptree_already_exists(tree *t, t_vector trees, int size) {
    for (int i = 0; i < size; ++i) {
        if (sameTreeTolopogy(*t, *trees[i]))
            return true;
    }
    return false;
}

t_vector MixTreEMLogParser::getAllSpeciesTrees() {
    t_vector all_trees;
    s_vector fnames;

    std::ifstream infile(log_file.c_str());
    if (!infile.good()) {
        cerr << "can't open " << log_file << endl;
        cerr << "exiting now..." << endl;
        exit(EXIT_FAILURE);
    }

    std::string line;
    int counter = 0;
    while (std::getline(infile, line)) {
        if (line == "Species Trees Files") {
            cout << line << endl;
            counter++;
            continue;
        }
        if (counter == 1) {
            counter++;
            continue;
        }

        if ((counter == 2) && (line == "##############################")) {
            break;
        }
        if (counter == 2) {
            cout << line << endl;
            if (line != "")
                fnames.push_back(line);
        }
    }

    all_trees.resize(fnames.size(), NULL);

    for (int i = 0; i < fnames.size(); ++i) {
        cout << "reading " << fnames[i] << endl;
        tree *t = new tree(fnames[i]);
        if (!sptree_already_exists(t,  all_trees, i)) {            
            all_trees[i] = new tree(fnames[i]);
        } else {
            cout << "species tree # " << (i+1) << " already exists, so not adding the duplicate" << endl;
        }
        delete(t);
    }

    return all_trees;
}

dd_vector MixTreEMLogParser::getAllResponsibilityScores() {
    s_vector complete_file;
    dd_vector all_scores;

    std::ifstream infile(log_file.c_str());
    if (!infile.good()) {
        cerr << "can't open " << log_file << endl;
        cerr << "exiting now..." << endl;
        exit(EXIT_FAILURE);
    }

    int last_rscore_line = 0;
    std::string line;
    int counter = 0;
    while (std::getline(infile, line)) {
        complete_file.push_back(line);
        counter++;

        // we need last Responsibility score line
        if (line == "===Responsibility-Scores===") {
            last_rscore_line = counter;
        }
    }

    cout << "number of lines are " << counter << " and rscore_line is " << last_rscore_line << endl;

    for (int i = last_rscore_line;; ++i) {
        if ((complete_file[i] == "") || (complete_file[i] == "##############################")) {
            break;
        }
        d_vector rs;
        char *pch;
        char *l = strdup(complete_file[i].c_str());
        pch = strtok(l, ",");
        while (pch != NULL) {
            //printf("%s\n", pch);
            rs.push_back(atof(pch));
            pch = strtok(NULL, " ,");
        }
        all_scores.push_back(rs);
        free(l);
    }

    return all_scores;
}

s_vector MixTreEMLogParser::getAllMixTreEMSpeciesTree() {
    s_vector complete_file;
    s_vector all_trees;

    std::ifstream infile(log_file.c_str());
    if (!infile.good()) {
        cerr << "can't open " << log_file << endl;
        cerr << "exiting now..." << endl;
        exit(EXIT_FAILURE);
    }

    int last_sptree_line = 0;
    std::string line;
    int counter = 0;
    while (std::getline(infile, line)) {
        complete_file.push_back(line);
        counter++;

        // we need last Responsibility score line
        if (line == "===Species-Trees===") {
            last_sptree_line = counter;
        }
    }   

    int i = last_sptree_line;
    while(true) {
        if ((complete_file[i] == "") || (complete_file[i] == "##############################")) {
            break;
        }
        
        all_trees.push_back(complete_file[i]);
        // new two lines are (i) likelihood, and (ii) @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@, so skip them
        i = i + 3;
    }

    return all_trees;
}

