# Make sure the compiler can find include files from our Hello library. 
include_directories (${STISEM_SOURCE_DIR}/libraries/core) 
include_directories (${STISEM_SOURCE_DIR}/libraries/semphy) 

# Make sure the linker can find the Hello library once it is built. 
link_directories (${STISEM_BINARY_DIR}/libraries/core) 
link_directories (${STISEM_BINARY_DIR}/libraries/semphy)     

# Add executable called "helloDemo" that is built from the source files 
# "demo.cxx" and "demo_b.cxx". The extensions are automatically found. 
add_executable (sp_tree_from_stisem_log 
main.cpp
GlobalConstants.h
Reader.cpp        
MixTreEMLogParser.cpp        
) 

# Link the executable to the Hello library. 
target_link_libraries (sp_tree_from_stisem_log semphy core) 
