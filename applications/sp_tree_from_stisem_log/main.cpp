/* 
 * File:   main.cpp
 * Author: ikramu
 *
 * Created on February 26, 2014, 7:05 PM
 */

#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <errno.h>
#include <iosfwd>
#include "errorMsg.h"
#include "MixTreEMLogParser.h"
#include "Reader.h"
#include "mainSemphy.h"
#include "bblEM.h"
#include "semphy_cmdline.h"

using namespace std;

void write_species_trees(s_vector strees, string outdirectory);

/*
 * 
 */
int main(int argc, char** argv) {

    if (argc < 3) {

        cerr << "Error: wrong number of parameters" << endl;
        cerr << "Usage: " << argv[0] << " mixtreem_log_file out_tree_dir" << endl;
        cerr << "mixtreem_log_file = Log file containing, among others, the responsibility scores and gene family names" << endl;
        cerr << "out_tree_dir = directory where species tree, from mixtreem_log_file, will be written" << endl;
        exit(EXIT_FAILURE);
    }

    string pfile(argv[1]);
    string outdirectory(argv[2]);

    int sizeOfAlp = 20; // right now, we are doing it for protein only
    MixTreEMLogParser parser(pfile, sizeOfAlp);

    //t_vector strees = parser.getAllSpeciesTrees();
    s_vector strees = parser.getAllMixTreEMSpeciesTree();
    write_species_trees(strees, outdirectory);
    return 0;
}

void write_species_trees(s_vector strees, string outfile) {
    // first remove all the previous species tree files.
    char spno[50];
    string fname("/mixtreem_sp.trees");
    sprintf(spno, "%s", "/sp_*.tree");
    string command("rm -f ");
    command.append(outfile);    
    system(command.c_str());
    
    ofstream out(outfile.c_str());
    for (int i = 0; i < strees.size(); ++i) {
        out << strees[i] << endl;
    }
    out.flush();
    out.close();
}

