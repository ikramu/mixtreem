/* 
 * File:   ResponsibilityScore.cpp
 * Author: ikramu
 * 
 * Created on August 2, 2013, 3:29 PM
 */

#include <numeric>
#include <cmath>
#include <assert.h>
#include <complex>
#include <algorithm>

#include "ResponsibilityScore.h"

#define USE_SINGLE_SITE false

/*
struct myclass {

    double operator()(double first, double second) {
        double greater, smaller;
        if (first >= second) {
            greater = first;
            smaller = second;
        } else {
            greater = second;
            smaller = first;
        }

        double result = 0.0;
        double base = exp(1);

        // log_b(a+c) = log_b(a) + log_b(1 + b^(log_b(c) - log_b(a)))
        result = smaller + log(1 + pow(base, greater - smaller));
        return result;
    }
} addlogprob;
 */

ResponsibilityScore::ResponsibilityScore() {
}

ResponsibilityScore::ResponsibilityScore(gene_families gf, t_vector sp_top,
        stochasticProcess sprocess) : sp(sprocess) {
    initVariables(gf, sp_top);
}

ResponsibilityScore::ResponsibilityScore(const ResponsibilityScore& orig) {
}

ResponsibilityScore::~ResponsibilityScore() {
}

void ResponsibilityScore::initVariables(gene_families gf, t_vector sp_top) {
    /* allocate space to all variables */
    g_families.resize(gf.size());
    tree_topologies.resize(sp_top.size());
    tree_lk_ratio.resize(sp_top.size());
    resp_score.resize(g_families.size());

    for (int i = 0; i < resp_score.size(); ++i)
        resp_score[i].resize(tree_topologies.size());

    /* copy gene families */
    g_families = gf;
    tree_topologies = sp_top;
}

void ResponsibilityScore::computeLikelihoodsAllTreesAllFamilies() {
    for (int findex = 0; findex < g_families.size(); ++findex) {
        computeLikelihoodsOneFamilyAllTrees(findex);
    }

    /* 
     * first normalize the responsiblity score by each family 
     * so that for each family fi, the sum of its likelihoods over 
     * all species trees equals one
     */
    normalizeRespScoreByGeneFamily();
    //normalizeRespScoreByGeneFamilyAlternate();

    /*
     * now normalize by each species tree
     */
    //normalizeRespScoreBySpeciesTree();
}

void ResponsibilityScore::computeLikelihoodsOneFamilyAllTrees(int famindex) {
    assert(g_families.size() > 0 && "No gene families are present. Exiting...");
    Vdouble lkpp;
    lkpp.resize(g_families[0].seqLen());
    //double total;

    sequenceContainer fam = g_families[famindex];
    double rel_score;

    cout << "computing likelihood of species tree # 1 for family " <<
            (famindex + 1);
    rel_score = likelihoodComputation::computeLikelihoodAndLikelihoodPerPosition(
            fam, *tree_topologies[0], sp, lkpp);
    cout << " = " << rel_score << endl;

    // using full likelihood as score
    rel_score = rel_score;
    // using 'nth' root of likelihood where n is number of sites
    if (USE_SINGLE_SITE)
        rel_score = rel_score / fam.seqLen();
    resp_score[famindex][0] = rel_score;
    //total = rel_score;

    for (int tindex = 1; tindex < tree_topologies.size(); ++tindex) {
        cout << "computing likelihood of species tree # " << (tindex + 1) << " for family " <<
                (famindex + 1);
        double score =
                likelihoodComputation::computeLikelihoodAndLikelihoodPerPosition(
                fam, *tree_topologies[tindex], sp, lkpp);

        // using full likelihood as score
        rel_score = score;
        // using 'nth' root of likelihood where n is number of sites
        if (USE_SINGLE_SITE)
            rel_score = rel_score / fam.seqLen();
        //cout << "tree " << tindex << endl;

        cout << " = " << score << endl;
        //cout << "; Per site likelihood is " << rel_score << endl;
        resp_score[famindex][tindex] = rel_score;
        //resp_score[famindex][tindex] = score;

        /* due to nature of formula, we send greater as first argument and less as second */
        //if (total > rel_score)
        //    total = getSumOfLogLikelihood(total, rel_score);
        //else
        //    total = getSumOfLogLikelihood(rel_score, total);
    }
}

/*
 * compute the "relative" importance of each famliy i for each tree j
 * in terms of ith "suitability" for jth tree. 
 * Important: due to very low values of likelihoods, we are approximating it
 * with per site likelihood computed as log-likelihood/seqLength i.e. taking 
 * nth root where n is sequence length
 */
void ResponsibilityScore::normalizeRespScoreByGeneFamily() {

    /* TODO: remove the error in normalization here... */
    double row_sum[resp_score.size()];
    double row_max[resp_score.size()];
    /* compute by-row total for each species tree 
     * note: we have to change back from log 
     */
    
    // stage 1: find the max of each row
    for (int i = 0; i < resp_score.size(); ++i) {        
        row_max[i] = resp_score[i][0];
        //cout << resp_score[i][0];
        for (int j = 1; j < resp_score[0].size(); ++j) {
            //cout << ", " << resp_score[i][j];
            double current = resp_score[i][j];            
            if(row_max[i] < current)
                row_max[i] = current;
        }
        //cout << endl;
    }

    // stage 2: subtract row_max[i] from each row i, so atleast 
    // one value is 1 (to circumvent numerical problems)
    resp_score_norm_by_family.resize(resp_score.size());
    for (int i = 0; i < resp_score.size(); ++i) {        
        resp_score_norm_by_family[i].resize(resp_score[0].size());
        resp_score_norm_by_family[i][0] = resp_score[i][0] - row_max[i];
        double total = resp_score_norm_by_family[i][0];

        for (int j = 1; j < resp_score[0].size(); ++j) {
            resp_score_norm_by_family[i][j] = resp_score[i][j] - row_max[i]; //row_sum[i];
            total = getSumOfLogLikelihood(total, resp_score_norm_by_family[i][j]);
        }
        row_sum[i] = total;
        //cout << row_sum[i] << endl;
    }
    
    // stage 3: normalize each row
    for (int i = 0; i < resp_score.size(); ++i) {
        //resp_score_norm_by_family[i].resize(resp_score[0].size());

        for (int j = 0; j < resp_score[0].size(); ++j) {            
            resp_score_norm_by_family[i][j] = resp_score_norm_by_family[i][j] - row_sum[i]; 
            //cout << resp_score_norm_by_family[i][j] << ", ";
        }
        //cout << endl;
    }

    /* 
     * convert from log space to normal by taking exponent
     * also check if values are normalized or not 
     */
    for (int i = 0; i < resp_score_norm_by_family.size(); ++i) {
        double t = 0.0;
        //cout << "family " << i << endl;
        for (int j = 0; j < resp_score_norm_by_family[0].size(); ++j) {
            resp_score_norm_by_family[i][j] = exp(resp_score_norm_by_family[i][j]);
            t += resp_score_norm_by_family[i][j];
            //cout << resp_score_norm_by_family[i][j] << ", ";
        }
        //cout << endl;
    }

}

void ResponsibilityScore::normalizeRespScoreByGeneFamilyAlternate() {
    /* For each gene family i, divide i_max (the maximum ML over all the species 
     * tree) over all ML estimates j for that family.
     */
    resp_score_norm_by_family.resize(resp_score.size());
    double row_sum[resp_score_norm_by_family.size()];

    resp_score_norm_by_family.resize(resp_score.size());
    for (int i = 0; i < resp_score.size(); ++i) {
        resp_score_norm_by_family[i].resize(resp_score[0].size());
        double* arr = &resp_score[i][0];
        double *max = std::max_element(arr, arr + resp_score[i].size());
        double total = 0.0;

        for (int j = 0; j < resp_score[i].size(); j++) {
            resp_score_norm_by_family[i][j] = resp_score[i][j] - *max;
            if (j == 0)
                total = resp_score_norm_by_family[i][j];
            else
                total = getSumOfLogLikelihood(total, resp_score_norm_by_family[i][j]);
            //cout << "i = " << i << endl;
        }
        row_sum[i] = total;
    }

    /* now normalize them */
    /* compute by-row total for each species tree 
     * note: we have to change back from log 
     */
    for (int i = 0; i < resp_score_norm_by_family.size(); ++i) {
        double total = row_sum[i];
        for (int j = 0; j < resp_score_norm_by_family[0].size(); ++j) {
            resp_score_norm_by_family[i][j] = exp(resp_score_norm_by_family[i][j] - total);
            cout << resp_score_norm_by_family[i][j] << ",";
        }
    }
}

double ResponsibilityScore::getSumOfLogLikelihood(double first, double second) {

    double greater, smaller;
    double result = 0.0;
    double base = exp(1);

    /* For c > a, the formula is 
     * log_b(a+c) = log_b(a) + log_b(1 + b^(log_b(c) - log_b(a))) */
    result = max(first,second) + log(1 + pow(base, -abs(first - second)));
    /*
    if (first >= second) {
        result = second + log(1 + pow(base, first - second));
        //greater = first;
        //smaller = second;
    } else {
        result = first + log(1 + pow(base, second - first));
        cout << result << " and " << (first + log(1 + pow(base, second - first))) << endl;
        //greater = second;
        //smaller = first;
    }
     */ 

    //result = smaller + log(1 + pow(base, greater - smaller));
    return result;

    /*
     // old code where we have to specify greater and smaller
     // would cause "assertion failed" error sometimes.
    assert(greater > smaller);

    double result = 0.0;
    double base = exp(1);

    // log_b(a+c) = log_b(a) + log_b(1 + b^(log_b(c) - log_b(a))) 
    result = smaller + log(1 + pow(base, greater - smaller));
    return result;
     */
}

void ResponsibilityScore::normalizeLikelihoodRatio() {
    /* check if already done and then return */
    double total = std::accumulate(tree_lk_ratio.begin(), tree_lk_ratio.end(), 0.0);
    assert(total > 0 && "The sum of likelihoods should be greater than zero");

    if (abs(total - 1) < 0.0001)
        return;

    /* normalize otherwise */
    for (d_vector::iterator i = tree_lk_ratio.begin(); i != tree_lk_ratio.end(); ++i)
        *i = *i / total;
}

void ResponsibilityScore::normalizeRespScoreBySpeciesTree() {
    /* check that the weights are already normalized by gene family */
    /* we don't use this assumption anymore */
    //for (unsigned i = 0; i < resp_score_norm_by_family.size(); ++i)
    //    assert(abs(std::accumulate(resp_score_norm_by_family[i].begin(),
    //        resp_score_norm_by_family[i].end(), 0.0) - 1.0) < 0.00001);

    /* if resp_score_norm_by_family is MxN matrix,
     * resp_score_norm_by_stree is NxM
     */
    resp_score_norm_by_stree.resize(resp_score[0].size());
    for (unsigned i = 0; i < resp_score_norm_by_stree.size(); ++i)
        resp_score_norm_by_stree[i].resize(resp_score.size());

    /* first copy the values */
    for (unsigned i = 0; i < resp_score_norm_by_stree.size(); ++i)
        for (unsigned j = 0; j < resp_score_norm_by_stree[0].size(); ++j)
            /* note: [j][i] goes to [i][j] */
            resp_score_norm_by_stree[i][j] = resp_score[j][i];

    /* now normalize them */
    for (unsigned i = 0; i < resp_score_norm_by_stree.size(); ++i) {

        double total = resp_score_norm_by_stree[i][0];
        for (int j = 1; j < resp_score_norm_by_stree[i].size(); j++)
            //total = addlogprob.operator ()(total, resp_score_norm_by_stree[i][j]);
            total = getSumOfLogLikelihood(total, resp_score_norm_by_stree[i][j]);
        //double total = std::accumulate(resp_score_norm_by_stree[i].begin(),
        //resp_score_norm_by_stree[i].end(), 0.0, addlogprob);
        for (unsigned j = 0; j < resp_score_norm_by_stree[0].size(); ++j) {
            resp_score_norm_by_stree[i][j] = exp(resp_score_norm_by_stree[i][j] - total);
        }
    }
}
