/* 
 * File:   MapComparator.cpp
 * Author: ikramu
 * 
 * Created on July 18, 2013, 2:56 PM
 */

#include "MapComparator.h"
#include <algorithm>

MapComparator::MapComparator() {
}

MapComparator::MapComparator(const MapComparator& orig) {
}

MapComparator::~MapComparator() {
}

sd_pair MapComparator::getMax(sd_map the_map) {
    sd_pair max = *max_element(the_map.begin(), the_map.end(), 
            CompareSecond()); 
    return max;
}

sd_pair MapComparator::getMin(sd_map the_map) {
    sd_pair max = *min_element(the_map.begin(), the_map.end(), 
            CompareSecond()); 
    return max;
}

bool 
MapComparator::compare(sd_pair i, sd_pair j) 
{ 
  return i.second > j.second; 
}

