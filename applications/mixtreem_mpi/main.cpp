// standard includes
#include <iostream>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
//#include "mpi.h"
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include "boost/mpi/communicator.hpp"

#include "MixtreemMPI.h"

// stisem includes
#include "MapComparator.h"
#include "Reader.h"
#include "StisemLog.h"
#include "ResponsibilityScore.h"

// semphy includes
#include "semphy_cmdline.h"
#include "logFile.h"
#include "mainSemphy.h"


#define BILLION 1000000000L;

using namespace boost;
using namespace boost::mpi;
using namespace std;

void print_software_info();
void handle_command_line_arguments(int argc, char* argv[],
        semphy_args_info &args_info);
void writeArrayToStisemLog(StisemLog &stisem_logger, s_vector gfNames, string header);
void iterate(semphy_args_info args, gene_families families, sequenceContainer fullMSA,
        vector<GeneFamilyMapper> msa_mapper, t_vector species_trees, stochasticProcess sp,
        StisemLog &stisem_logger);

int main(int argc, char** argv) {

    if (argc < 3) {
        cerr << "\nUsage: " << argv[0] << " tree_file seq_file" << endl;
        cerr << "\ntree_file: file containing different topologies of the tree " << endl;
        cerr << "seq_file: file containing FASTA sequences for the "
                "leaves of the tree " << endl;
        exit(EXIT_FAILURE);
    }

    mpi::environment env(argc, argv);
    mpi::communicator world;
    int proc_num = world.rank();

    if (proc_num == 0) {
        print_software_info();
    }

    /* handle command line arguments */
    semphy_args_info args_info;
    handle_command_line_arguments(argc, argv, args_info);

    cmdline2EvolObjs<semphy_args_info> cmd2Objs(args_info);

    //if (world.rank() == 0) {
    if (!args_info.seed_given) {
        //cout << "seed before " << args_info.seed_arg << endl;
        srand(time(NULL));
        long seed = rand();
        args_info.seed_arg = seed;
        args_info.seed_given = 1;
        //cout << "seed after " << args_info.seed_arg << endl;
    }

    if (proc_num == 0) {
        cout << "\nAvailable number of processors are " << world.size() << endl;
        cout << "seed is " << args_info.seed_arg << endl;
    }
    stochasticProcess sp;
    sp = cmd2Objs.cmdline2StochasticProcess();

    Reader reader;
    gene_families_with_names gf_w_names;
    gene_families gfamilies;
    s_vector gfNames;
    reader.readGeneFamilies(args_info.genefamilies_arg, gfamilies, gf_w_names,
            gfNames, args_info.alphabet_arg);

    /* read initial species tree */
    s_vector stNames;
    t_vector species_trees = reader.readSpeciesTrees(args_info.speciestrees_arg, stNames);

    StisemLog stisem_logger(args_info.stisemlog_arg, gfamilies.size(),
            species_trees.size(), args_info.stisemiter_arg, args_info.seed_arg);

    if (proc_num == 0) {
        stisem_logger.printSimulationStartHeader(stisem_logger.num_families, reader.getWhole_genome_msa().seqLen(), 
                stisem_logger.num_strees, stisem_logger.num_iter, stisem_logger.seed);
        writeArrayToStisemLog(stisem_logger, gfNames, "Gene Family Files");
        writeArrayToStisemLog(stisem_logger, stNames, "Species Trees Files");
        stisem_logger.writeConcatenatedSequence(reader.getWhole_genome_msa());
        cout << "Total number of sites in concatenated sequence is " << reader.getWhole_genome_msa().seqLen() << endl;
    }
    struct timespec start, stop;
    double accum;

    if (clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    
    /* call the iterative routine */
    iterate(args_info, gfamilies, reader.getWhole_genome_msa(), reader.getMsa_mappers(),
            species_trees, sp, stisem_logger);

    if (clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }

    accum = (stop.tv_sec - start.tv_sec)
            + (double) (stop.tv_nsec - start.tv_nsec)
            / (double) BILLION;
    
    cout << "Total time elapsed in processor " << proc_num << " is " << accum << " seconds" << endl;

    return 0;
}

void print_software_info() {
    cout << "\n\n";
    cout << "******************************************************************" << endl;
    cout << "*                   MIXTREEM, version 1.0                     	 *" << endl;
    cout << "* Author: Ikram Ullah (ikramu@kth.se)      Created: January 2014 *" << endl;
    cout << "******************************************************************" << endl;
    cout << "\n\n";
}

void handle_command_line_arguments(int argc, char* argv[],
        semphy_args_info &args_info) {
    if (semphy_cmdline_parser(argc, argv, &args_info) != 0) {
        errorMsg::reportError("error reading command line", 1);
    }

    /**/
    if (args_info.consurf_flag) {
        tree t_tmp;
        { // for ms1;
            args_info.optimizeAlpha_flag = 0; // true
            args_info.alpha_given = 0;
            mainSemphy ms_tmp(args_info);
            ms_tmp.computeNJtree();
            t_tmp = ms_tmp.getTree();
        }
        args_info.optimizeAlpha_flag = 1; // true
        args_info.alpha_given = 1;
        args_info.laguerre_flag = 3;
        strcat(args_info.Logfile_arg, "log.txt");
        mainSemphy ms2(args_info);
        myLog::printArgv(1, argc, argv);
        ms2.setTree(t_tmp);
        ms2.optimizeAlphaOnly();
        args_info.alpha_arg = static_cast<gammaDistribution*> (ms2.getStochasticProcess().distr())->getAlpha();
    }

    mpi::communicator w;
    if (w.rank() == 0) {
        myLog::printArgv(1, argc, argv);
        cout << endl;
    }
}

/**
 * Writes vector array contents to log file. This function is mainly meant to 
 * write gene family names and species tree names to log file.
 * 
 * @param stisem_logger log file object
 * @param data          vector data
 * @param header        header explaining what the data is about
 */
void writeArrayToStisemLog(StisemLog &stisem_logger, s_vector data, string header) {
    stisem_logger.openFileConnection();
    header += string("\n-------------------");
    stisem_logger.writeLine(header);
    for (unsigned i = 0; i < data.size(); ++i)
        stisem_logger.writeLine(data[i]);
    stisem_logger.writeLine("##############################");
    stisem_logger.closeFileConnection();
}

/**
 * This is the iterative process where each iteration consists of 
 * following operations.
 * 1. compute responsibility score for each (family, species_tree) pair
 * 2. construct super sequences from gene families for each species tree 
 *    based on their responsibility score for that species tree.
 * 3. for each species tree, compute new topology based on the super sequences
 * 4. compare new topologies with old one, and stop computing that topology again
 *    if they are the same
 * 5. Exit when done for all species tree topologies
 * 
 * @param args command line arguments for SEMPHY
 * @param families the MSA for gene families
 * @param fullMSA the collective MSA we get by combining all MSAs
 * @param msa_mapper information for each MSA position in fullMSA
 * @param species_trees set of species trees
 * @param sp stochasticProcess object used in likelihood computation 
 *        in SEMPHY library
 * @param num_iter The number of iterations after which the program will return. If convergence
 *         is reached before that, the program will return forthwith. (default = 10)
 * 
 * @author Ikram Ullah (ikramu) 20130913
 */
void iterate(semphy_args_info args, gene_families families, sequenceContainer fullMSA,
        vector<GeneFamilyMapper> msa_mapper, t_vector species_trees, stochasticProcess sp,
        StisemLog &stisem_logger) {

    mpi::communicator world;

    dd_vector resp_score_gfamily;
    unsigned cur_iter = 1;
    double prev_likelihood;
    int proc_num = world.rank();
    int total_proc = world.size();

    if (proc_num == 0) {
        for (unsigned k = 0; k < species_trees.size(); ++k)
            stisem_logger.writeSpeciesTree(*species_trees[k], k, 0.0);
        cout << "number of families in gene_families is " << families.size() << endl;
        cout << "number of species trees is " << species_trees.size() << endl;

        /* set up structures to hold previous iteration values */
        prev_likelihood = -std::numeric_limits<double>::max();
    }

    /* we will repeat the process until convergence */
    while (true) {
        if (proc_num == 0) {
            stisem_logger.printIterationStartHeader(cur_iter, args.stisemiter_arg);
            ResponsibilityScore rscore(families, species_trees, sp);
            rscore.computeLikelihoodsAllTreesAllFamilies();

            resp_score_gfamily = rscore.getResponsibilityScoreByGeneFamily();

            for (int i = 0; i < resp_score_gfamily.size(); ++i) {
                double total = 0.0;
                for (int j = 0; j < resp_score_gfamily[0].size(); ++j) {
                    total += resp_score_gfamily[i][j];
                }

                if (total <= 0.9) {
                    cerr << "Error in normalizing the responsibilities. Exiting..." << endl;
                    exit(EXIT_FAILURE);
                }                
            }

            /* write responsibility scores to log file */
            stisem_logger.writeResponsibilityScores(rscore.getOriginalLikelihoodValues());
            stisem_logger.writeResponsibilityScores(resp_score_gfamily);
        }

        double cur_likelihood = 0.0;

        d_vector all_likelihoods;        
        cout << "I am processor " << proc_num << " WITHIN IF statement" << endl;
        // call the parallel routine
        // if the current processor number is greater than required, skip the call.
        if (proc_num <= species_trees.size()) {
            
            MixtreemMPI mmpi(args, species_trees, fullMSA, resp_score_gfamily,
                    msa_mapper, species_trees.size());
            mmpi.runParallelAlgo();


            if (proc_num == 0) {
                species_trees = mmpi.getSpeciesTrees();
                all_likelihoods = mmpi.getLikelihoods();
                cout << "\n\nReceived species tree are : " << endl;
                for (unsigned i = 0; i < all_likelihoods.size(); ++i) {
                    /* write species tree to log file */
                    species_trees[i]->output(cout);
                    stisem_logger.writeSpeciesTree(*species_trees[i], i, all_likelihoods[i]);

                    cur_likelihood += all_likelihoods[i];
                }

                prev_likelihood = cur_likelihood;

                cout << "\n######### After " << cur_iter << ", overall species likelihood is " << cur_likelihood << "###########\n" << endl;
            }
        }

        /* increment the iteration counter */
        cur_iter++;

        /* if we have reached max iterations, then return */
        if (cur_iter > args.stisemiter_arg) {
            if (proc_num == 0) {
                /* write the summary to the log file */
                stisem_logger.writeSummaryStatistics(cur_iter - 1, prev_likelihood);
            }
            //cout << "_____ MPI::Finalize() called by processor " << (proc_num+1) << endl;
            MPI::Finalize();
            break;
        }
    }
}