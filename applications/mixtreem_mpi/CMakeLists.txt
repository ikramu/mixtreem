# Make sure the compiler can find include files from SEMPHY library. 
include_directories (${STISEM_SOURCE_DIR}/libraries/core) 
include_directories (${STISEM_SOURCE_DIR}/libraries/semphy) 
  
# Make sure the linker can find the SEMPHY library once it is built. 
link_directories (${STISEM_BINARY_DIR}/libraries/core) 
link_directories (${STISEM_BINARY_DIR}/libraries/semphy) 

add_executable(mixtreem_mpi MixtreemMPI.cpp 
main.cpp MapComparator.cpp Reader.cpp StisemLog.cpp
ResponsibilityScore.cpp )

target_link_libraries(mixtreem_mpi rt ${MPI_CXX_LIBRARIES} ${Boost_LIBRARIES} semphy core)

