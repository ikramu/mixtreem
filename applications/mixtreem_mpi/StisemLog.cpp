/* 
 * File:   StisemLog.cpp
 * Author: ikramu
 * 
 * Created on September 13, 2013, 7:38 PM
 */

#include <stdlib.h>
#include <sstream>
#include <time.h>

#include "StisemLog.h"
#define VERBOSE false

StisemLog::StisemLog() {
}

StisemLog::StisemLog(string ofn, unsigned numGF, unsigned numST,
        unsigned numIter, long cur_seed, int log_level) : num_families(numGF), 
        num_strees(numST), num_iter(numIter), seed(cur_seed) {
    abs_stat_time = time(0);
    /* append to the end of the file */
    out_file_name = ofn;
    _out_file_writer.open(out_file_name.c_str(), ios::app);
    if (!_out_file_writer.is_open()) {
        cerr << "Error: output stream can't be initialized. Exiting now..." << endl;
        exit(EXIT_FAILURE);
    }
    _log_level = log_level;        

    //printSimulationStartHeader(numGF, numST, numIter, seed);
    start_time = time(0);    
    closeFileConnection();
}

StisemLog::StisemLog(const StisemLog& orig) {
}

StisemLog::~StisemLog() {
    _out_file_writer.flush();
    _out_file_writer.close();
}

void StisemLog::printEntity(ostream &out, string name, string data, string endId) {
    out << name << endl;
    out << data << endl;
    out << endId << endl;
    out.flush();
}

void StisemLog::printIterationStartHeader(int iterNo, int total) {
    time_t now = time(0);
    struct tm tms;
    tms = *localtime(&now);    
    double seconds = difftime(now, start_time);
    time_t elapsed = static_cast<time_t> (difftime(now, start_time));
    struct tm * ptm = gmtime(&elapsed);    
    char line[1000];
    sprintf(line, "%id %ih %im %is", ptm->tm_mday-1, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
    cout << "Time since last iteration : " << line << endl << endl;        
    
    openFileConnection();
    _out_file_writer << "********************************************************************" << endl;
    _out_file_writer << "*****   start of iteration " << iterNo << " of " <<
            total << " at " << getCurrentTimeStamp() << "   *****" << endl;
    _out_file_writer << "*****   total seconds since last iteration are " << seconds << "   *****" << endl;
    _out_file_writer << "*****   which equals " << line << "                  *****" << endl;
    _out_file_writer << "********************************************************************" << endl << endl;        
    closeFileConnection();
    
    start_time = now;
}

void StisemLog::printSimulationStartHeader(int numGF, int seqLen_sseq, int numST, int numIt, long seed) {
    openFileConnection();
    iter_start_time = getCurrentTimeStamp();
    _out_file_writer << endl << endl;
    _out_file_writer << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    _out_file_writer << "%%%%%%%%%%%%  " << "Stisem simulation STARTED on " << iter_start_time
            << "   %%%%%%%%%%%%" << endl;
    _out_file_writer << "Seed = " << seed << endl;
    _out_file_writer << "Total number of gene families = " << numGF << endl;
    _out_file_writer << "Sequence length of super_sequence = " << seqLen_sseq << endl;
    _out_file_writer << "Total number of species tree topologies = " << numST << endl;
    _out_file_writer << "Total number of iterations = " << numIt << endl;
    _out_file_writer << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    _out_file_writer << endl << endl;
    closeFileConnection();
}

void StisemLog::printSimulationEndHeader(ofstream &out) {
    string iter_end_time = getCurrentTimeStamp();
    time_t now = time(0);
    struct tm tms;
    tms = *localtime(&now);
    double seconds = difftime(now, start_time);
    time_t elapsed = static_cast<time_t> (difftime(now, start_time));
    struct tm * ptm = gmtime(&elapsed);    
    char line[1000];
    sprintf(line, "%id %ih %im %is", ptm->tm_mday-1, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);

    out << endl << endl;
    out << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    out << "%%%%%%%%%%%%  " << "Stisem simulation ENDED on " << iter_end_time
            << "   %%%%%%%%%%%%" << endl;
    out << "*****   total seconds since last iteration are " << seconds << "   *****" << endl;
    out << "*****   which equals " << line << "                  *****" << endl;
    out << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    out << endl << endl;
    out.flush();
}

string StisemLog::getCurrentTimeStamp() {
    time_t now = time(0);
    struct tm tstruct;
    tstruct = *localtime(&now);

    char curtime[100];

    strftime(curtime, sizeof (curtime), "%Y-%m-%d.%X", &tstruct);    
    return string(curtime);
}

void StisemLog::writeResponsibilityScores(dd_vector resp_score) {
    openFileConnection();
    string name = "===Responsibility-Scores===";
    stringstream data;
    string endId("##############################");

    for (unsigned i = 0; i < resp_score.size(); ++i) {
        for (unsigned j = 0; j < resp_score[i].size(); ++j) {
            data << resp_score[i][j] << ", ";
        }
        data << endl;
    }

    printEntity(_out_file_writer, name, data.str(), endId);

    if (VERBOSE)
        printEntity(cout, name, data.str(), endId);
    closeFileConnection();
}

void StisemLog::writeSuperSequences(ss_map super_seq, unsigned stree_counter) {
    openFileConnection();
    char stinking_c_doesnot_have_int2str[100];
    sprintf(stinking_c_doesnot_have_int2str, "%d", (stree_counter + 1));
    string name = string("super-sequence # ") + string(stinking_c_doesnot_have_int2str);
    stringstream data;
    string endId("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

    /* if last super sequence, then write the end of entity identifier as well */
    if (stree_counter == (num_strees - 1))
        endId += "\n##############################";

    if (stree_counter == 0) {
        _out_file_writer << "===Super-Sequences===" << endl;

        if (VERBOSE) {
            cout << "Super Sequences" << endl;
        }
    }

    for (ss_map::iterator it = super_seq.begin(); it != super_seq.end(); ++it) {
        data << ">" << it->first << "\n" << it->second << endl;
    }

    printEntity(_out_file_writer, name, data.str(), endId);

    if (VERBOSE)
        printEntity(cout, name, data.str(), endId);
    closeFileConnection();
}

void StisemLog::writeConcatenatedSequence(sequenceContainer fullMSA) {
    openFileConnection();
    
    string name = "===Super-Sequence===";
    _out_file_writer << name << endl;
    for(int i = 0; i < fullMSA.numberOfSeqs(); ++i) {
        sequence s = fullMSA[i];
        _out_file_writer << ">" << s.name() << endl;
        _out_file_writer << s.toString() << endl;
    }
    _out_file_writer << "##############################" << endl;
    closeFileConnection();
}

void StisemLog::writeSpeciesTree(tree sp_tree, unsigned stree_counter, double likelihood) {
    openFileConnection();

    string name = "===Species-Trees===";
    string endId("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    /* if last super sequence, then write the end of entity identifier as well */
    if (stree_counter == (num_strees - 1))
        endId += "\n##############################";

    if (stree_counter == 0) {
        _out_file_writer << name << endl;

        if (VERBOSE) {
            _out_file_writer << name << endl;
        }
    }

    sp_tree.output(_out_file_writer);
    _out_file_writer << likelihood << endl;
    _out_file_writer << endId << endl;
    //_out_file_writer.flush();

    if (VERBOSE) {
        sp_tree.output(cout);
        cout << endl << likelihood << endl;
        cout << endId << endl;
        cout.flush();
    }

    closeFileConnection();
}

void StisemLog::writeLine(string line) {
    _out_file_writer << line << endl;
    _out_file_writer.flush();

    if (VERBOSE)
        cout << line << endl;
}

void StisemLog::writeEndOfIterationTags(int iter) {
    char buf[100];    
    sprintf(buf, "##########   END of iteration %d   ##########", iter);

    _out_file_writer << "##################################################" << endl;
    _out_file_writer << buf << endl;
    _out_file_writer << "##################################################\n\n" << endl;

    if (VERBOSE) {
        cout << "##################################################" << endl;
        cout << buf << endl;
        cout << "##################################################\n\n" << endl;
    }
}

void StisemLog::writeSummaryStatistics(int total_iter, double stree_likelihood) {    
    time_t now = time(0);
    struct tm tms;
    tms = *localtime(&now);
    double seconds = difftime(now, abs_stat_time);
    time_t elapsed = static_cast<time_t> (difftime(now, abs_stat_time));
    struct tm * ptm = gmtime(&elapsed);    
    char line[1000];
    sprintf(line, "%id %ih %im %is", ptm->tm_mday-1, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
    
    openFileConnection();   

    string message("");
    string header("\n***************************************************************************");
    char buf[1000], curtime[100];
    strftime(curtime, sizeof (curtime), "%Y-%m-%d.%X", &tms);

    message = header;
    message += string("\n************  Stisem simulation ended on ");
    message += string(curtime) + string("   ************");
    message += header;
    
    buf[0] = 0;
    sprintf(buf, "\nTotal running time of program in seconds is %d", static_cast<int>(seconds));
    sprintf(buf, "%s. \nwhich equals %id %ih %im %is", buf, ptm->tm_mday-1, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
    message += string(buf) + header;

    /* reset the buffer */
    buf[0] = 0;
    sprintf(buf, "\nTotal iterations are %d", total_iter);
    sprintf(buf, "%s.\nOverall species tree likelihood is %f", buf, stree_likelihood);
    sprintf(buf, "%s.\nPlease see the last iteration for individual species trees "
            "and their likelihoods.", buf);
    message += string(buf) + header + header;    

    _out_file_writer << message << endl << endl;

    if (VERBOSE)
        cout << message << endl << endl;
    closeFileConnection();
}

void StisemLog::openFileConnection() {
    _out_file_writer.open(out_file_name.c_str(), ios::app);
    if (!_out_file_writer.is_open()) {
        cerr << "Error: output stream can't be initialized. Exiting now..." << endl;
        exit(EXIT_FAILURE);
    }
    if(!_out_file_writer.good()){
        cerr << "Error: output stream doesn't seems to be in consistent state." << endl;
        cerr << "In other words, the stream seems to have some problem (good() returns false)" << endl;
        cerr << "Exiting now..." << endl;
        exit(EXIT_FAILURE);
    }
}

void StisemLog::closeFileConnection() {
    _out_file_writer.flush();
    _out_file_writer.close();
}

