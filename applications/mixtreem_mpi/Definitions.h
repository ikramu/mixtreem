/* 
 * File:   Definitions.h
 * Author: ikramu
 *
 * Created on July 5, 2013, 6:12 PM
 */

#ifndef DEFINITIONS_H
#define	DEFINITIONS_H

#include <string>
#include <limits>
#include <vector>
#include <map>

#include "sequenceContainer.h"
#include "tree.h"
using namespace std;

typedef std::map<string, double>        sd_map;
typedef std::pair<string, double>        sd_pair;

/* 
 * This is amount of "noise" added/removed while selecting the chunk of 
 * MSA from each family. formula = 1/k^2 where k is num of species trees
 */
const double epsilon = 1/((double)16); // for 4 species trees

/* vector of MSA */
typedef vector<sequenceContainer> gene_families;

/* vector of (string, sequence container) hashmap */
typedef map<string, sequenceContainer> s_gf_map;
typedef vector<s_gf_map> gene_families_with_names;

/*  vector of super sequences, same as gene_families but 
 *  defining it separate for sake of understandability
 */
typedef vector<sequenceContainer> sseq_vector;

/* vector of integer */
typedef vector<int> i_vector;

/* vector of string */
typedef vector<string> s_vector;

/* vector of tree */
typedef vector<tree *> t_vector;

/* vector of double */
typedef vector<double> d_vector;

/* 2D double vector for responsibility score */
typedef vector<d_vector> dd_vector;

/* string-string hashmap 
 * may be used to store info about single sequence
 */
typedef map<string, string> ss_map;

/* vector of string-string hashmap 
 * may be used to store multiple sequence alignment
 */
typedef vector<ss_map> ss_map_vector;

#define LOWEST_DOUBLE_VALUE           std::numeric_limits<double>::min() 
#define HIGHEST_DOUBLE_VALUE          std::numeric_limits<double>::max() 

/* enumerations */
enum Nucleotide { Adenine, Cytosine, Guanine, Thymine};


#endif	/* DEFINITIONS_H */

