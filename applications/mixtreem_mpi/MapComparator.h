/* 
 * File:   MapComparator.h
 * Author: ikramu
 *
 * Created on July 18, 2013, 2:56 PM
 */

#ifndef MAPCOMPARATOR_H
#define	MAPCOMPARATOR_H

#include "Definitions.h"


struct CompareSecond {
    bool operator()(const sd_pair &left, const sd_pair &right) const {
        return left.second < right.second;
    }
};

class MapComparator {
public:
    MapComparator();
    MapComparator(const MapComparator& orig);
    virtual ~MapComparator();        
    
    sd_pair getMax(sd_map mymap);
    sd_pair getMin(sd_map mymap);
    
private:
    bool compare(sd_pair i, sd_pair j);
};

#endif	/* MAPCOMPARATOR_H */

