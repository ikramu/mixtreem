/* 
 * File:   MixtreemMPI.cpp
 * Author: ikramu
 * 
 * Created on July 17, 2013, 5:20 PM
 */
#include <vector>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <stdio.h>
#include <stdlib.h>

#include "MixtreemMPI.h"
#include "MapComparator.h"
#include "StisemLog.h"
#include "Reader.h"

#define CHAR_SEP "="

MixtreemMPI::MixtreemMPI(char** argv) {
}

MixtreemMPI::MixtreemMPI(semphy_args_info args, t_vector strees,
        sequenceContainer fMSA, dd_vector rscore_gfam,
        vector<GeneFamilyMapper> msamapper, unsigned int stree_size) {
    num_families = rscore_gfam.size();
    semphy_args = args;
    fullMSA = fMSA;
    msa_mapper = msamapper;
    int my_node = MPI::COMM_WORLD.Get_rank();
    if (my_node == 0) {
        species_trees = strees;
        num_strees = stree_size;
        rscore_gene_family = rscore_gfam;
        likelihood_vector.reserve(stree_size);
        comp_likelihood.reserve(stree_size);
        comp_strees.reserve(stree_size);

        // initialize all vector
        for (unsigned i = 0; i < stree_size; ++i) {
            likelihood_vector.push_back(0.0);
            comp_likelihood.push_back(0.0);
            comp_strees.push_back(new tree());
        }
    }
}

MixtreemMPI::MixtreemMPI(const MixtreemMPI& orig) {
}

MixtreemMPI::~MixtreemMPI() {
    if (istr.is_open()) {
        istr.close();
    }
    sss.clear();
}

void MixtreemMPI::runParallelAlgo() {
    boost::mpi::communicator world;
    int my_node = world.rank();

    if ((my_node == 0) && (world.size() == 1)) {
        cerr << "This program should be run with at least 2 processor (e.g. mpirun -np 2 <program_name_and_args>)" << endl;
        cerr << "exiting now..." << endl;
        exit(EXIT_FAILURE);
    }

    if (my_node == 0) {
        master();
    } else {
        slave(my_node);
    }
}

/**
 * Perform the tree likelihood using structural EM 
 * @param args_info             SEMPHY command line arguments
 * @param sp_tree               initial species tree for SEMPHY
 * @param super_sequences       MSA for super sequences
 * @param sp_tree_llk           sp_tree likelihood 
 * @return                      SEMPHY's produced species tree
 */
tree MixtreemMPI::perform_semphy_analysis(semphy_args_info args, sequenceContainer fMSA,
        d_vector resp_score_gfamily, double &sp_tree_llk) {
    mainSemphy ms(args);
    if (!args.BPonUserTree_given) {

        ms.setSequenceContainer(fMSA);

        ms.setResponsibilityScores(resp_score_gfamily);
        ms.setMsaMapper(msa_mapper);
        ms.compute();
    }

    // computing the BP values
    if (args.BPrepeats_given) {
        cout << "ERRORR:::: In args.BPrepeats_given clause" << endl;
        bootstrapProvider bp(args);
        bp.computeBP(ms);
        bp.output(ms.out());
    }

    /* store the tree's likelihood */
    sp_tree_llk = ms.getLikelihood();

    return ms.getTree();
}

d_vector MixtreemMPI::get_responsibility_score(int sp_id) {
    d_vector cur_resp(rscore_gene_family.size(), 0.0);
    for (unsigned y = 0; y < rscore_gene_family.size(); ++y) {
        cur_resp[y] = rscore_gene_family[y][sp_id];
    }
    return cur_resp;
}

void MixtreemMPI::get_objects_from_string(std::string str, string &sp_tree,
        double &likelihood, int &cur_tree) {
    using namespace boost;
    char * pointer;
    char_separator<char> sep(CHAR_SEP);

    boost::tokenizer< char_separator<char> > tokens(str, sep);
    vector<string> parts;

    BOOST_FOREACH(const string& t, tokens) {
        parts.push_back(t);
    }
    sp_tree = parts[0];
    likelihood = atof(parts[1].c_str());
    cur_tree = atoi(parts[2].c_str());

    cout << "\nResulting species tree " << cur_tree << " is: " << endl;
    cout << sp_tree << endl << endl;

    // save the tree to temp file
    pointer = tmpnam(NULL); 
    ofstream of;
    of.open(pointer);
    if (!of.good()) {
        cout << "************************************************" << endl;
        cout << "ERRIR.... ERROOOORRRR...." << endl;
        cout << "could not write tree to file " << pointer << endl;
        cout << "exiting now" << endl;
        cout << "************************************************" << endl;
        exit(EXIT_FAILURE);
    }
    of << sp_tree;
    of.close();

    sp_tree = string(pointer);
}

void MixtreemMPI::master() {
    int n_procs;
    string result;
    int rank;
    int cur_tree = 0;
    int ret_spid;
    double ret_likelihood;
    string ret_tree;
    boost::mpi::communicator world;
    n_procs = world.size();

    if (n_procs > (num_strees + 1)) {
        n_procs = num_strees + 1;
        /*
        cout << "Information:\n***********\n";
        cout << "The number of species trees are " << num_strees
                << " while the number of available processors are " << (world.size() + 1) << endl;
        cout << "In master-slave MPI paradigm, we will need 1 master and " << num_strees
                << " slaves = " << n_procs << " processors." << endl;

        cout << "So reducing n_processors from " << (world.size() + 1) << " to " << n_procs << endl;

        for (int p = n_procs; p < world.size(); ++p) {
            d_vector rscore;

            cout << "\nSending DIE signal to (UNUSED) processor " << (p + 1) << endl;
            rscore.push_back(DIE_SIGNAL);
            world.send(p, 1, rscore);
        }
        */
    }

    for (rank = 1; rank < n_procs; rank++) {
        d_vector rscore = get_responsibility_score(cur_tree);

        rscore.push_back(cur_tree);

        //        cout << "\nS-Tree " << cur_tree << endl;
        //        for (int y = 0; y < rscore.size(); ++y) {
        //            cout << rscore[y] << ", ";
        //        }
        //        cout << endl;

        cout << "sending to processor number " << (rank + 1) << endl;

        world.send(rank, 1, rscore);
        sleep(1);
        cur_tree++;
        if (cur_tree == num_strees)
            break;
    }

    while (cur_tree < num_strees) {
        boost::mpi::status s = world.recv(boost::mpi::any_source, 1, result);

        get_objects_from_string(result, ret_tree, ret_likelihood, ret_spid);

        if (istr.is_open())
            istr.close();
        istr.open(ret_tree.c_str());

        comp_likelihood[ret_spid] = ret_likelihood;
        comp_strees[ret_spid] = new tree(istr);

        // close the stream and delete the file
        istr.close();
        remove(ret_tree.c_str());


        d_vector rscore = get_responsibility_score(cur_tree);
        rscore.push_back(cur_tree);
        cout << "\nS-Tree " << cur_tree << endl;
        for (int y = 0; y < rscore.size(); ++y) {
            cout << rscore[y] << ", ";
        }
        cout << endl;

        world.send(s.source(), 1, rscore);
        cur_tree++;
    }

    for (int i = 1; i < n_procs; i++) {
        int ret_spid;
        boost::mpi::status s = world.recv(i, 1, result);

        get_objects_from_string(result, ret_tree, ret_likelihood, ret_spid);

        if (istr.is_open())
            istr.close();
        istr.open(ret_tree.c_str());

        comp_likelihood[ret_spid] = ret_likelihood;
        comp_strees[ret_spid] = new tree(istr);
    }

    for (int i = 1; i < n_procs; i++) {
        d_vector rscore;

        cout << "\nSending DIE signal to processor " << (i + 1) << endl;
        for (int y = 0; y < rscore.size(); ++y) {
            cout << rscore[y] << ", ";
        }
        cout << endl;

        rscore.push_back(DIE_SIGNAL);
        world.send(i, 1, rscore);
    }
}

void MixtreemMPI::slave(int rank) {
    d_vector val;
    val.resize(num_families);

    boost::mpi::communicator world;
    //cout << "In processor number " << rank << endl;
    while (true) {
        int cur_tree;
        boost::mpi::status s = world.recv(0, 1, val);

        if (val[0] == DIE_SIGNAL) {
            cout << "DIE signal received for processor number " << (rank + 1) << endl;
            return;
        }

        cur_tree = int(val[val.size() - 1]);

        cout << "\nComputing likelihood for species tree " << cur_tree << " in processor " << (rank + 1) << endl;

        double likelihood;

        // Here was one of the error. I was previously copying from 0 to (val.size() - 2)
        vector<double> sub(&val[0], &val[val.size() - 1]);
        tree ctree = perform_semphy_analysis(semphy_args, fullMSA, sub, likelihood);
        cout << "\nAfter perform_semphy_analysis() call in processor " << (rank + 1) << endl;
        sss.str(std::string());
        ctree.output(sss);
        string tree_str(sss.str());
        sss.str(std::string());
        tree_str.erase(std::remove(tree_str.begin(), tree_str.end(), '\n'), tree_str.end());

        sss << tree_str << CHAR_SEP;
        sss << likelihood << CHAR_SEP;
        sss << cur_tree;
        string packet = sss.str();

        world.send(0, 1, packet);
    }
}

t_vector MixtreemMPI::getSpeciesTrees() {
    return comp_strees;
}

d_vector MixtreemMPI::getLikelihoods() {
    return comp_likelihood;
}

