/* 
 * File:   StisemLog.h
 * Author: ikramu
 *
 * Created on September 13, 2013, 7:38 PM
 */

#ifndef STISEMLOG_H
#define	STISEMLOG_H

#include <fstream>
#include <ctime>
#include <string>

#include "Definitions.h"

using namespace std;

/**
 * This class is responsible for writing the 'interesting' statistics to the
 * log file. The file will be parsed to get 'customized' information like 
 * species tree topologies, super sequences, responsibility score etc. We 
 * therefore use some conventions as described below.
 * 1. Each entity (tree, sequence etc) start with (i) a descriptive name, 
 * followed by (ii) the data, and (iii) an ending identifier. 
 * 2. The descriptive names are
 *      a. Likelihoods
 *      b. Responsibility Scores
 *      c. Super Sequences
 *      d. Species Trees
 * 3. The ending identifier is "##############################" i.e. 30 hashes.
 * 4. For super sequences and species trees, the separator between consecutive 
 * entries is "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" i.e. 30 at-rate signs.
 * 5. Each iteration is started with following.
 * **************************************************
 * ***********   start of iteration 'i'   ***********
 * **************************************************
 * 
 * 6. Each simulation is started with following.
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * %%%%%%%%%%%%  Stisem simulation started on 'time_stamp'   %%%%%%%%%%%%
 * total number of gene families = i
 * total number of species topologies = j
 * total number of iterations = k
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * 
 * 7. Each simulation is ended with following.
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * %%%%%%%%%%%%  Stisem simulation ended on 'time_stamp'   %%%%%%%%%%%%
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * 
 */

class StisemLog {    
public:
    StisemLog();
    StisemLog(string out_file_name, unsigned ngf, unsigned nst, 
    unsigned iter, long seed, int log_level = 5);
    StisemLog(const StisemLog& orig);
    virtual ~StisemLog();
    
    /**
     * write responsibility scores to log file
     * 
     * @param resp_score_gfamily responsibility scores per gene family such 
     * that for each gene family, its sum over all species trees equals one.
     * @author Ikram Ullah (ikramu) 20130913
     */
    void writeResponsibilityScores(dd_vector resp_score_gfamily);
    
    void printSimulationEndHeader(ofstream &out);
    void printIterationStartHeader(int iterNo, int total);
    void printEntity(ostream &out, string name, string data, string endId);
    void printSimulationStartHeader(int numGF, int seqLen_sseq, int numST, int numIt, long seed);    
    string getCurrentTimeStamp();
    
    /**
     * write super sequences to log file
     * 
     * @param super_seq the super sequence
     * @param stree_counter tree counter used mainly for formatting output
     * @author Ikram Ullah (ikramu) 20130913
     */
    void writeSuperSequences(ss_map super_seq, unsigned stree_counter);
    
    /**
     * write the concatenated sequence (consisting of all gene families) to the log file
     * 
     * @param fullMSA sequenceContainer containing the concatenated sequence
     */
    void writeConcatenatedSequence(sequenceContainer fullMSA);
    
    /**
     * write species tree to log file (after it is computed in SEMPHY step)
     * 
     * @param sp_tree the species tree
     * @param stree_counter tree counter used mainly for formatting output
     * @param likelihood likelihood for the species tree
     * @author Ikram Ullah (ikramu) 21030917
     */
    void writeSpeciesTree(tree sp_tree, unsigned stree_counter, double likelihood);
    
    /**
     * write single line to log file
     * @param line the line to be written
     */
    void writeLine(string line);
    
    /**
     * indicates the end of one iteration
     * @param iter iteration number
     */
    void writeEndOfIterationTags(int iter);
    
    /**
     * write the summary statistics at the end of the simulation
     * @param total_iter total iterations
     * @param stree_likelihood overall species tree likelihood
     */
    void writeSummaryStatistics(int total_iter, double stree_likelihood);
    
    void openFileConnection();
    
    void closeFileConnection();
    
private:
    int _log_level;
    string iter_start_time;
    time_t start_time;
    time_t abs_stat_time;
    string out_file_name;
    std::ofstream _out_file_writer;
    
public:
    int num_families;
    int num_strees;  
    int num_iter;
    long seed;
};

#endif	/* STISEMLOG_H */

