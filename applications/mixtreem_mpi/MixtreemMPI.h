/* 
 * File:   MixtreemMPI.h
 * Author: ikramu
 *
 * Created on July 17, 2013, 5:20 PM
 */

#ifndef PARALLELML_H
#define	PARALLELML_H

#include "Definitions.h"

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <string>
//#include "mpi.h"
#include <boost/mpi.hpp>
//#include <boost/mpi/en>

// for semphy
#include "tree.h"
#include "semphy_cmdline.h"
#include "sequenceContainer.h"
#include "mainSemphy.h"
#include "bootstrapProvider.h"
#include "StisemLog.h"
#include "Reader.h"

#define DIE_SIGNAL 10000000

using namespace std;

class MixtreemMPI {
public:
    MixtreemMPI(char** argv);
    //MixtreemMPI pml(args, *species_trees, fullMSA, resp_score_gfamily, 
    //              msa_mapper, species_trees.size()); 
    MixtreemMPI(semphy_args_info args, t_vector strees, sequenceContainer fMSA,
            dd_vector rscore_gfam, vector<GeneFamilyMapper> msamapper, unsigned int stree_size);
    //MixtreemMPI(semphy_args_info args);
    MixtreemMPI(const MixtreemMPI& orig);
    virtual ~MixtreemMPI();

    void runParallelAlgo();    
    sd_pair getBestLikelihoodPair();
    
    t_vector getSpeciesTrees(); //t_vector &stree);
    d_vector getLikelihoods();

private:
    void master();
    void slave(int);   
    tree perform_semphy_analysis(semphy_args_info args, sequenceContainer fMSA, 
        d_vector resp_score_gfamily, double &sp_tree_llk);     
    d_vector get_responsibility_score(int spid);
    void get_objects_from_string(std::string str, string &sp_tree, double &likelihood, int &cur_tree);

    stringstream sss;
    ifstream istr;
    tree *comp_tree;
    
    t_vector species_trees;
    sequenceContainer fullMSA;
    dd_vector rscore_gene_family;
    vector<GeneFamilyMapper> msa_mapper;
    unsigned int num_strees;
    unsigned int num_families;
    semphy_args_info semphy_args;
    d_vector likelihood_vector;
    d_vector comp_likelihood;
    t_vector comp_strees;
};

#endif	/* PARALLELML_H */

