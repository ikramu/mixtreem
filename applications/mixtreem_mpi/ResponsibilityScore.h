/* 
 * File:   ResponsibilityScore.h
 * Author: ikramu
 *
 * Created on August 2, 2013, 3:29 PM
 */

#ifndef RESPONSIBILITYSCORE_H
#define	RESPONSIBILITYSCORE_H

#include "Definitions.h"
#include "stochasticProcess.h"
#include "likelihoodComputation.h"

class ResponsibilityScore {
public:
    ResponsibilityScore();
    ResponsibilityScore(gene_families gf, t_vector sp_top, stochasticProcess spr);
    ResponsibilityScore(const ResponsibilityScore& orig);
    virtual ~ResponsibilityScore();
    
    /* compute normalized likelihood for species trees */
    void computeLikelihoodsAllTreesAllFamilies();
    
    /* we use the formula to add likelihoods in log space 
     * log_b(a+c) = log_b(a) + log_b(1 + b^(log_b(c) - log_b(a)))
     */
    double getSumOfLogLikelihood(double greater, double smaller);
    //double ResponsibilityScore::getSumOfLogLikelihoodNoOrder(double first, double second);
    
    /* get the responsibility vector */
    dd_vector getResponsibilityScoreBySpeciesTree(){
        return resp_score_norm_by_stree;
    }
    
    /* get the responsibility vector by gene family, so that the sum of 
     * responsibility of all sequences for a single species tree equals one.
     * 
     * for a species tree instance st_k, sum_{i=1}^{n}(resp_score_{ik}) = 1.0
     */
    dd_vector getResponsibilityScoreByGeneFamily(){
        return resp_score_norm_by_family;
    }
    
    dd_vector getOriginalLikelihoodValues(){
        return resp_score;
    }
    
private:
    /* initialize variables to given values */
    void initVariables(gene_families gf, t_vector sp_top);
    
    /* normalize the likelihood vector */
    void normalizeLikelihoodRatio();
        
    void computeLikelihoodsOneFamilyAllTrees(int ti);    
    
    /* 
     * normalize the responsibility score by each family so that for each 
     * family 'i', the sum of all likelihood scores (one per each species
     * tree 'j') sums to 1
     */
    void normalizeRespScoreByGeneFamily(); 
    
    /**
     * alternative to normalizeRespScoreByGeneFamily() function (to 
     * avoid all zeros)
     */
    void normalizeRespScoreByGeneFamilyAlternate(); 
    
    /* 
     * normalize the responsibility score by each tree so that for each 
     * tree 'i', the sum of all likelihood scores (one per each family 'j') 
     * sums to 1
     * Note: this should be done after the weights have been normalized by 
     * gene family (by calling normalizeRespScoreByGeneFamily() )
     */
    void normalizeRespScoreBySpeciesTree();   
    
    /* This is (family by tree) matrix. For 'm' families and 'n' species tree 
     * we have (m x n) matrix i.e. with m rows and n columns.
     */
    dd_vector resp_score;
    dd_vector resp_score_norm_by_stree;
    dd_vector resp_score_norm_by_family;
    t_vector tree_topologies;
    gene_families g_families;
    d_vector tree_lk_ratio;
    stochasticProcess sp;

};

#endif	/* RESPONSIBILITYSCORE_H */

