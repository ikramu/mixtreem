/* 
 * File:   main.cpp
 * Author: ikramu
 *
 * Created on February 26, 2014, 7:05 PM
 */

#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <errno.h>
#include <iosfwd>
#include "errorMsg.h"
#include "MixTreEMLogParser.h"
#include "Reader.h"
#include "mainSemphy.h"
#include "bblEM.h"
#include "semphy_cmdline.h"

#define THREHOLD_FOR_GFAM_INCLUSION 0.1

using namespace std;

void write_seq_to_file(sequenceContainer the_seq, string outfile);

void get_super_seq(sequenceContainer &super_seq, gene_families families);

void write_super_seq_for_all_sp_trees(t_vector strees, dd_vector resp_scores
        , gene_families families, string outdir);

std::string int_to_string(int i);

/*
 * 
 */
int main(int argc, char** argv) {

    if (argc < 3) {

        cerr << "Error: wrong number of parameters" << endl;
        cerr << "Usage: " << argv[0] << " mixtreem_log_file out_seq_dir" << endl;
        cerr << "mixtreem_seq_file = Log file containing, among others, the responsibility scores and gene family names" << endl;
        cerr << "out_sseq_dir = directory where the concatenated (super) sequences for each species tree will be written."
                "For each species tree, only those sequences will be added which have responsibility score greater than 0.1" << endl;
        exit(EXIT_FAILURE);
    }
    
    string pfile(argv[1]);
    string outdirectory(argv[2]);
    //semphy_args_info args_info;    

    //stochasticProcess sp = get_sp(argc, argv, args_info);


    //string rand_seq = get_random_seq(super_seq.seqLen()); 

    int sizeOfAlp = 20; // right now, we are doing it for protein only
    MixTreEMLogParser parser(pfile, sizeOfAlp);

    gene_families families = parser.getAllGeneFamilies();
    t_vector strees = parser.getAllSpeciesTrees();
    
    dd_vector resp_scores = parser.getAllResponsibilityScores();
    
    write_super_seq_for_all_sp_trees(strees, resp_scores, families, outdirectory);

    /*
    get_super_seq(super_seq, families);

    string rand_seq = get_random_seq(super_seq.seqLen());
    const sequence outgroup(rand_seq, string("out_group"), string(""), super_seq.numberOfSeqs(), super_seq.getAlphabet());
    sseq_with_outgroup = super_seq;
    sseq_with_outgroup.add(outgroup);

    // write to output for future error-detection-and-correction
    write_seq_to_file(super_seq, string(outdirectory).append("/original.fasta"));
    write_seq_to_file(sseq_with_outgroup, string(outdirectory).append("/with.outgroup.fasta"));

    // sanity check; number of gene families should be equal to columns in resp_scores
    if (families.size() != resp_scores.size()) {
        cerr << "Something fishy! Number of responsibility scores should be equal to number of gene families" << endl;
        cerr << "Apparently, its not the case here, so quitting..." << endl;
        cerr << "Before getting angry at me, have a look at the files :)" << endl;
        exit(EXIT_FAILURE);
    }
    
    for (int i = 0; i < strees.size(); ++i) {
        d_vector weights;
        char spno[50];
        sprintf(spno, "%d", (i + 1));
        string outfile = outdirectory;
        outfile.append("/sp_");
        outfile.append(spno);
        outfile.append(".tree");
        clock_t begin, end;
        double time_spent;

        begin = clock();

        get_weights_for_sptree(weights, families, resp_scores, i);

        // first compute the tree for original sequences
        cout << "computing tree for original sequences for tree # " << (i + 1) << endl;
        compute_weighted_likelihood(super_seq, outfile, weights, sp, args_info);
        end = clock();
        time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
        cout << "Total time spent in one SEMPHY call is " << time_spent << endl;

        // next compute the tree for original plus out-group sequences
        outfile.append(".with.outgroup");
        cout << "computing tree for outgroup-appended sequences for tree # " << (i + 1) << endl;
        compute_weighted_likelihood(sseq_with_outgroup, outfile, weights, sp, args_info);
    }
     */    
    return 0;
}

void get_super_seq(sequenceContainer &super_seq, gene_families families) {
    Reader reader;
    reader.initializeGeneFamilyMapper(families.size());
    for (int i = 0; i < families.size(); ++i) {
        reader.addMSAtoBigMSA(families[i], i);
    }

    super_seq = reader.getWhole_genome_msa();
}


void write_seq_to_file(sequenceContainer the_seq, string outfile) {
    std::ofstream of(outfile.c_str(), std::ios::out);
    for (int i = 0; i < the_seq.numberOfSeqs(); ++i) {
        of << ">" << the_seq[i].name() << endl;
        of << the_seq[i].toString() << endl;
    }
    of.flush();
    of.close();
}

void write_super_seq_for_all_sp_trees(t_vector strees, dd_vector resp_scores,
        gene_families gfam, string outdir) {
    
    /*
     *  strees is mxn matrix where m is num of gene families and n is num of species trees
     *  so in each row (for each gene family 'gi') we have resp score of species tree
     *  sj for gi (where j = 1..n )
     */
    
    vector<gene_families> families_for_stree;
    families_for_stree.reserve(strees.size());

    cout << "resp_score matrix has " << resp_scores.size() << " rows and " <<
            resp_scores[0].size() << " columns" << endl;

    for (int i = 0; i < strees.size(); ++i) {
        gene_families gfi;
        for (int j = 0; j < gfam.size(); ++j) {
            if (resp_scores[j][i] > THREHOLD_FOR_GFAM_INCLUSION)
                gfi.push_back(gfam[j]);
        }
        families_for_stree.push_back(gfi);
        cout << "For species tree " << i << ", we have " << gfi.size() << " families" << endl;
    }

    // now write to the file
    for (int i = 0; i < families_for_stree.size(); ++i) {
        sequenceContainer ssequence;
        get_super_seq(ssequence, families_for_stree[i]);
        cout << "For species tree " << i << ", we have sequence length of " << ssequence.seqLen() << endl;
        string fname(outdir);
        fname.append("/sptree");
        fname.append(int_to_string(i+1));
        fname.append(".fasta");
        
        cout << "Writing super sequences for species tree " << (i+1) << endl;
        write_seq_to_file(ssequence, fname);
    }
    
    cout << "All the super-sequences have been written to " << outdir << endl;
}

std::string int_to_string(int i) {
    char spno[50];
    sprintf(spno, "%d", i);
    string s(spno);
    
    return s;
}

