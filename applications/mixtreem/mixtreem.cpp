// $Id: stisem.cpp,v 2.12 2006-09-19 09:03:30 ninio Exp $

#include "bootstrapProvider.h"
#include "mainSemphy.h"
#include "semphy_cmdline.h"
#include "cmdline2EvolObjs.h"
#include "stochasticProcess.h"
#include "errorMsg.h"
#include "Reader.h"
#include "NJTree.h"
#include "ResponsibilityScore.h"
#include "SuperSequence.h"
#include "StisemLog.h"
//#include "FastTree.c"

#include <stdlib.h>
#include <stdio.h>
#include <ctime>
#include <limits>
#include <string.h>

void handle_command_line_arguments(int argc, char* argv[],
        semphy_args_info &args_info);

tree perform_semphy_analysis(semphy_args_info args_info, tree sp_tree,
        sequenceContainer super_sequences, d_vector resp_score_gfamily,
        vector<GeneFamilyMapper> msa_mapper, double &sp_tree_llk);

void iterate(semphy_args_info args, gene_families families, sequenceContainer fullMSA,
        vector<GeneFamilyMapper> msa_mapper, t_vector species_trees, stochasticProcess sp,
        StisemLog &stisem_logger);


void writeArrayToStisemLog(StisemLog &stisem_logger, s_vector gfNames, string header);

int main(int argc, char* argv[]) {

    if (argc < 2) {
        errorMsg::reportError("The program must get some parameters in "
                "the command line, use -h for help");
    }

    /* handle command line arguments */
    semphy_args_info args_info;
    handle_command_line_arguments(argc, argv, args_info);

    cmdline2EvolObjs<semphy_args_info> cmd2Objs(args_info);

    /* set a random seed and save it for writing to stisem log file */
    //long seed = JKISS32();
    //cout << "seed before " << args_info.seed_arg << endl;
    if (args_info.seed_given) {
        cout << "seed is " << args_info.seed_arg << endl;
    } else {
        cout << "seed before " << args_info.seed_arg << endl;
        srand(time(NULL));        
        long seed = rand();
        args_info.seed_arg = seed;        
        args_info.seed_given = 1;
        cout << "seed after " << args_info.seed_arg << endl;
    }
    stochasticProcess sp;
    if (args_info.optimizeAlpha_flag) {
        sp = cmd2Objs.cmdline2StochasticProcessThatRequiresAlphaOptimization();
    } else {
        sp = cmd2Objs.cmdline2StochasticProcess();
    }

    /* read gene families */
    Reader reader;
    gene_families_with_names gf_w_names;
    gene_families gfamilies;
    s_vector gfNames;
    reader.readGeneFamilies(args_info.genefamilies_arg, gfamilies, gf_w_names,
            gfNames, args_info.alphabet_arg);

    /* check if sane values are there */
    /*
    for (int i = 0; i < gfamilies.size(); i++){        
        for(sequenceContainer::taxaIterator t = gfamilies[i].taxaBegin();
                t != gfamilies[i].taxaEnd(); ++t) {
            cout << t->name() << ", " << endl;
        }
        cout << endl;
        cout << "no of sequences are " << gfamilies[i].numberOfSeqs() << " ; ";
        cout << gfamilies[i].seqLen() << endl;
    }
     */

    /* read initial species tree */
    s_vector stNames;
    t_vector species_trees = reader.readSpeciesTrees(args_info.speciestrees_arg, stNames);

    StisemLog stisem_logger(args_info.stisemlog_arg, gfamilies.size(),
            species_trees.size(), args_info.stisemiter_arg, args_info.seed_arg);
    writeArrayToStisemLog(stisem_logger, gfNames, "Gene Family Files");
    writeArrayToStisemLog(stisem_logger, stNames, "Species Trees Files");
    stisem_logger.writeConcatenatedSequence(reader.getWhole_genome_msa());
    cout << "Total number of families are " << gfamilies.size() 
            << " while the sequence length of super_sequence is " << reader.getWhole_genome_msa().seqLen() << endl;

    /* call the iterative routine */
    iterate(args_info, gfamilies, reader.getWhole_genome_msa(), reader.getMsa_mappers(),
            species_trees, sp, stisem_logger);

    return 0;
}

void handle_command_line_arguments(int argc, char* argv[],
        semphy_args_info &args_info) {
    if (semphy_cmdline_parser(argc, argv, &args_info) != 0) {
        errorMsg::reportError("error reading command line", 1);
    }

    /**/
    cout << "on the way" << endl;
    if (args_info.consurf_flag) {
        tree t_tmp;
        { // for ms1;
            args_info.optimizeAlpha_flag = 0; // true
            args_info.alpha_given = 0;
            mainSemphy ms_tmp(args_info);
            ms_tmp.computeNJtree();
            t_tmp = ms_tmp.getTree();
        }
        args_info.optimizeAlpha_flag = 1; // true
        args_info.alpha_given = 1;
        args_info.laguerre_flag = 3;
        strcat(args_info.Logfile_arg, "log.txt");
        mainSemphy ms2(args_info);
        myLog::printArgv(1, argc, argv);
        ms2.setTree(t_tmp);
        ms2.optimizeAlphaOnly();
        args_info.alpha_arg = static_cast<gammaDistribution*> (ms2.getStochasticProcess().distr())->getAlpha();
    }

    myLog::printArgv(1, argc, argv);
}

void showTreeLengths(vector<tree::nodeP> nodes) {
    for (unsigned i = 0; i < nodes.size(); ++i)
        cout << nodes[i]->dis2father() << "\t";
    cout << endl;
}

/**
 * Perform the tree likelihood using structural EM 
 * @param args_info             SEMPHY command line arguments
 * @param sp_tree               initial species tree for SEMPHY
 * @param super_sequences       MSA for super sequences
 * @param sp_tree_llk           sp_tree likelihood 
 * @return                      SEMPHY's produced species tree
 */
tree perform_semphy_analysis(semphy_args_info args_info, tree sp_tree,
        sequenceContainer super_sequences, d_vector resp_score_gfamily,
        vector<GeneFamilyMapper> msa_mapper, double &sp_tree_llk) {
    mainSemphy ms(args_info);
    if (!args_info.BPonUserTree_given) {
        /* check if the species tree has any edge length equal to zero
         * if yes, then assign the min(sp_tree_lengths) to such edges. 
         * This is necessary since we get error in main SEMPHY code, apparently
         * due to them (not 100% sure though)
         */
        /*
        vector<tree::nodeP> all_nodes;
        sp_tree.getAllNodes(all_nodes, sp_tree.getRoot());
        showTreeLengths(all_nodes);
        double min_length = std::numeric_limits<double>::max();
        /* first find the minimum length greater than zero across the
         * branch lengths 
         */
        /*
       for(unsigned i = 0; i < all_nodes.size(); ++i){
           double el = all_nodes[i]->dis2father();
           if((min_length > el) && ( el > 0))
               min_length = el;
       }
        
       /* now assign all the zeros to min_length */
        /*
        for(unsigned i = 0; i < all_nodes.size(); ++i){
            double el = all_nodes[i]->dis2father();
            if(all_nodes[i]->dis2father() < min_length)
                all_nodes[i]->setDisToFather(min_length);
        }
        
        showTreeLengths(all_nodes);
         */
        /*
        std::fstream fs;
        fs.open ("/tmp/test.txt", std::fstream::in | std::fstream::out | std::fstream::app);
        fs << "before\n";
        sp_tree.output(fs);
        fs << "\n";
        fs.close();
         */
        //ms.setTree(sp_tree);
        cout << "number of sequences in super_seq is " << super_sequences.seqLen() <<
                " and number of genes are " << super_sequences.numberOfSeqs() << endl;
        //cout << "size of weights array is " << resp_score_gfamily.size() << endl;               
        
        ms.setSequenceContainer(super_sequences);
        ms.setResponsibilityScores(resp_score_gfamily);
        
        cout << "\n\n";
        cout << "SIZE of resp_score_gfamily is " << resp_score_gfamily.size() << endl;
        for(int i = 0; i < resp_score_gfamily.size(); ++i)
            cout << resp_score_gfamily[i] << endl;
        cout << endl;
        
        ms.setMsaMapper(msa_mapper);
        ms.compute();
    }

    // computing the BP values
    if (args_info.BPrepeats_given) {
        bootstrapProvider bp(args_info);
        bp.computeBP(ms);
        bp.output(ms.out());
    }
    ms.getTree().output(cout);
    /* store the tree's likelihood */
    sp_tree_llk = ms.getLikelihood();

    return ms.getTree();
}

/**
 * This is the iterative process where each iteration consists of 
 * following operations.
 * 1. compute responsibility score for each (family, species_tree) pair
 * 2. construct super sequences from gene families for each species tree 
 *    based on their responsibility score for that species tree.
 * 3. for each species tree, compute new topology based on the super sequences
 * 4. compare new topologies with old one, and stop computing that topology again
 *    if they are the same
 * 5. Exit when done for all species tree topologies
 * 
 * @param args command line arguments for SEMPHY
 * @param families the MSA for gene families
 * @param fullMSA the collective MSA we get by combining all MSAs
 * @param msa_mapper information for each MSA position in fullMSA
 * @param species_trees set of species trees
 * @param sp stochasticProcess object used in likelihood computation 
 *        in SEMPHY library
 * @param num_iter The number of iterations after which the program will return. If convergence
 *         is reached before that, the program will return forthwith. (default = 10)
 * 
 * @author Ikram Ullah (ikramu) 20130913
 */
void iterate(semphy_args_info args, gene_families families, sequenceContainer fullMSA,
        vector<GeneFamilyMapper> msa_mapper, t_vector species_trees, stochasticProcess sp,
        StisemLog &stisem_logger) {
    for (unsigned k = 0; k < species_trees.size(); ++k)
        stisem_logger.writeSpeciesTree(*species_trees[k], k, 0.0);


    unsigned cur_iter = 1;

    /* set up structures to hold previous iteration values */
    double prev_likelihood = -std::numeric_limits<double>::max();

    /* we will repeat the process until convergence */
    while (true) {
        stisem_logger.printIterationStartHeader(cur_iter, args.stisemiter_arg);

        //SuperSequence ss(families);

        ResponsibilityScore rscore(families, species_trees, sp);
        rscore.computeLikelihoodsAllTreesAllFamilies();

        dd_vector resp_score_gfamily = rscore.getResponsibilityScoreByGeneFamily();
        //dd_vector resp_score_stree = rscore.getResponsibilityScoreBySpeciesTree();
        for (int i = 0; i < resp_score_gfamily.size(); ++i) {
            double total = 0.0;
            for (int j = 0; j < resp_score_gfamily[0].size(); ++j) {
                total += resp_score_gfamily[i][j];
                cout << resp_score_gfamily[i][j] << "\t";
            }

            if (total <= 0.9) {
                cerr << "Error in normalizing the responsibilities. Exiting..." << endl;
                exit(EXIT_FAILURE);
            }
            cout << endl;
        }

        /* write responsibility scores to log file */
        stisem_logger.writeResponsibilityScores(rscore.getOriginalLikelihoodValues());
        stisem_logger.writeResponsibilityScores(resp_score_gfamily);

        /*
        /* construct super-sequences based on by-tree responsibility scores 
        sseq_vector super_seq;

        /* for each species tree 
        for (unsigned stree_id = 0; stree_id < species_trees.size(); ++stree_id) {
            ss_map cur_seq;
            ss.getMSAChunkGivenRespScore(resp_score_gfamily, stree_id, cur_seq);
            sequenceContainer sc = reader.constructSequenceContainerFromStringMap(cur_seq,
                    families[0].getAlphabet());
            super_seq.push_back(sc);

            /* write super sequences to log file 
            //stisem_logger.writeSuperSequences(cur_seq, stree_id);
        }
         */

        double cur_likelihood = 0.0;

        /* perform SEMPHY analysis for each set of super sequences (or species tree) */
        for (unsigned ss_it = 0; ss_it < species_trees.size(); ++ss_it) {

            double sp_llk = std::numeric_limits<double>::min();
            //cout << "before calling SEMPHY routine..." << endl;

            d_vector cur_resp(resp_score_gfamily.size(), 0.0);
            for (unsigned y = 0; y < resp_score_gfamily.size(); ++y) {
                cur_resp[y] = resp_score_gfamily[y][ss_it];
                cout << cur_resp[y] << endl;
            }
            //tree sp_tree = perform_semphy_analysis(args,
            //        *species_trees[ss_it], super_seq[ss_it], sp_llk);            
            tree sp_tree = perform_semphy_analysis(args, *species_trees[ss_it],
                    fullMSA, cur_resp, msa_mapper, sp_llk);

            /* add log likelihood to the total */
            cur_likelihood += sp_llk;

            /* set tree i to tree (i+1) */
            *species_trees[ss_it] = sp_tree;

            /* write species tree to log file */
            stisem_logger.writeSpeciesTree(*species_trees[ss_it], ss_it, sp_llk);
        }

        prev_likelihood = cur_likelihood;

        cout << "After " << cur_iter << ", overall species likelihood is " << cur_likelihood << endl;

        /* increment the iteration counter */
        cur_iter++;

        /* check if we have increased on the overall likelihood */
        /*
        if (prev_likelihood < cur_likelihood) {
            prev_likelihood = cur_likelihood;
        } else {
            // if we  haven't increased but are still in 
            // early iterations, then continue 
            //
            if (cur_iter < 10)
                continue;
            else
                break;

        }
         */

        /* if we have reached max iterations, then return */
        if (cur_iter > args.stisemiter_arg)
            break;
    }

    /* write the summary to the log file */
    stisem_logger.writeSummaryStatistics(cur_iter - 1, prev_likelihood);
}

/**
 * Writes vector array contents to log file. This function is mainly meant to 
 * write gene family names and species tree names to log file.
 * 
 * @param stisem_logger log file object
 * @param data          vector data
 * @param header        header explaining what the data is about
 */
void writeArrayToStisemLog(StisemLog &stisem_logger, s_vector data, string header) {
    stisem_logger.openFileConnection();
    header += string("\n-------------------");
    stisem_logger.writeLine(header);
    for (unsigned i = 0; i < data.size(); ++i)
        stisem_logger.writeLine(data[i]);
    stisem_logger.writeLine("##############################");
    stisem_logger.closeFileConnection();
}
