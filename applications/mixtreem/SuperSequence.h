/* 
 * File:   SuperSequence.h
 * Author: ikramu
 *
 * Created on July 30, 2013, 5:48 PM
 */

#ifndef SUPERSEQUENCE_H
#define	SUPERSEQUENCE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
//#include <random>

#include "errorMsg.h"
#include "sequenceContainer.h"
#include "GlobalConstants.h"

class SuperSequence {
public:
    SuperSequence();
    SuperSequence(gene_families gf);
    SuperSequence(const SuperSequence& orig);
    virtual ~SuperSequence();
    
    /* resp_score is MxN matrix with M trees and N families */
    sequenceContainer constructSuperSequence(d_vector resp_score);
    
    
    i_vector getUniformSitesFromRandomShuffle(int range, int num_sites);
    
    /* get chunk of MSA given responsibility score */
    void getMSAChunkGivenRespScore(dd_vector resp_score_stree, 
        int stree_id, ss_map& ssequence);
    
    /**
     * copy selected (random) sites from multiple sequence alignment 
     * to current_seq
     * 
     * @param sites vector containing ids of the selected sites
     * @param current_seq container to which sites will be copied
     * @param fam_id id of the MSA within the vector
     * @author Ikram Ullah (ikramu)
     */
    void getSelectedSitesFromMSA(unsigned fam_id, i_vector sites, ss_map& current_seq);
            
private:
    gene_families geneFamilies;
    sequenceContainer superSequence;
};

#endif	/* SUPERSEQUENCE_H */

