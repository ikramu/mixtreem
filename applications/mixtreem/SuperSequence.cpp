/* 
 * File:   SuperSequence.cpp
 * Author: ikramu
 * 
 * Created on July 30, 2013, 5:48 PM
 */

#include <stdlib.h>
#include <cmath>
#include <sstream>

#include "SuperSequence.h"
#include "amino.h"

SuperSequence::SuperSequence() {
}

SuperSequence::SuperSequence(gene_families gf) {
    /* seed for pseudo-random generator, used in random_shuffle() routine */
    srand(time(NULL));
    
    geneFamilies.resize(gf.size());
    for (int i = 0; i < gf.size(); ++i) {
        geneFamilies[i] = gf[i];        
    }
}

SuperSequence::SuperSequence(const SuperSequence& orig) {
}

SuperSequence::~SuperSequence() {
}

/* resp_score is MxN matrix with M trees and N families */
sequenceContainer SuperSequence::constructSuperSequence(d_vector resp_score) {

    for (int i = 0; i < resp_score.size(); ++i) {
        int num_sites = ceil(resp_score[i] * 100);
        i_vector sites = getUniformSitesFromRandomShuffle(geneFamilies[i].seqLen(), num_sites);
    }

    return superSequence;
}

i_vector SuperSequence::getUniformSitesFromRandomShuffle(int range, int num_sites) {
    i_vector values(range);
    for (int i = 0; i < values.size(); ++i) {
        values[i] = i;
        //cout << values[i] << ", ";
    }
    //cout << endl;

    /* We are using Knuth-Fisher-Yates algorithm for uniform shuffling of 
     * the vector. After its done, we will get the num_sites sub-vector 
     * for this vector.
     */
    std::random_shuffle(values.begin(), values.end());

    i_vector selected(values.begin(), values.begin() + num_sites);

    return selected;
}

/**
 * get MSA chunk from each family based on how much weight (suitability) 
 * it has for the species tree
 * @param resp_score_family (m x n) responsibility matrix for m gene
 * families and n species trees.
 * @param stree_id the ID of the species tree (within responsibility matrix
 * for which we are creating the super sequence
 * @param ssequence data structure for containing the super sequence
 */
void SuperSequence::getMSAChunkGivenRespScore(dd_vector resp_score_family,
        int stree_id, ss_map& ssequence) {    
    ss_map current_seq;    
    
    /* set up names of sequences for hash map */
    s_vector seqNames = geneFamilies[0].names();    
    for(s_vector::iterator i = seqNames.begin(); i != seqNames.end(); ++i){
        current_seq[*i] = "";    
        ssequence[*i] = "";
    }
    
    for (unsigned fam_id = 0; fam_id < geneFamilies.size(); ++fam_id) {
        int range = geneFamilies[fam_id].seqLen();
        double r_score = resp_score_family[fam_id][stree_id];
        //double r_score = resp_score_stree[stree_id][fam_id];
        int num_sites = ceil(range * r_score);
        i_vector sites = getUniformSitesFromRandomShuffle(range, num_sites);
        
        /* copy selected sites from MSA into current_seq */
        getSelectedSitesFromMSA(fam_id, sites, current_seq);
        
        /* copy current_seq into ssequence */
        for(ss_map::iterator k = current_seq.begin(); k != current_seq.end(); ++k)
            ssequence[k->first] += k->second;  
        
        /* empty current_seq */
        for(ss_map::iterator k = current_seq.begin(); k != current_seq.end(); ++k)
            current_seq[k->first] = "";
    }
}

void SuperSequence::getSelectedSitesFromMSA(unsigned fam_id, i_vector sites, ss_map& seq_container) {
    sequenceContainer msa = geneFamilies[fam_id];
    for(int i = 0; i < msa.numberOfSeqs(); ++i){
        sequence s = msa[i];        
        const string s_name = s.name();                        
        stringstream ss;
        for(unsigned j = 0; j < sites.size(); ++j) {
            int ch_id = s[sites[j]];
            ss << s.getAlphabet()->fromInt(ch_id);
        }
        
        /* copy selected sites to sequence container */
        seq_container[s_name] = ss.str();
    }
}

