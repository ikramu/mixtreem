/* 
 * File:   Reader.h
 * Author: ikramu
 *
 * Created on July 31, 2013, 6:04 PM
 */

#ifndef READER_H
#define	READER_H

#include <stddef.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <dirent.h>
#include <vector>

/* SEMPHY includes */
#include "alphabet.h"
#include "recognizeFormat.h"
#include "nucleotide.h"
#include "amino.h"
#include "codon.h"

/* STISEM includes */
#include "GlobalConstants.h"
#include "GeneFamilyMapper.h"

class Reader {
public:
    Reader();
    Reader(const Reader& orig);
    virtual ~Reader();
    
    
    void readGeneFamilies(string directory, gene_families &gf, 
    gene_families_with_names &families, Vstring &fgNames, int numAlp);
    
    void addMSAtoBigMSA(sequenceContainer msa, int counter);
    
    t_vector readSpeciesTrees(string directory, Vstring &stNames);
    t_vector constructRandomSpeciesTrees(int num);
    
    /* create sequenceContainer object from array of individual 
     * sequences (in string hash map format) */
    sequenceContainer constructSequenceContainerFromStringMap(ss_map seq_map, const alphabet* alph);

    vector<GeneFamilyMapper> getMsa_mappers() const {
        return msa_mappers;
    }

    sequenceContainer getWhole_genome_msa() const {
        return whole_genome_msa;
    }
    
private:
    sequenceContainer readSingleSequenceFile(int sizeOfAlp, 
            std::string fileName);
    sequenceContainer sortSequenceByGeneName(sequenceContainer unsorted);
    alphabet *getTypeOfSequence(int sizeOfAlp);
    s_vector getFileNamesFromDirectory(string directory);
    static bool compare_file_names(string a, string b);
    
    void getAllSitesFromMSAAsAlphabets(sequenceContainer msa, ss_map& seq_container);
    void createOneMSAFromAllFamilies(gene_families families);
    
    sequenceContainer whole_genome_msa;
    vector<GeneFamilyMapper> msa_mappers;
    
};

#endif	/* READER_H */

