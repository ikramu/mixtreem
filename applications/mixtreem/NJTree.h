/* 
 * File:   NJTree.h
 * Author: ikramu
 *
 * Created on August 1, 2013, 4:36 PM
 */

#ifndef NJTREE_H
#define	NJTREE_H

#include "GlobalConstants.h"
#include "semphy_cmdline.h"
#include "cmdline2EvolObjs.h"
#include "likeDist.h"
#include "nj.h"
#include "distanceTable.h"

class NJTree {
public:
    NJTree();
    NJTree(semphy_args_info sai);
    NJTree(const NJTree& orig);
    virtual ~NJTree();
    
    t_vector getNJTreesFromSequences(gene_families gf);
private:
    void initialize();    
    
    t_vector nj_trees;
    stochasticProcess sp;
    semphy_args_info cl_args;
};

#endif	/* NJTREE_H */

