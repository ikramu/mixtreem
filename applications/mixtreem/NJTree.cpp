/* 
 * File:   NJTree.cpp
 * Author: ikramu
 * 
 * Created on August 1, 2013, 4:36 PM
 */

#include "NJTree.h"
#include "Reader.h"

NJTree::NJTree() {

}

NJTree::NJTree(semphy_args_info sai) : cl_args(sai) {

    initialize();

    /*
    Reader reader;
    reader.readGeneFamilies(directory, )

    // Create alphabet
    alphabet *alphPtr = cmd2Objs.cmdline2Alphabet();

    // Create sequenceContainer (read from file)
    sequenceContainer sc = cmd2Objs.cmdline2SequenceContainer(alphPtr);

    // Take care of gaps
    cmd2Objs.takeCareOfGaps(sc);
     */

}

NJTree::NJTree(const NJTree& orig) {
}

NJTree::~NJTree() {
}

void NJTree::initialize() {
    cmdline2EvolObjs<semphy_args_info> cmd2Objs(cl_args);

    // Initialize seed
    cmd2Objs.initializeRandomSeed();

    // Create homogeneous stochastic process
    sp = cmd2Objs.cmdline2HomogenuisStochasticProcess();

    // Create stochastic process with gamma ASRV. If alpha was given it will be set
    //stochasticProcess sp;
    if (cl_args.optimizeAlpha_flag) {
        sp = cmd2Objs.cmdline2StochasticProcessThatRequiresAlphaOptimization();
    } else {
        sp = cmd2Objs.cmdline2StochasticProcess();
    }
    assert(sp.categories() > 1); // see that we actually got gamma
}

t_vector NJTree::getNJTreesFromSequences(gene_families gf) {        
    nj_trees.assign(gf.size(), new tree());

    MDOUBLE treeLogLikelihood = 0.0;
    NJalg nj;
    MDOUBLE newAlpha = 99.0;

    for (int i = 0; i < gf.size(); ++i) {
        VVdouble distTable;
        vector<string> vNames;
        likeDist ld(sp);
        giveDistanceTable(&ld, gf[i], distTable, vNames, NULL);

        *nj_trees[i] = nj.computeTree(distTable, vNames); 
    }

    return nj_trees;
}

