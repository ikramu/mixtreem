/* 
 * File:   Reader.cpp
 * Author: ikramu
 * 
 * Created on July 31, 2013, 6:04 PM
 */

#include <math.h>
#include <sstream>
#include <algorithm>

#include "Reader.h"
#include "someUtil.h"

Reader::Reader() {
}

Reader::Reader(const Reader& orig) {
}

Reader::~Reader() {
}

void Reader::readGeneFamilies(string directory, gene_families &gf,
        gene_families_with_names &families, Vstring &gfNames, int sizeOfAlp) {
    gfNames = getFileNamesFromDirectory(directory);
    //families.resize(gfNames.size());
    gf.resize(gfNames.size());
    msa_mappers.resize(gfNames.size(), GeneFamilyMapper());

    for (int i = 0; i < gfNames.size(); i++) {
        cout << (i + 1) << " : " << gfNames[i] << endl;
        //families[i][gfNames[i]] = readSingleSequenceFile(sizeOfAlp, gfNames[i]);
        gf[i] = readSingleSequenceFile(sizeOfAlp, gfNames[i]);
        /* currently we remove the gapped position, can make them 'unknown' as well */
        //families[i][gfNames[i]].removeGapPositions();
        gf[i].removeGapPositions();
        //families[i][gfNames[i]].addGeneralRemark(gfNames[i]);
        gf[i].addGeneralRemark(gfNames[i]);
        
        /*
        for(int k = 0; k < gf[i].numberOfSeqs(); ++i){
            cout << gf[i][k].toString() << endl;
            for(int l = 0; l < gf[i][k].seqLen(); ++l)
                cout << gf[i][k][l];
            cout << endl;
        }
        cout << "\n\n";
        */
        
        //addMSAtoBigMSA(families[i][gfNames[i]], i);
        addMSAtoBigMSA(gf[i], i);
    }
}

void Reader::addMSAtoBigMSA(sequenceContainer msa, int counter) {
    /* add to big sequence */
    if (counter == 0) {
        whole_genome_msa = sequenceContainer(msa, msa.getAlphabet());

        /* add the mapper info for msa */
        string sfile = msa.getGeneralRemarks()[0];        
        //GeneFamilyMapper gfm(sfile, counter, msa.seqLen(), 0);
        msa_mappers[counter] = GeneFamilyMapper(sfile, counter, msa.seqLen(), 0);

        /* make sure that big_msa contains ONLY msa */
        assert(whole_genome_msa.seqLen() == msa.seqLen());
    } else {
        /* add the mapper info for msa */
        int sl = msa.seqLen();
        int sp = whole_genome_msa.seqLen();
        /* add msaID, seqLength, startPosition for msa */
        msa_mappers[counter] = GeneFamilyMapper(msa.getGeneralRemarks()[0], counter, sl, sp);

        /* make sure that msa and big_msa have same number of sequences */
        assert(msa.names().size() == whole_genome_msa.names().size());

        s_vector names = whole_genome_msa.names();
        for (unsigned i = 0; i < names.size(); ++i) {
            sequence s = msa[i];
            int id = whole_genome_msa.getId(s.name(), false);

            //int b4 = big_msa[id].seqLen();
            //cout << big_msa[id].name() << " size Before adding " << b4 << endl;
            //cout << s.name() << " length alone is " << s.seqLen() << endl;
            whole_genome_msa[id] += s;

            /* diagnotics output */
            //cout << big_msa[id].name() << " size After adding " << big_msa[id].seqLen() << endl;
            //cout << "while it should be " << (b4 + s.seqLen()) << endl;
            //cout << "it starts from " << msa_mappers[counter].GetStartPosition() << " till " << 
            //       (msa_mappers[counter].GetStartPosition() + s.seqLen() - 1) << endl;
            //cout << endl;
        }
    }
}

ss_map Reader::createOneMSAFromAllFamilies(gene_families families) {
    if (families.size() < 1) {
        cerr << "Error: There are no families to create one BIG family. Exiting..." << endl;
        exit(EXIT_FAILURE);
    }

    ss_map bigseq;
    /* set up names of sequences for hash map */
    s_vector seqNames = families[0].names();
    for (s_vector::iterator i = seqNames.begin(); i != seqNames.end(); ++i) {
        bigseq[*i] = "";
    }

    /* add the sequences one by one */
    for (unsigned scNo = 0; scNo < families.size(); ++scNo) {
        sequenceContainer sc = families[scNo];

        for (int sNo = 0; sNo < sc.numberOfSeqs(); ++sNo) {
            ss_map my_seq;
            getAllSitesFromMSAAsAlphabets(sc, my_seq);
            sequence s = sc[sNo];
            bigseq[s.name()] += s.toString();
        }
    }
    
    return bigseq;
}

void Reader::getAllSitesFromMSAAsAlphabets(sequenceContainer msa, ss_map& seq_container) {
    for (int i = 0; i < msa.numberOfSeqs(); ++i) {
        sequence s = msa[i];
        const string s_name = s.name();
        stringstream ss;
        for (int j = 0; j < s.seqLen(); ++j) {
            int ch_id = s[j];
            ss << s.getAlphabet()->fromInt(ch_id);
        }

        /* copy selected sites to sequence container */
        seq_container[s_name] = ss.str();
    }
}

sequenceContainer Reader::sortSequenceByGeneName(sequenceContainer unsorted) {
    sequenceContainer sc;
    sc = unsorted;
    map<string, sequence> all_seq;
    for (unsigned i = 0; i < unsorted.numberOfSeqs(); i++) {
        sequence s = unsorted[i];
        all_seq.insert(std::pair<string, sequence > (s.name(), s));
        //all_seq[s.name()].addFromString(s.toString());
        sc.remove(i);
    }

    for (map<string, sequence>::iterator iter = all_seq.begin(); iter != all_seq.end(); ++iter)
        sc.add(iter->second);

    return sc;
}

sequenceContainer Reader::readSingleSequenceFile(int sizeOfAlp,
        std::string sequenceFileName) {
    sequenceContainer scon, sortedScon;
    ifstream ins;
    istream* inPtr; // = &cin;
    alphabet *alphPtr = getTypeOfSequence(sizeOfAlp);
    if (sequenceFileName != "" && sequenceFileName != "-") {
        ins.open(sequenceFileName.c_str());
        if (!ins.is_open()) {
            errorMsg::reportError(string("can not open sequence file ")
                    + sequenceFileName);
            exit(EXIT_FAILURE);
        }
        inPtr = &ins;
    }
    istream& in = *inPtr;
    scon = recognizeFormat::read(in, alphPtr);

    return scon;
}

alphabet *Reader::getTypeOfSequence(int sizeOfAlp) {
    // always defined, with default
    switch (sizeOfAlp) {
        case 4:
            return new nucleotide;
            break;
        case 20:
            return new amino;
            break;
        case 64: case 61: case 60: case 62:
            return new codon;
            break;
        default: errorMsg::reportError("alphabet size not supported");
    }
    return NULL; // never happens
}

Vstring Reader::getFileNamesFromDirectory(string directory) {
    DIR *dir;
    Vstring names;
    vector<char*> sortedNames;
    names.clear();

    struct dirent *ent;
    if ((dir = opendir(directory.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir(dir)) != NULL) {
            string n(ent->d_name);
            if ((n != ".") && (n != "..") && (n != ".svn")) {
                names.push_back(directory + n);                
            }
        }
        closedir(dir);
    } else {
        /* could not open directory */
        perror("Error: could not open gene families' directory. Exiting...");
        exit(EXIT_FAILURE);
    }

    /* For sake of ease in dubugging, we sort all the entries */
    std::sort(names.begin(), names.end());

    for (std::vector<std::string>::iterator i = names.begin(), j = i + 1;
            j != names.end();
            ++i, ++j)
        if (*i > *j) {
            cerr << "Couldn't sort the values in input string. Exiting..." << endl;
        }     
    
    return names;
}

bool Reader::compare_file_names(std::string a, std::string b) {
    toLower(a);
    toLower(b);
    size_t start_a = a.find_last_not_of("0123456789");
    size_t end_a = a.find_first_of(".",start_a);
    int aint = atoi(a.substr(start_a,(end_a - start_a)).c_str());
    size_t start_b = b.find_last_not_of("0123456789");
    size_t end_b = b.find_first_of(".",start_b);
    int bint = atoi(b.substr(start_b,(end_b - start_b)).c_str());
    
    cout << aint << " , " << bint << endl;
    
    return (aint > bint);
}

t_vector Reader::readSpeciesTrees(string directory, Vstring &stNames) {
    t_vector trees;
    stNames = getFileNamesFromDirectory(directory);
    trees.resize(stNames.size(), NULL);

    for (int i = 0; i < stNames.size(); ++i) {
        cout << "reading " << stNames[i] << endl;
        trees[i] = new tree(stNames[i]);
    }

    return trees;
}

/**
 * construct ML species trees using uniformally distributed sites
 * from all the available gene families. For each of the "n" species 
 * trees, we take 1/n (uniformally distributed) sites from each gene family.
 * @param num number of species trees 
 * @return vector of species trees
 */
t_vector Reader::constructRandomSpeciesTrees(int num) {
    s_vector strees; /* one string for each MSA */

}

sequenceContainer Reader::constructSequenceContainerFromStringMap(
        ss_map seq_map, const alphabet* alph) {
    sequenceContainer msa;
    string remark = ""; // remark is empty 
    int localid = 0;
    for (ss_map::iterator mi = seq_map.begin(); mi != seq_map.end(); ++mi) {
        msa.add(sequence(mi->second, mi->first, remark, localid, alph));
        localid++;
    }

    return msa;
}

