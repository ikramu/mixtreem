/* 
 * File:   main.cpp
 * Author: ikramu
 *
 * Created on February 26, 2014, 7:05 PM
 */

#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <errno.h>
#include <iosfwd>
#include "errorMsg.h"
#include "MixTreEMLogParser.h"
#include "Reader.h"
#include "mainSemphy.h"
#include "bblEM.h"
#include "semphy_cmdline.h"

#define THREHOLD_FOR_GFAM_INCLUSION 0.00001

using namespace std;

void compute_weighted_likelihood(sequenceContainer super_seq, string outfile,
        d_vector weights, stochasticProcess sp, semphy_args_info &args_info);

void write_seq_to_file(sequenceContainer the_seq, string outfile);

void handle_command_line_arguments(int argc, char* argv[],
        semphy_args_info &args_info);

stochasticProcess get_sp(int argc, char* argv[], semphy_args_info &args_info);

string get_random_seq(int nchar);

void get_super_seq(sequenceContainer &super_seq, gene_families families);

void get_weights_for_sptree(d_vector &weights, gene_families families,
        dd_vector resp_scores, int sptree_id);

void write_species_trees(t_vector strees, string outdirectory);

void write_super_seq_for_all_sp_trees(t_vector strees, dd_vector resp_scores
        , gene_families families);

std::string int_to_string(int i);

vector<gene_families> get_super_seq_for_all_sp_trees(t_vector strees, 
        dd_vector resp_scores, gene_families gfam);

/*
 * 
 */
int main(int argc, char** argv) {

    if (argc < 3) {
        cerr << endl << "Information about the program" << endl;
        cerr << "*****************************" << endl;
        cerr << "This program parse mixtreem_log_file for the all the (unrooted) species tree files, and then" << endl;
        cerr << "for, each species tree, adds a random sequence as outgroup and writes it to out_tree_dir." << endl;
        cerr << "The written files will be used for outgroup based rooting by any R/Python/C library\n" << endl;
        cerr << "Error: wrong number of parameters" << endl;
        cerr << "Usage: " << argv[0] << " mixtreem_log_file out_tree_dir -a 20 --jtt -S -H" << endl;
        cerr << "mixtreem_log_file = Log file containing, among others, the responsibility scores and gene family names" << endl;
        cerr << "out_tree_dir = directory where ML tree (from concatenated sequences) and the one with outgroup will be written" << endl << endl;        
        cerr << "The rest of the arguments are for SEMPHY (a - alphabet size, jtt - model of seq evol" << endl;
        cerr << "S - semphy step, H - homogenous i.e. no gamma optimization" << endl;
        exit(EXIT_FAILURE);
    }


    //string pfile("../../../applications/outgroup_rooting/mixtreem_log.file");
    string pfile(argv[1]);
    string outdirectory(argv[2]);
    semphy_args_info args_info;
    sequenceContainer super_seq, sseq_with_outgroup;

    stochasticProcess sp = get_sp(argc, argv, args_info);


    //string rand_seq = get_random_seq(super_seq.seqLen()); 

    int sizeOfAlp = 20; // right now, we are doing it for protein only
    MixTreEMLogParser parser(pfile, sizeOfAlp);

    gene_families families = parser.getAllGeneFamilies();
    t_vector strees = parser.getAllSpeciesTrees();
    write_species_trees(strees, outdirectory);
    dd_vector resp_scores = parser.getAllResponsibilityScores();
    write_super_seq_for_all_sp_trees(strees, resp_scores, families);

    get_super_seq(super_seq, families);

    string rand_seq = get_random_seq(super_seq.seqLen());
    const sequence outgroup(rand_seq, string("out_group"), string(""), super_seq.numberOfSeqs(), super_seq.getAlphabet());
    sseq_with_outgroup = super_seq;
    sseq_with_outgroup.add(outgroup);

    // write to output for future error-detection-and-correction
    write_seq_to_file(super_seq, string(outdirectory).append("/original.fasta"));
    write_seq_to_file(sseq_with_outgroup, string(outdirectory).append("/with.outgroup.fasta"));
    
    vector<gene_families> sp_superseq = get_super_seq_for_all_sp_trees(strees, resp_scores, families);

    // sanity check; number of gene families should be equal to columns in resp_scores
    if (families.size() != resp_scores.size()) {
        cerr << "Something fishy! Number of responsibility scores should be equal to number of gene families" << endl;
        cerr << "Apparently, its not the case here, so quitting..." << endl;
        cerr << "Before getting angry at me, have a look at the files :)" << endl;
        exit(EXIT_FAILURE);
    }
    
    for (int i = 0; i < strees.size(); ++i) {
        d_vector weights;
        char spno[50];
        sprintf(spno, "%d", (i + 1));
        string outfile = outdirectory;
        outfile.append("/sp_");
        outfile.append(spno);
        outfile.append(".tree");
        clock_t begin, end;
        double time_spent;

        begin = clock();

        get_weights_for_sptree(weights, sp_superseq[i], resp_scores, i);

        // first compute the tree for original sequences
        cout << "computing tree for original sequences for tree # " << (i + 1) << endl;
        compute_weighted_likelihood(super_seq, outfile, weights, sp, args_info);
        end = clock();
        time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
        cout << "Total time spent in one SEMPHY call is " << time_spent << endl;

        // next compute the tree for original plus out-group sequences
        outfile.append(".with.outgroup");
        cout << "computing tree for outgroup-appended sequences for tree # " << (i + 1) << endl;
        compute_weighted_likelihood(sseq_with_outgroup, outfile, weights, sp, args_info);
    }
    return 0;
}

void write_species_trees(t_vector strees, string outdirectory) {
    // first remove all the previous species tree files.
    char spno[50];
    sprintf(spno, "%s", "/sp_*.tree");
    string command("rm -rf ");
    command.append(outdirectory);
    command.append(spno);
    system(command.c_str());

    for (int i = 0; i < strees.size(); ++i) {
        spno[0] = 0;
        sprintf(spno, "%d", (i + 1));
        string outfile = outdirectory;
        outfile.append("/sp_");
        outfile.append(spno);
        outfile.append(".tree");
        ofstream out(outfile.c_str());
        strees[i]-> output(out);
        out.flush();
        out.close();
    }
}

void handle_command_line_arguments(int argc, char* argv[],
        semphy_args_info &args_info) {
    if (semphy_cmdline_parser(argc, argv, &args_info) != 0) {
        errorMsg::reportError("error reading command line", 1);
    }

    /**/
    //cout << "on the way" << endl;
    if (args_info.consurf_flag) {
        tree t_tmp;
        { // for ms1;
            args_info.optimizeAlpha_flag = 0; // true
            args_info.alpha_given = 0;
            mainSemphy ms_tmp(args_info);
            ms_tmp.computeNJtree();
            t_tmp = ms_tmp.getTree();
        }
        args_info.optimizeAlpha_flag = 1; // true
        args_info.alpha_given = 1;
        args_info.laguerre_flag = 3;
        strcat(args_info.Logfile_arg, "log.txt");
        mainSemphy ms2(args_info);
        myLog::printArgv(1, argc, argv);
        ms2.setTree(t_tmp);
        ms2.optimizeAlphaOnly();
        args_info.alpha_arg = static_cast<gammaDistribution*> (ms2.getStochasticProcess().distr())->getAlpha();
    }

    myLog::printArgv(1, argc, argv);
}

stochasticProcess get_sp(int argc, char* argv[], semphy_args_info &args_info) {
    /* handle command line arguments */
    handle_command_line_arguments(argc, argv, args_info);

    cmdline2EvolObjs<semphy_args_info> cmd2Objs(args_info);

    /* set a random seed and save it for writing to stisem log file */
    //long seed = JKISS32();
    cout << "seed before " << args_info.seed_arg << endl;
    srand(time(NULL));
    long seed = rand();
    args_info.seed_arg = seed;
    cout << "seed is " << seed << endl;
    args_info.seed_given = 1;
    //cout << "seed before " << args_info.seed_arg << endl;

    stochasticProcess sp;
    if (args_info.optimizeAlpha_flag) {
        sp = cmd2Objs.cmdline2StochasticProcessThatRequiresAlphaOptimization();
    } else {
        sp = cmd2Objs.cmdline2StochasticProcess();
    }

    return sp;
}

d_vector get_array_of_same_double_values(double val, int num_val) {
    d_vector dv;
    dv.reserve(num_val);

    for (int i = 0; i < num_val; ++i)
        dv.push_back(val);

    return dv;
}

void compute_weighted_likelihood(sequenceContainer super_seq, string outfile,
        d_vector weights, stochasticProcess sp, semphy_args_info &args_info) {
    mainSemphy ms(args_info);
    ms.setSequenceContainer(super_seq);
    ms.setResponsibilityScores(weights);
    cout << "number of sequences in super_seq is " << super_seq.seqLen() <<
            " and number of genes are " << super_seq.numberOfSeqs() << endl;
    cout << "size of weights array is " << weights.size() << endl;
    //ms.computeNJtree();
    ms.computeSemphyTree();
    tree t = ms.getTree();

    //bblEM bblEM1(t, super_seq, sp, &weights);
    t.output(outfile);
}

void get_weights_for_sptree(d_vector &weights, gene_families families,
        dd_vector resp_scores, int sptree_id) {
    for (int i = 0; i < families.size(); ++i) {
        double val = resp_scores[i][sptree_id];

        d_vector wts = get_array_of_same_double_values(val, families[i].seqLen());
        if (i == 0) {
            weights = wts;
        } else {
            weights.insert(weights.end(), wts.begin(), wts.end());
        }
    }
}

void get_super_seq(sequenceContainer &super_seq, gene_families families) {
    Reader reader;
    reader.initializeGeneFamilyMapper(families.size());
    for (int i = 0; i < families.size(); ++i) {
        reader.addMSAtoBigMSA(families[i], i);
    }

    super_seq = reader.getWhole_genome_msa();
}

string get_random_seq(int nchar) {
    amino am;
    string rseq;

    for (int i = 0; i < nchar; ++i) {
        int char_id = rand() % 20 + 1;
        rseq.append(am.fromInt(char_id));
    }
    return rseq;
}

void write_seq_to_file(sequenceContainer the_seq, string outfile) {
    std::ofstream of(outfile.c_str(), std::ios::out);
    for (int i = 0; i < the_seq.numberOfSeqs(); ++i) {
        of << ">" << the_seq[i].name() << endl;
        of << the_seq[i].toString() << endl;
    }
    of.flush();
    of.close();
}

void write_super_seq_for_all_sp_trees(t_vector strees, dd_vector resp_scores,
        gene_families gfam) {
    // strees is mxn matrix where m is num of gene families and n is num of species trees
    // so in each row (for each gene family 'gi') we have resp score of species tree
    // sj for gi (where j = 1..n )
    vector<gene_families> families_for_stree;
    families_for_stree.reserve(strees.size());

    cout << "resp_score matrix has " << resp_scores.size() << " rows and " <<
            resp_scores[0].size() << " columns" << endl;

    for (int i = 0; i < strees.size(); ++i) {
        gene_families gfi;
        for (int j = 0; j < gfam.size(); ++j) {
            if (resp_scores[j][i] > THREHOLD_FOR_GFAM_INCLUSION)
                gfi.push_back(gfam[j]);
        }
        families_for_stree.push_back(gfi);
        cout << "For species tree " << i << ", we have " << gfi.size() << " families" << endl;
    }

    // now write to the file
    for (int i = 0; i < families_for_stree.size(); ++i) {
        sequenceContainer ssequence;
        get_super_seq(ssequence, families_for_stree[i]);
        cout << "For species tree " << i << ", we have sequence length of " << ssequence.seqLen() << endl;
        string fname("/tmp/sseq_for_stree_");
        fname.append(int_to_string(i));
        fname.append(".fasta");
        
        write_seq_to_file(ssequence, fname);
    }
}

std::string int_to_string(int i) {
    char spno[50];
    sprintf(spno, "%d", i);
    string s(spno);
    
    return s;
}

vector<gene_families> get_super_seq_for_all_sp_trees(t_vector strees, dd_vector resp_scores,
        gene_families gfam) {
    
    /*
     *  strees is mxn matrix where m is num of gene families and n is num of species trees
     *  so in each row (for each gene family 'gi') we have resp score of species tree
     *  sj for gi (where j = 1..n )
     */
    
    vector<gene_families> families_for_stree;
    families_for_stree.reserve(strees.size());

    cout << "resp_score matrix has " << resp_scores.size() << " rows and " <<
            resp_scores[0].size() << " columns" << endl;

    for (int i = 0; i < strees.size(); ++i) {
        gene_families gfi;
        for (int j = 0; j < gfam.size(); ++j) {
            if (resp_scores[j][i] > THREHOLD_FOR_GFAM_INCLUSION)
                gfi.push_back(gfam[j]);
        }
        families_for_stree.push_back(gfi);
        cout << "For species tree " << i << ", we have " << gfi.size() << " families" << endl;
    }

    return families_for_stree;
}

