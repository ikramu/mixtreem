/* 
 * File:   MixTreEMLogParser.h
 * Author: ikramu
 *
 * Created on February 26, 2014, 7:31 PM
 */

#ifndef MIXTREEMLOGPARSER_H
#define	MIXTREEMLOGPARSER_H

#include <string>
#include <fstream>
#include <sstream>
#include "GlobalConstants.h"
#include "Reader.h"

class MixTreEMLogParser {
public:
    MixTreEMLogParser(string lf, int soa);
    MixTreEMLogParser(const MixTreEMLogParser& orig);
    virtual ~MixTreEMLogParser();
    
    gene_families getAllGeneFamilies();
    
    t_vector getAllSpeciesTrees();
    
    dd_vector getAllResponsibilityScores();
    
    bool sptree_already_exists(tree *t, t_vector trees, int size);
    
private:    
    string log_file;
    int sizeOfAlp;
    Reader reader;
};

#endif	/* MIXTREEMLOGPARSER_H */

